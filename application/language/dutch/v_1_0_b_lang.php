<?php
$lang = array(
    "local post viewed" => "lokale post bekeken",
    "cta button clicked" => "cta-knop aangeklikt",
    "location information" => "locatie informatie",
    "location list" => "locatie lijst",
    "new review url" => "nieuwe review url",
    "post analytics" => "post-analyse",
    "coupon code:" => "coupon code:",
    "queries chain" => "vragenketen",
    "queries direct" => "vragen direct",
    "end date:" => "einddatum:",
    "the number of times a resource was shown as a result of a search for the chain it belongs to, or a brand it sells. for example, starbucks, adidas. this is a subset of queries_indirect." => "het aantal keren dat een hulpmiddel is weergegeven als gevolg van een zoekopdracht naar de keten waartoe het behoort, of een merk dat het verkoopt. bijvoorbeeld starbucks, adidas. dit is een subset van queries_indirect.",
    "generic" => "algemeen",
    "message for generic reply." => "bericht voor algemeen antwoord.",
    "spintax" => "spintax",
    "do you really want to cancel this template?" => "wilt u deze sjabloon echt annuleren?",
    "warning!" => "waarschuwing!",
    "add more settings" => "voeg meer instellingen toe",
    "add settings" => "instellingen toevoegen",
    "remove" => "verwijderen",
    "star" => "ster",
    "message for generic reply" => "bericht voor algemeen antwoord",
    "update settings" => "update-instellingen",
    "the number of times the resource was shown when searching for the location directly." => "het aantal keren dat de bron werd getoond bij het direct zoeken naar de locatie.",
    "the number of times the resource was shown as a result of a categorical search (for example, restaurant)." => "het aantal keren dat de bron is weergegeven als resultaat van een categorische zoekopdracht (bijvoorbeeld restaurant).",
    "the number of times the resource was viewed on google maps." => "het aantal keren dat de bron is bekeken op google maps.",
    "for" => "voor",
    "the number of times the resource was viewed on google search." => "het aantal keren dat de bron is bekeken op Google Zoeken.",
    "the number of times the website was clicked." => "het aantal keer dat op de website is geklikt.",
    "the number of times the phone number was clicked." => "het aantal keren dat op het telefoonnummer is geklikt.",
    "the number of times driving directions were requested." => "het aantal keren dat een routebeschrijving is opgevraagd.",
    "the number of views on media items uploaded by the merchant." => "het aantal weergaven van media-items die door de verkoper zijn geüpload.",
    "the number of views on media items uploaded by customers." => "het aantal weergaven van media-items die door klanten zijn geüpload.",
    "the total number of media items that are currently live that have been uploaded by the merchant." => "het totale aantal media-items dat momenteel live is en door de verkoper is geüpload.",
    "the total number of media items that are currently live that have been uploaded by customers." => "het totale aantal media-items dat momenteel live is en door klanten is geüpload.",
    "the number of times the local post was viewed on google search." => "het aantal keren dat de lokale post is bekeken op Google Zoeken.",
    "the number of times the call to action button was clicked on google." => "het aantal keren dat op de call-to-action-knop op google is geklikt.",
    "phone number clicked" => "telefoonnummer aangeklikt",
    "driving directions requested" => "routebeschrijving gevraagd",
    "media uploaded by customers viewed" => "media geüpload door bekeken klanten",
    "start date:" => "begin datum:",
    "ok" => "OK",
    "campaign id" => "campagne-ID",
    "if you schedule a campaign, system will automatically process this campaign at mentioned time and time zone. schduled campaign may take upto 1 hour longer than your schedule time depending on server's processing." => "als u een campagne plant, verwerkt het systeem deze campagne automatisch op de genoemde tijd en tijdzone. een geplande campagne kan tot 1 uur langer duren dan uw geplande tijd, afhankelijk van de verwerking van de server.",
    "edit profile" => "bewerk profiel",
    "please select time zone" => "selecteer een tijdzone",
    "delete my account" => "Verwijder mijn account",
    "your subscription package is expired" => "uw abonnementspakket is verlopen",
    "to get access back please contact system admin." => "Neem contact op met de systeembeheerder om toegang te krijgen.",
    "read more" => "Lees verder",
    "limit" => "limiet",
    "used" => "gebruikt",
    "usage details" => "gebruiksdetails",
    "campaign" => "campagne",
    "cta/event/offer posts" => "cta / event / aanbieding berichten",
    "support spintax" => "ondersteuning spintax",
    "posting time" => "het plaatsen van tijd",
    "redeem online" => "online inwisselen",
    "change information about yourself on this page." => "verander informatie over jezelf op deze pagina.",
    "the number that you used to register on google my business will be set in the call now button." => "het nummer dat u heeft gebruikt om u te registreren op google my business, wordt ingesteld in de knop Nu bellen.",
    "action url:" => "actie url:",
    "post title:" => "titel van bericht:",
    "date range" => "datumbereik",
    "to" => "naar",
    "redeem url:" => "URL inwisselen:",
    "type summery here..." => "typ hier zomers ...",
    "post now" => "post nu",
    "preview" => "voorbeeld",
    "update campaign" => "campagne bijwerken",
    "create new post" => "nieuw bericht maken",
    "all posts" => "alle berichten",
    "delete account" => "account verwijderen",
    "hi" => "Hoi",
    "answer to question" => "antwoord op vraag",
    "answer now" => "antwoord nu",
    "question & answer report may take upto few minutes/hours to update & synchronize here." => "het vraag- en antwoordrapport kan tot enkele minuten / uren duren om hier bij te werken en te synchroniseren.",
    "update answer" => "update antwoord",
    "delete answer" => "antwoord verwijderen",
    "the answer will be deleted from your google account" => "het antwoord wordt verwijderd uit uw Google-account",
    "update reply to review" => "update antwoord op beoordeling",
    "delete review reply" => "review-antwoord verwijderen",
    "review report may take upto few minutes/hours to update & synchronize here." => "beoordelingsrapport kan tot enkele minuten / uren duren om hier te updaten en te synchroniseren.",
    "update review reply" => "update review antwoord",
    "anonymous" => "anoniem",
    "reply to review" => "antwoord op beoordeling",
    "reply now" => "antwoord nu",
    "do you really want to delete the review reply from google and database?" => "wilt u echt het review-antwoord van google en database verwijderen?",
    "do you really want to delete this review reply settings from the database?" => "wilt u deze instellingen voor het beantwoorden van recensies echt uit de database verwijderen?",
    "deleting your account will delete all your data from our system and this account can not be restored again. do you really want to delete your account?" => "Als u uw account verwijdert, worden al uw gegevens uit ons systeem verwijderd en kan dit account niet opnieuw worden hersteld. wil je echt je account verwijderen?",
    "all stars" => "Alle sterren",
    "photo" => "foto",
    "comment" => "commentaar",
    "replied at" => "antwoordde op",
    "do you really want to delete the reply to review from the database?" => "wilt u echt het antwoord dat u wilt beoordelen uit de database verwijderen?",
    "select package" => "selecteer pakket",
    "payment options" => "betalingsmogelijkheden",
    "last payment" => "laatste betaling",
    "recurring" => "terugkerend",
    "manual" => "handleiding",
    "subscription message" => "abonnementsbericht",
    "you have already a subscription enabled in paypal. if you want to use different paypal or different package, make sure to cancel your previous subscription from your paypal." => "u heeft al een abonnement ingeschakeld in paypal. als u een ander paypal of ander pakket wilt gebruiken, zorg er dan voor dat u uw vorige abonnement op uw paypal annuleert.",
    "delete account?" => "account verwijderen?",
    "success!" => "succes!",
    "social settings" => "sociale instellingen",
    "(max 5mb)" => "(max 5mb)",
    "credential check" => "referentiecontrole",
    "sign up" => "inschrijven",
    "name" => "naam",
    "captcha" => "captcha",
    "invalid captcha" => "Onjuiste captcha",
    "to activate your account please perform the following steps" => "Voer de volgende stappen uit om uw account te activeren",
    "activate your account" => "Activeer uw account",
    "account activation" => "Account activatie",
    "message subject" => "Bericht Onderwerp",
    "message" => "bericht",
    "sorry, your bulk limit is exceeded for this module." => "Sorry, uw bulklimiet is overschreden voor deze module.",
    "click here to see usage log" => "klik hier om het gebruikslogboek te zien",
    "sorry, your monthly limit is exceeded for this module." => "Sorry, uw maandelijkse limiet is overschreden voor deze module.",
    "your account and all of it's corresponding locations and campaigns have been deleted successfully." => "uw account en alle bijbehorende locaties en campagnes zijn met succes verwijderd.",
    "link and code will be expired after 24 hours" => "link en code verlopen na 24 uur",
    "to reset your password please perform the following steps" => "Voer de volgende stappen uit om uw wachtwoord opnieuw in te stellen",
    "add-on purchase code has not been provided." => "add-on aankoopcode is niet verstrekt.",
    "purchase code is not valid or already used." => "aankoopcode is niet geldig of wordt al gebruikt.",
    "something went wrong. curl is not working." => "er is iets fout gegaan. krul werkt niet.",
    "add-on unique name has not been provided." => "add-on unieke naam is niet opgegeven.",
    "add-on is already active. duplicate unique name found." => "add-on is al actief. dubbele unieke naam gevonden.",
    "access forbidden" => "toegang verboden",
    "add-on controller has not been provided." => "add-on controller is niet verstrekt.",
    "add-on controller not found." => "add-on controller niet gevonden.",
    "database error. something went wrong." => "Databank fout. er is iets fout gegaan.",
    "add-on has been activated successfully." => "add-on is succesvol geactiveerd.",
    "add-on has been deactivated successfully." => "add-on is succesvol gedeactiveerd.",
    "add-on has been deleted successfully." => "add-on is succesvol verwijderd.",
    "the email was sent by" => "de e-mail is verzonden door",
    "reset your password" => "Stel je wachtwoord opnieuw in",
    "we could not find any data." => "we konden geen gegevens vinden.",
    "location insights" => "locatie-inzichten",
    "end date" => "einddatum",
    "coupon code" => "coupon code",
    "coupon code must be alphanumeric characters" => "couponcode moet alfanumerieke tekens zijn",
    "redeem url" => "URL inwisselen",
    "schedule time" => "tijd plannen",
    "timezone" => "tijdzone",
    "summary" => "samenvatting",
    "media url" => "media url",
    "location name" => "locatie naam",
    "it was a bad request!" => "het was een slecht verzoek!",
    "post id" => "post-ID",
    "bad request" => "foute aanvraag",
    "post insights" => "post inzichten",
    "old password" => "Oud Wachtwoord",
    "enter this code" => "voer deze code in",
    "new password" => "nieuw paswoord",
    "please login with new password" => "log in met een nieuw wachtwoord",
    "the old password you have given is wrong" => "het oude wachtwoord dat je hebt opgegeven is verkeerd",
    "cron job" => "cron baan",
    "theme manager" => "thema manager",
    "dashboard" => "dashboard",
    "you do not have permission to access this content" => "je hebt geen toestemming om toegang te krijgen tot deze inhoud",
    "admin panel" => "Administratie Paneel",
    "login" => "Log in",
    "invalid email or password" => "ongeldig e-mailadres of wachtwoord",
    "account name or email not found." => "accountnaam of e-mailadres niet gevonden.",
    "password recovery" => "wachtwoord herstel",
    "go to this url" => "ga naar deze url",
    "something went wrong, please try again." => "Er is iets mis gegaan, probeer het alstublieft nogmaals.",
    "review reply settings" => "bekijk de antwoordinstellingen",
    "post title" => "titel van post",
    "question id" => "vraag-id",
    "review id" => "recensie-ID",
    "star rating must be one of five, four, three, two and one" => "sterbeoordeling moet een van vijf, vier, drie, twee en een zijn",
    "reply message" => "Antwoord",
    "review name" => "review naam",
    "profile photo" => "profielfoto",
    "you do not have permission to reply to the review" => "u heeft geen toestemming om op de recensie te reageren",
    "unable to update review-reply data to database" => "kan review-responsgegevens niet bijwerken naar database",
    "reply to the review updated successfully" => "antwoord op de beoordeling met succes bijgewerkt",
    "invalid review reply id provided" => "ongeldig antwoord-id opgegeven",
    "submit" => "indienen",
    "questions list" => "vragenlijst",
    "invalid question id provided" => "ongeldige vraag-ID opgegeven",
    "answer" => "antwoord",
    "invalid review id provided" => "ongeldige review-ID opgegeven",
    "you do not have permission to answer the question" => "je hebt geen toestemming om de vraag te beantwoorden",
    "answer to the question created successfully" => "antwoord op de succesvol gecreëerde vraag",
    "the answer to the question has been deleted successfully" => "het antwoord op de vraag is succesvol verwijderd",
    "review report" => "beoordelingsverslag",
    "profile" => "profiel",
    "language editor" => "taaleditor",
    "new language" => "nieuwe taal",
    "index (english)" => "index (engels)",
    "translation" => "vertaling",
    "your data has been successfully saved." => "uw gegevens zijn succesvol opgeslagen.",
    "update language" => "taal bijwerken",
    "your data has been updated successfully." => "uw gegevens zijn succesvol bijgewerkt.",
    "click to delete" => "klik om te verwijderen",
    "review reply type is invalid" => "antwoordtype van review is ongeldig",
    "reviews list" => "recensies lijst",
    "change settings" => "instellingen veranderen",
    "no data found." => "geen data gevonden.",
    "review list" => "beoordelingslijst",
    "post list" => "berichtenlijst",
    "questions & ans." => "vragen & ans.",
    "copy the below url for further use." => "kopieer de onderstaande url voor verder gebruik.",
    "report" => "verslag doen van",
    "keyword is required" => "trefwoord is vereist",
    "reply is required" => "antwoord is vereist",
    "star rating" => "sterbeoordeling",
    "reply type" => "antwoordtype",
    "generic message" => "algemeen bericht",
    "message for no match" => "bericht voor geen overeenkomst",
    "update reply" => "update antwoord",
    "delete reply" => "antwoord verwijderen",
    "location manager" => "locatie manager",
    "posts list" => "berichten lijst",
    "copied!" => "gekopieerd!",
    "copy" => "kopiëren",
    "review reply list" => "bekijk de antwoordlijst",
    "add review reply settings" => "voeg review-antwoordinstellingen toe",
    "keyword" => "zoekwoord",
    "reply" => "antwoord",
    "reply settings is already enabled for this location by" => "antwoordinstellingen zijn al ingeschakeld voor deze locatie door",
    "review reply settings inserted successfully." => "controleer de antwoordinstellingen die met succes zijn ingevoegd.",
    "something went wrong while updating data" => "er is iets misgegaan bij het bijwerken van de gegevens",
    "review reply settings successfully updated." => "Controleer antwoordinstellingen succesvol bijgewerkt.",
    "edit review reply settings" => "bewerk de antwoordinstellingen",
    "review reply settings deleted successfully" => "Controleer antwoordinstellingen succesvol verwijderd",
    "something went wrong while deleting review reply settings" => "er is iets misgegaan bij het verwijderen van de instellingen voor reviewreacties",
    "start date" => "begin datum",
    "action url" => "actie-URL",
    "paypal recurring payment" => "PayPal terugkerende betaling",
    "analytics settings" => "analyse-instellingen",
    "address" => "adres",
    "user type" => "gebruikerstype",
    "package" => "pakket",
    "expiry date" => "vervaldatum",
    "log has been deleted successfully" => "log is succesvol verwijderd",
    "something went wrong, please try again" => "Er is iets mis gegaan, probeer het alstublieft nogmaals",
    "password has been changed successfully" => "wachtwoord is succesvol gewijzigd",
    "email sent successfully" => "e-mail succesvol verzonden",
    "delete junk files" => "verwijder ongewenste bestanden",
    "general settings" => "Algemene instellingen",
    "front-end settings" => "front-end instellingen",
    "smtp settings" => "smtp-instellingen",
    "advertisement settings" => "advertentie-instellingen",
    "password" => "wachtwoord",
    "active" => "actief",
    "inactive" => "inactief",
    "edit" => "Bewerk",
    "change password" => "verander wachtwoord",
    "delete" => "verwijderen",
    "activity" => "werkzaamheid",
    "add user" => "Voeg gebruiker toe",
    "edit user" => "bewerk gebruiker",
    "login log" => "login log",
    "email template settings" => "e-mailsjablooninstellingen",
    "pending" => "in afwachting",
    "processing" => "verwerken",
    "completed" => "voltooid",
    "confirm password" => "bevestig wachtwoord",
    "mobile" => "mobiel",
    "announcement" => "Aankondiging",
    "force https" => "forceer https",
    "add-on has been uploaded successfully. you can activate it from here." => "add-on is succesvol geüpload. je kunt het vanaf hier activeren.",
    "settings" => "instellingen",
    "company name" => "Bedrijfsnaam",
    "company address" => "bedrijfsadres",
    "company email" => "bedrijfse-mailadres",
    "company phone/ mobile" => "bedrijfstelefoon / mobiel",
    "time zone" => "tijdzone",
    "slogan" => "slogan",
    "language" => "taal",
    "product name" => "productnaam",
    "product short name" => "korte naam van het product",
    "master password" => "Master wachtwoord",
    "email sending option" => "e-mail verzenden optie",
    "enable suppordesk" => "suppordesk inschakelen",
    "email" => "e-mail",
    "enable signup form" => "aanmeldingsformulier inschakelen",
    "file upload limit" => "bestands upload limiet",
    "mailchimp list" => "mailchimp lijst",
    "email address" => "e-mailadres",
    "smtp host" => "smtp-host",
    "smtp port" => "smtp-poort",
    "smtp user" => "smtp-gebruiker",
    "smtp password" => "smtp wachtwoord",
    "connection type" => "connectie type",
    "status" => "toestand",
    "user manager" => "gebruikersmanager",
    "never" => "nooit",
    "full name" => "voor-en achternaam",
    "activity log" => "activiteiten logboek",
    "published" => "gepubliceerd",
    "action type" => "actie type",
    "location name is required" => "locatienaam is vereist",
    "subscriber boradcast" => "abonnee boradcast",
    "cta" => "cta",
    "posting status" => "boekingsstatus",
    "event" => "evenement",
    "offer" => "aanbod",
    "visit post" => "bezoek post",
    "not published yet." => "nog niet gepubliceerd.",
    "campaign report" => "campagnerapport",
    "edit campaign" => "campagne bewerken",
    "only pending and scheduled campaigns are editable" => "alleen lopende en geplande campagnes kunnen worden bewerkt",
    "delete campaign" => "campagne verwijderen",
    "post type" => "berichttype",
    "created at" => "gemaakt bij",
    "location name is invalid" => "locatienaam is ongeldig",
    "video slide" => "videodia",
    "module limit is over." => "module limiet is voorbij.",
    "campaign updated successfully." => "campagne succesvol bijgewerkt.",
    "something went wrong while submitting campaign." => "er is iets misgegaan bij het indienen van de campagne.",
    "campaign submitted successfully." => "campagne succesvol ingediend.",
    "something went wrong while deleting post!" => "er is iets misgegaan bij het verwijderen van de post!",
    "campaigns" => "campagnes",
    "post campaign list" => "post campagnelijst",
    "not scheduled" => "niet gepland",
    "please select" => "selecteer alstublieft",
    "create campaign" => "campagne maken",
    "no data found for this campaign" => "geen gegevens gevonden voor deze campagne",
    "posted to locations" => "geplaatst op locaties",
    "campaign type" => "campagnetype",
    "quick boradcast" => "snelle boradcast",
    "carousel" => "carrousel",
    "draft" => "droogte",
    "auto reply set date" => "datum automatisch antwoord instellen",
    "options" => "opties",
    "mark seen" => "mark gezien",
    "add announcement" => "aankondiging toevoegen",
    "title" => "titel",
    "description" => "Beschrijving",
    "edit announcement" => "aankondiging bewerken",
    "this feature is disabled in this demo." => "deze functie is uitgeschakeld in deze demo.",
    "announcement has been deleted successfully" => "aankondiging is succesvol verwijderd",
    "announcement details" => "aankondiging details",
    "no data found" => "geen data gevonden",
    "access denied" => "toegang geweigerd",
    "announcement has been marked as seen sucessfully." => "aankondiging is gemarkeerd als succesvol gezien.",
    "unseen announcements have been marked as seen." => "ongeziene aankondigingen zijn gemarkeerd als gezien.",
    "type" => "type",
    "video" => "video",
    "number to send" => "nummer om te verzenden",
    "page name" => "paginanaam",
    "stopped" => "gestopt",
    "page" => "bladzijde",
    "you can edit the campaign" => "u kunt de campagne bewerken",
    "only pending campaigns are editable" => "alleen lopende campagnes kunnen worden bewerkt",
    "activity calendar" => "activiteitenkalender",
    "campaign name" => "campagne naam",
    "auto comment set date" => "datum voor automatisch commentaar instellen",
    "bulk" => "bulk",
    "text" => "tekst",
    "link" => "koppeling",
    "image" => "beeld",
    "paypal email" => "Paypal E-mail",
    "review reply has been deleted successfully" => "review antwoord is succesvol verwijderd",
    "paypal sandbox mode" => "paypal sandbox-modus",
    "language name" => "taal naam",
    "do you really want to mark all unseen notifications as seen?" => "wilt u echt alle ongeziene meldingen markeren als gezien?",
    "generate api key" => "genereer api key",
    "get your api key" => "haal je api-sleutel",
    "re-generate api key" => "opnieuw genereren api-sleutel",
    "your api key" => "uw API-sleutel",
    "every cron url must contain the api key for authentication purpose. generate your api key to see the cron job list." => "elke cron-url moet de api-sleutel bevatten voor authenticatiedoeleinden. genereer uw api-sleutel om de cron-takenlijst te zien.",
    "membership expiration alert & delete junk data" => "lidmaatschap verlopen waarschuwing & verwijder ongewenste gegevens",
    "once/day" => "eens per dag",
    "auto review reply" => "antwoord automatisch beoordelen",
    "once/5 minutes" => "een keer / 5 minuten",
    "post to location" => "posten op locatie",
    "once/10 minutes" => "eenmaal / 10 minuten",
    "add language" => "taal toevoegen",
    "seen" => "gezien",
    "add-ons languages" => "add-ons talen",
    "3rd party languages" => "Talen van derden",
    "plugin languages" => "plugin talen",
    "please put a language name & then save." => "Voer een taalnaam in en sla op.",
    "only characters and underscores are allowed." => "alleen tekens en onderstrepingstekens zijn toegestaan.",
    "sorry, this language already exists, you can not add this again." => "sorry, deze taal bestaat al, u kunt deze niet opnieuw toevoegen.",
    "add language translation" => "taalvertaling toevoegen",
    "system languages" => "systeemtalen",
    "download this as backup" => "download dit als back-up",
    "download" => "downloaden",
    "update this language" => "update deze taal",
    "sorry, english language can not be deleted." => "Sorry, Engelse taal kan niet worden verwijderd.",
    "this is your default language, it can not be deleted." => "dit is uw standaardtaal, deze kan niet worden verwijderd.",
    "mark all unseen as seen" => "markeer alles ongezien zoals gezien",
    "unseen" => "ongezien",
    "do you really want to delete this language? it will delete all files of this language." => "wil je deze taal echt verwijderen? het zal alle bestanden van deze taal verwijderen.",
    "deactive add-on?" => "deactieve add-on?",
    "are you sure?" => "Weet je het zeker?",
    "search..." => "zoeken...",
    "search" => "zoeken",
    "ticket" => "ticket",
    "no reply found." => "geen antwoord gevonden.",
    "reply ticket" => "antwoord ticket",
    "system" => "systeem",
    "activate" => "activeren",
    "deactivate" => "deactiveren",
    "Review Replies" => "Bekijk antwoorden",
);