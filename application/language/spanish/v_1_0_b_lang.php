<?php 
$lang = array(
	"local post viewed" => "publicación local vista",
	"cta button clicked" => "botón cta hecho clic",
	"location information" => "Información sobre la ubicación",
	"location list" => "lista de ubicaciones",
	"new review url" => "nueva url de revisión",
	"post analytics" => "post analytics",
	"coupon code:" => "Código promocional:",
	"queries chain" => "cadena de consultas",
	"queries direct" => "consultas directas",
	"end date:" => "fecha final:",
	"the number of times a resource was shown as a result of a search for the chain it belongs to, or a brand it sells. for example, starbucks, adidas. this is a subset of queries_indirect." => "la cantidad de veces que se mostró un recurso como resultado de una búsqueda de la cadena a la que pertenece o una marca que vende. por ejemplo, starbucks, adidas. Este es un subconjunto de queries_indirect.",
	"generic" => "genérico",
	"message for generic reply." => "mensaje de respuesta genérica.",
	"spintax" => "spintax",
	"do you really want to cancel this template?" => "¿De verdad quieres cancelar esta plantilla?",
	"warning!" => "¡advertencia!",
	"add more settings" => "agregar más configuraciones",
	"add settings" => "agregar configuraciones",
	"remove" => "eliminar",
	"star" => "estrella",
	"message for generic reply" => "mensaje de respuesta genérica",
	"update settings" => "ajustes de actualización",
	"the number of times the resource was shown when searching for the location directly." => "la cantidad de veces que se mostró el recurso al buscar la ubicación directamente.",
	"the number of times the resource was shown as a result of a categorical search (for example, restaurant)." => "la cantidad de veces que se mostró el recurso como resultado de una búsqueda categórica (por ejemplo, restaurante).",
	"the number of times the resource was viewed on google maps." => "la cantidad de veces que se vio el recurso en google maps.",
	"for" => "para",
	"the number of times the resource was viewed on google search." => "la cantidad de veces que se vio el recurso en la búsqueda de google.",
	"the number of times the website was clicked." => "la cantidad de veces que se hizo clic en el sitio web.",
	"the number of times the phone number was clicked." => "la cantidad de veces que se hizo clic en el número de teléfono.",
	"the number of times driving directions were requested." => "la cantidad de veces que se solicitaron indicaciones para llegar en automóvil.",
	"the number of views on media items uploaded by the merchant." => "El número de vistas en los elementos multimedia cargados por el comerciante.",
	"the number of views on media items uploaded by customers." => "El número de vistas en los elementos multimedia subidos por los clientes.",
	"the total number of media items that are currently live that have been uploaded by the merchant." => "la cantidad total de elementos de medios que están actualmente en vivo que el comerciante ha subido.",
	"the total number of media items that are currently live that have been uploaded by customers." => "El número total de elementos de medios que están actualmente en vivo y que los clientes han subido.",
	"the number of times the local post was viewed on google search." => "la cantidad de veces que se vio la publicación local en la búsqueda de Google.",
	"the number of times the call to action button was clicked on google." => "la cantidad de veces que se hizo clic en el botón de llamado a la acción en google.",
	"phone number clicked" => "número de teléfono en el que se hizo clic",
	"driving directions requested" => "direcciones de manejo solicitadas",
	"media uploaded by customers viewed" => "medios subidos por clientes vistos",
	"start date:" => "fecha de inicio:",
	"ok" => "Okay",
	"campaign id" => "ID de campaña",
	"if you schedule a campaign, system will automatically process this campaign at mentioned time and time zone. schduled campaign may take upto 1 hour longer than your schedule time depending on server's processing." => "si programa una campaña, el sistema procesará automáticamente esta campaña en la hora y zona horaria mencionadas. La campaña programada puede demorar hasta 1 hora más que el tiempo programado, dependiendo del procesamiento del servidor.",
	"edit profile" => "Editar perfil",
	"please select time zone" => "por favor seleccione zona horaria",
	"delete my account" => "borrar mi cuenta",
	"your subscription package is expired" => "su paquete de suscripción ha caducado",
	"to get access back please contact system admin." => "para volver a acceder, comuníquese con el administrador del sistema.",
	"read more" => "Lee mas",
	"limit" => "límite",
	"used" => "usado",
	"usage details" => "detalles de uso",
	"campaign" => "Campaña",
	"cta/event/offer posts" => "publicaciones de cta / evento / oferta",
	"support spintax" => "soporte spintax",
	"posting time" => "tiempo de publicación",
	"redeem online" => "canjear en línea",
	"change information about yourself on this page." => "cambiar información sobre usted en esta página.",
	"the number that you used to register on google my business will be set in the call now button." => "El número que utilizó para registrarse en Google My Business se establecerá en el botón Llamar ahora.",
	"action url:" => "URL de acción:",
	"post title:" => "título de la entrada:",
	"date range" => "rango de fechas",
	"to" => "a",
	"redeem url:" => "canjear url:",
	"type summery here..." => "escriba veraniego aquí ...",
	"post now" => "publicar ahora",
	"preview" => "avance",
	"update campaign" => "campaña de actualización",
	"create new post" => "crear nueva publicación",
	"all posts" => "todos los mensajes",
	"delete account" => "borrar cuenta",
	"hi" => "Hola",
	"answer to question" => "responder a la pregunta",
	"answer now" => "responde ahora",
	"question & answer report may take upto few minutes/hours to update & synchronize here." => "El informe de preguntas y respuestas puede tardar hasta unos minutos / horas en actualizarse y sincronizarse aquí.",
	"update answer" => "actualizar respuesta",
	"delete answer" => "borrar respuesta",
	"the answer will be deleted from your google account" => "la respuesta se eliminará de tu cuenta de google",
	"update reply to review" => "actualizar respuesta a revisión",
	"delete review reply" => "eliminar revisión responder",
	"review report may take upto few minutes/hours to update & synchronize here." => "El informe de revisión puede tardar unos minutos / horas en actualizarse y sincronizarse aquí.",
	"update review reply" => "actualizar revisión respuesta",
	"anonymous" => "anónimo",
	"reply to review" => "responder a la reseña",
	"reply now" => "Contesta ahora",
	"do you really want to delete the review reply from google and database?" => "¿realmente quieres eliminar la respuesta de revisión de google y la base de datos?",
	"do you really want to delete this review reply settings from the database?" => "¿realmente desea eliminar esta configuración de respuesta de revisión de la base de datos?",
	"deleting your account will delete all your data from our system and this account can not be restored again. do you really want to delete your account?" => "Al eliminar su cuenta, se eliminarán todos sus datos de nuestro sistema y esta cuenta no se puede restaurar nuevamente. ¿De verdad quieres eliminar tu cuenta?",
	"all stars" => "todas las estrellas",
	"photo" => "foto",
	"comment" => "comentario",
	"replied at" => "respondió a",
	"do you really want to delete the reply to review from the database?" => "¿realmente desea eliminar la respuesta a la revisión de la base de datos?",
	"select package" => "seleccione paquete",
	"payment options" => "opciones de pago",
	"last payment" => "ultimo pago",
	"recurring" => "periódico",
	"manual" => "manual",
	"subscription message" => "mensaje de suscripción",
	"you have already a subscription enabled in paypal. if you want to use different paypal or different package, make sure to cancel your previous subscription from your paypal." => "Ya tienes una suscripción habilitada en PayPal. Si desea utilizar un paquete diferente de PayPal o diferente, asegúrese de cancelar su suscripción anterior de PayPal.",
	"delete account?" => "¿borrar cuenta?",
	"success!" => "¡éxito!",
	"linkedin" => "linkedin",
	"(max 5mb)" => "(max 5mb)",
	"credential check" => "verificación de credenciales",
	"sign up" => "Regístrate",
	"name" => "nombre",
	"captcha" => "captcha",
	"invalid captcha" => "Captcha inválido",
	"to activate your account please perform the following steps" => "para activar su cuenta, realice los siguientes pasos",
	"activate your account" => "activa tu cuenta",
	"account activation" => "activación de cuenta",
	"message subject" => "Asunto del mensaje",
	"message" => "mensaje",
	"sorry, your bulk limit is exceeded for this module." => "lo sentimos, se ha excedido su límite de volumen para este módulo.",
	"click here to see usage log" => "haga clic aquí para ver el registro de uso",
	"sorry, your monthly limit is exceeded for this module." => "lo sentimos, su límite mensual se ha excedido para este módulo.",
	"your account and all of it's corresponding locations and campaigns have been deleted successfully." => "su cuenta y todas sus ubicaciones y campañas correspondientes se han eliminado con éxito.",
	"link and code will be expired after 24 hours" => "el enlace y el código caducarán después de 24 horas",
	"to reset your password please perform the following steps" => "para restablecer su contraseña, realice los siguientes pasos",
	"add-on purchase code has not been provided." => "no se ha proporcionado el código de compra adicional.",
	"purchase code is not valid or already used." => "El código de compra no es válido o ya está en uso.",
	"something went wrong. curl is not working." => "algo salió mal. el rizo no funciona.",
	"add-on unique name has not been provided." => "No se ha proporcionado el nombre exclusivo del complemento.",
	"add-on is already active. duplicate unique name found." => "El complemento ya está activo. nombre único duplicado encontrado.",
	"access forbidden" => "Acceso Prohibido",
	"add-on controller has not been provided." => "no se ha proporcionado el controlador adicional.",
	"add-on controller not found." => "controlador adicional no encontrado.",
	"database error. something went wrong." => "Error de la base de datos. algo salió mal.",
	"add-on has been activated successfully." => "El complemento se ha activado correctamente.",
	"add-on has been deactivated successfully." => "El complemento se ha desactivado con éxito.",
	"add-on has been deleted successfully." => "El complemento se ha eliminado con éxito.",
	"the email was sent by" => "el correo electrónico fue enviado por",
	"reset your password" => "Restablecer su contraseña",
	"we could not find any data." => "No pudimos encontrar ningún dato.",
	"location insights" => "información de ubicación",
	"end date" => "fecha final",
	"coupon code" => "Código promocional",
	"coupon code must be alphanumeric characters" => "el código de cupón debe ser caracteres alfanuméricos",
	"redeem url" => "canjear url",
	"schedule time" => "tiempo programado",
	"timezone" => "zona horaria",
	"summary" => "resumen",
	"media url" => "url de medios",
	"location name" => "nombre del lugar",
	"it was a bad request!" => "fue una mala solicitud!",
	"post id" => "ID del mensaje",
	"bad request" => "solicitud incorrecta",
	"post insights" => "publicar ideas",
	"old password" => "Contraseña anterior",
	"enter this code" => "ingrese este código",
	"new password" => "nueva contraseña",
	"please login with new password" => "por favor inicie sesión con una nueva contraseña",
	"the old password you have given is wrong" => "la contraseña anterior que has dado es incorrecta",
	"cron job" => "trabajo cron",
	"theme manager" => "gerente de tema",
	"dashboard" => "tablero",
	"you do not have permission to access this content" => "no tienes permiso para acceder a este contenido",
	"admin panel" => "Panel de administrador",
	"login" => "iniciar sesión",
	"invalid email or password" => "correo electrónico o contraseña no válidos",
	"account name or email not found." => "nombre de cuenta o correo electrónico no encontrado.",
	"password recovery" => "recuperación de contraseña",
	"go to this url" => "ve a esta url",
	"something went wrong, please try again." => "Algo salió mal. Por favor, vuelva a intentarlo.",
	"review reply settings" => "revisar la configuración de respuesta",
	"post title" => "título de la entrada",
	"question id" => "ID de pregunta",
	"review id" => "revisar id",
	"star rating must be one of five, four, three, two and one" => "la calificación de estrellas debe ser uno de cinco, cuatro, tres, dos y uno",
	"reply message" => "mensaje de respuesta",
	"review name" => "nombre de revisión",
	"profile photo" => "Foto de perfil",
	"you do not have permission to reply to the review" => "no tienes permiso para responder a la revisión",
	"submit" => "enviar",
	"reply to the review updated successfully" => "responder a la revisión actualizada con éxito",
	"invalid review reply id provided" => "ID de respuesta de revisión no válida proporcionado",
	"review reply has been deleted successfully" => "la respuesta de revisión se ha eliminado con éxito",
	"questions list" => "lista de preguntas",
	"invalid question id provided" => "ID de pregunta no válida proporcionado",
	"answer" => "responder",
	"invalid review id provided" => "ID de revisión no válida proporcionado",
	"you do not have permission to answer the question" => "no tienes permiso para responder la pregunta",
	"answer to the question created successfully" => "responder a la pregunta creada con éxito",
	"the answer to the question has been deleted successfully" => "la respuesta a la pregunta se ha eliminado con éxito",
	"review report" => "Reporte de revisión",
	"profile" => "perfil",
	"language editor" => "editor de idiomas",
	"new language" => "nuevo idioma",
	"index (english)" => "índice (inglés)",
	"translation" => "Traducción",
	"your data has been successfully saved." => "sus datos han sido guardados exitosamente",
	"update language" => "idioma de actualización",
	"your data has been updated successfully." => "sus datos han sido actualizados exitosamente",
	"click to delete" => "haga clic para eliminar",
	"review reply type is invalid" => "el tipo de respuesta de revisión no es válido",
	"reviews list" => "lista de comentarios",
	"change settings" => "cambiar ajustes",
	"no data found." => "datos no encontrados.",
	"review list" => "lista de revisión",
	"post list" => "lista de publicaciones",
	"questions & ans." => "preguntas y respuestas",
	"copy the below url for further use." => "copie la siguiente URL para su uso posterior.",
	"report" => "reporte",
	"keyword is required" => "se requiere palabra clave",
	"reply is required" => "se requiere respuesta",
	"star rating" => "calificación de estrellas",
	"reply type" => "tipo de respuesta",
	"generic message" => "mensaje genérico",
	"message for no match" => "mensaje de no coincidencia",
	"update reply" => "actualizar respuesta",
	"delete reply" => "borrar respuesta",
	"location manager" => "Gerente de locación",
	"posts list" => "lista de publicaciones",
	"copied!" => "copiado!",
	"copy" => "Copiar",
	"review reply list" => "revisar lista de respuestas",
	"add review reply settings" => "agregar configuración de respuesta de revisión",
	"keyword" => "palabra clave",
	"reply" => "respuesta",
	"reply settings is already enabled for this location by" => "la configuración de respuesta ya está habilitada para esta ubicación por",
	"review reply settings inserted successfully." => "Revise la configuración de respuesta insertada correctamente.",
	"something went wrong while updating data" => "algo salió mal al actualizar los datos",
	"review reply settings successfully updated." => "revisar la configuración de respuesta actualizada correctamente.",
	"edit review reply settings" => "editar configuración de respuesta de revisión",
	"review reply settings deleted successfully" => "revisar la configuración de respuesta eliminada correctamente",
	"something went wrong while deleting review reply settings" => "algo salió mal al eliminar la configuración de respuesta de revisión",
	"start date" => "fecha de inicio",
	"action url" => "URL de acción",
	"paypal recurring payment" => "pago recurrente de paypal",
	"analytics settings" => "configuración de análisis",
	"address" => "habla a",
	"user type" => "tipo de usuario",
	"package" => "paquete",
	"expiry date" => "fecha de caducidad",
	"log has been deleted successfully" => "el registro se ha eliminado con éxito",
	"something went wrong, please try again" => "Algo salió mal. Por favor, vuelva a intentarlo",
	"password has been changed successfully" => "la contraseña ha sido cambiada exitosamente",
	"email sent successfully" => "correo electrónico enviado con éxito",
	"delete junk files" => "eliminar archivos basura",
	"general settings" => "Configuración general",
	"front-end settings" => "configuración de front-end",
	"smtp settings" => "configuración de smtp",
	"advertisement settings" => "ajustes de publicidad",
	"password" => "contraseña",
	"active" => "activo",
	"inactive" => "inactivo",
	"edit" => "editar",
	"change password" => "Cambia la contraseña",
	"delete" => "Eliminar",
	"activity" => "actividad",
	"add user" => "agregar usuario",
	"edit user" => "editar usuario",
	"login log" => "registro de inicio de sesión",
	"email template settings" => "configuración de plantilla de correo electrónico",
	"pending" => "pendiente",
	"processing" => "Procesando",
	"completed" => "terminado",
	"confirm password" => "Confirmar contraseña",
	"mobile" => "móvil",
	"announcement" => "anuncio",
	"force https" => "forzar https",
	"add-on has been uploaded successfully. you can activate it from here." => "El complemento se ha subido correctamente. Puedes activarlo desde aquí.",
	"settings" => "ajustes",
	"company name" => "nombre de empresa",
	"company address" => "Dirección de la empresa",
	"company email" => "correo electrónico de la empresa",
	"company phone/ mobile" => "teléfono de la empresa / móvil",
	"time zone" => "zona horaria",
	"slogan" => "eslogan",
	"language" => "idioma",
	"product name" => "nombre del producto",
	"product short name" => "nombre corto del producto",
	"master password" => "contraseña maestra",
	"email sending option" => "opción de envío de correo electrónico",
	"enable suppordesk" => "habilitar soporte",
	"email" => "correo electrónico",
	"enable signup form" => "habilitar formulario de registro",
	"file upload limit" => "límite de carga de archivos",
	"mailchimp list" => "lista de mailchimp",
	"email address" => "dirección de correo electrónico",
	"smtp host" => "host smtp",
	"smtp port" => "puerto smtp",
	"smtp user" => "usuario smtp",
	"smtp password" => "contraseña smtp",
	"connection type" => "tipo de conección",
	"status" => "estado",
	"user manager" => "administrador de usuarios",
	"never" => "Nunca",
	"full name" => "nombre completo",
	"activity log" => "registro de actividades",
	"published" => "publicado",
	"action type" => "tipo de acción",
	"location name is required" => "se requiere el nombre de la ubicación",
	"subscriber boradcast" => "boradcast de suscriptor",
	"cta" => "cta",
	"posting status" => "estado de publicación",
	"event" => "evento",
	"offer" => "oferta",
	"visit post" => "puesto de visita",
	"not published yet." => "No publicado aún.",
	"campaign report" => "informe de campaña",
	"edit campaign" => "editar campaña",
	"only pending and scheduled campaigns are editable" => "solo las campañas pendientes y programadas son editables",
	"delete campaign" => "eliminar campaña",
	"post type" => "tipo de mensaje",
	"created at" => "Creado en",
	"location name is invalid" => "el nombre de la ubicación no es válido",
	"video slide" => "diapositiva de video",
	"module limit is over." => "el límite del módulo ha terminado.",
	"campaign updated successfully." => "campaña actualizada con éxito.",
	"something went wrong while submitting campaign." => "Algo salió mal al enviar la campaña.",
	"campaign submitted successfully." => "campaña enviada con éxito.",
	"something went wrong while deleting post!" => "Algo salió mal al eliminar la publicación.",
	"campaigns" => "campañas",
	"post campaign list" => "publicar lista de campañas",
	"not scheduled" => "no programada",
	"please select" => "por favor seleccione",
	"create campaign" => "crear campaña",
	"no data found for this campaign" => "no se encontraron datos para esta campaña",
	"posted to locations" => "publicado en ubicaciones",
	"campaign type" => "tipo de campaña",
	"quick boradcast" => "boradcast rápido",
	"carousel" => "carrusel",
	"draft" => "sequía",
	"auto reply set date" => "fecha establecida de respuesta automática",
	"options" => "opciones",
	"mark seen" => "marca vista",
	"add announcement" => "agregar anuncio",
	"title" => "título",
	"description" => "descripción",
	"edit announcement" => "editar anuncio",
	"this feature is disabled in this demo." => "Esta función está deshabilitada en esta demostración.",
	"announcement has been deleted successfully" => "el anuncio se ha eliminado con éxito",
	"announcement details" => "detalles del anuncio",
	"no data found" => "datos no encontrados",
	"access denied" => "acceso denegado",
	"announcement has been marked as seen sucessfully." => "El anuncio ha sido marcado como visto con éxito.",
	"unseen announcements have been marked as seen." => "anuncios invisibles han sido marcados como vistos.",
	"type" => "tipo",
	"video" => "vídeo",
	"number to send" => "número para enviar",
	"page name" => "Nombre de la página",
	"stopped" => "detenido",
	"page" => "página",
	"you can edit the campaign" => "puedes editar la campaña",
	"only pending campaigns are editable" => "solo las campañas pendientes son editables",
	"activity calendar" => "calendario de actividades",
	"campaign name" => "Nombre de campaña",
	"auto comment set date" => "fecha establecida de comentario automático",
	"bulk" => "abultar",
	"text" => "texto",
	"link" => "enlace",
	"image" => "imagen",
	"paypal email" => "e-mail de Paypal",
	"unable to update review-reply data to database" => "no se pueden actualizar los datos de revisión-respuesta a la base de datos",
	"paypal sandbox mode" => "modo sandbox de paypal",
	"language name" => "Nombre del lenguaje",
	"do you really want to mark all unseen notifications as seen?" => "¿de verdad quieres marcar todas las notificaciones invisibles como se ve?",
	"generate api key" => "generar clave api",
	"get your api key" => "consigue tu clave de API",
	"re-generate api key" => "regenerar clave api",
	"your api key" => "su clave de API",
	"every cron url must contain the api key for authentication purpose. generate your api key to see the cron job list." => "cada URL de cron debe contener la clave de API para fines de autenticación. genera tu clave de API para ver la lista de trabajos cron.",
	"membership expiration alert & delete junk data" => "alerta de vencimiento de membresía y eliminar datos basura",
	"once/day" => "Una vez al día",
	"auto review reply" => "respuesta de revisión automática",
	"once/5 minutes" => "una vez / 5 minutos",
	"post to location" => "publicar en la ubicación",
	"once/10 minutes" => "una vez / 10 minutos",
	"add language" => "agregar idioma",
	"seen" => "visto",
	"add-ons languages" => "idiomas de complementos",
	"3rd party languages" => "Idiomas de terceros",
	"plugin languages" => "idiomas de complementos",
	"please put a language name & then save." => "ponga un nombre de idioma y luego guarde.",
	"only characters and underscores are allowed." => "solo se permiten caracteres y guiones bajos.",
	"sorry, this language already exists, you can not add this again." => "lo siento, este idioma ya existe, no puede agregarlo nuevamente.",
	"add language translation" => "agregar traducción de idiomas",
	"system languages" => "idiomas del sistema",
	"download this as backup" => "descarga esto como copia de seguridad",
	"download" => "descargar",
	"update this language" => "actualizar este idioma",
	"sorry, english language can not be deleted." => "lo siento, el idioma inglés no se puede eliminar.",
	"this is your default language, it can not be deleted." => "Este es su idioma predeterminado, no se puede eliminar.",
	"mark all unseen as seen" => "marcar todo lo invisible como se ve",
	"unseen" => "invisible",
	"do you really want to delete this language? it will delete all files of this language." => "¿De verdad quieres eliminar este idioma? eliminará todos los archivos de este idioma.",
	"deactive add-on?" => "complemento desactivativo?",
	"are you sure?" => "¿Estás seguro?",
	"search..." => "buscar...",
	"search" => "buscar",
	"ticket" => "boleto",
	"no reply found." => "no se encontró respuesta",
	"reply ticket" => "boleto de respuesta",
	"system" => "sistema",
	"activate" => "activar",
	"deactivate" => "desactivar",
);

 ?>