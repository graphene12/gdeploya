<?php 

$lang = array(
"local post viewed" => "местный пост просмотрен",
"cta button clicked" => "нажата кнопка cta",
"location information" => "Информация о местонахождении",
"location list" => "список местоположений",
"new review url" => "URL нового обзора",
"post analytics" => "пост-аналитика",
"coupon code:" => "код купона:",
"queries chain" => "цепочка запросов",
"queries direct" => "запросы прямые",
"end date:" => "Дата окончания:",
"the number of times a resource was shown as a result of a search for the chain it belongs to, or a brand it sells. for example, starbucks, adidas. this is a subset of queries_indirect." => "количество показов ресурса в результате поиска цепочки, к которой он принадлежит, или бренда, который он продает. например, Starbucks, Adidas. это подмножество query_indirect.",
"generic" => "общий",
"message for generic reply." => "сообщение для общего ответа.",
"spintax" => "spintax",
"do you really want to cancel this template?" => "Вы действительно хотите отменить этот шаблон?",
"warning!" => "предупреждение!",
"add more settings" => "добавить больше настроек",
"add settings" => "добавить настройки",
"remove" => "удалять",
"star" => "звезда",
"message for generic reply" => "сообщение для общего ответа",
"update settings" => "обновить настройки",
"the number of times the resource was shown when searching for the location directly." => "количество раз, когда ресурс показывался при поиске местоположения напрямую.",
"the number of times the resource was shown as a result of a categorical search (for example, restaurant)." => "количество раз, когда ресурс был показан в результате категориального поиска (например, ресторан).",
"the number of times the resource was viewed on google maps." => "количество просмотров ресурса на картах Google.",
"for" => "за",
"the number of times the resource was viewed on google search." => "количество просмотров ресурса в поиске Google.",
"the number of times the website was clicked." => "количество нажатий на веб-сайт.",
"the number of times the phone number was clicked." => "количество нажатий на номер телефона.",
"the number of times driving directions were requested." => "количество направлений движения было запрошено.",
"the number of views on media items uploaded by the merchant." => "количество просмотров мультимедийных элементов, загруженных продавцом.",
"the number of views on media items uploaded by customers." => "количество просмотров медиа-материалов, загруженных клиентами.",
"the total number of media items that are currently live that have been uploaded by the merchant." => "общее количество элементов мультимедиа, которые в данный момент активны и загружены продавцом",
"the total number of media items that are currently live that have been uploaded by customers." => "общее количество элементов мультимедиа, которые в данный момент активны и загружены клиентами.",
"the number of times the local post was viewed on google search." => "количество просмотров местного сообщения в поиске Google.",
"the number of times the call to action button was clicked on google." => "количество нажатий на кнопку призыва к действию в Google.",
"phone number clicked" => "номер телефона нажал",
"driving directions requested" => "запрошенные направления движения",
"media uploaded by customers viewed" => "медиа, загруженные просмотренными клиентами",
"start date:" => "Дата начала:",
"ok" => "Ok",
"campaign id" => "идентификатор кампании",
"if you schedule a campaign, system will automatically process this campaign at mentioned time and time zone. schduled campaign may take upto 1 hour longer than your schedule time depending on server's processing." => "Если вы запланируете кампанию, система автоматически обработает эту кампанию в указанное время и часовой пояс. Кампания по расписанию может занять до 1 часа больше, чем ваше расписание, в зависимости от обработки на сервере.",
"edit profile" => "редактировать профиль",
"please select time zone" => "пожалуйста, выберите часовой пояс",
"delete my account" => "удалите мой аккаунт",
"your subscription package is expired" => "срок вашей подписки истек",
"to get access back please contact system admin." => "Чтобы получить доступ обратно, пожалуйста, свяжитесь с системным администратором.",
"read more" => "Подробнее",
"limit" => "предел",
"used" => "используемый",
"usage details" => "детали использования",
"campaign" => "кампания",
"cta/event/offer posts" => "cta / событие / предложение сообщений",
"support spintax" => "поддержка spintax",
"posting time" => "время публикации",
"redeem online" => "выкупить онлайн",
"change information about yourself on this page." => "изменить информацию о себе на этой странице.",
"the number that you used to register on google my business will be set in the call now button." => "номер, который вы использовали для регистрации в google my business, будет установлен в кнопке «Позвонить сейчас».",
"action url:" => "URL действия:",
"post title:" => "Заголовок поста:",
"date range" => "диапазон дат",
"to" => "в",
"redeem url:" => "выкупить URL:",
"type summery here..." => "введите лето здесь ...",
"post now" => "опубликовать сейчас",
"preview" => "предварительный просмотр",
"update campaign" => "обновить кампанию",
"create new post" => "создать новый пост",
"all posts" => "все сообщения",
"delete account" => "удалить аккаунт",
"hi" => "Здравствуй",
"answer to question" => "ответ на вопрос",
"answer now" => "ответь сейчас",
"question & answer report may take upto few minutes/hours to update & synchronize here." => "Обновление и синхронизация отчета с вопросами и ответами может занять до нескольких минут / часов.",
"update answer" => "обновить ответ",
"delete answer" => "удалить ответ",
"the answer will be deleted from your google account" => "ответ будет удален из вашей учетной записи Google",
"update reply to review" => "обновить ответ на обзор",
"delete review reply" => "удалить отзыв отзыв",
"review report may take upto few minutes/hours to update & synchronize here." => "Обновление отчета может занять до нескольких минут / часов.",
"update review reply" => "обновить отзыв ответить",
"anonymous" => "анонимный",
"reply to review" => "ответ на отзыв",
"reply now" => "ответь сейчас",
"do you really want to delete the review reply from google and database?" => "Вы действительно хотите удалить ответ на отзыв из Google и базы данных?",
"do you really want to delete this review reply settings from the database?" => "Вы действительно хотите удалить этот отзыв настройки ответа из базы данных?",
"deleting your account will delete all your data from our system and this account can not be restored again. do you really want to delete your account?" => "удаление вашей учетной записи удалит все ваши данные из нашей системы, и эта учетная запись не может быть восстановлена ​​снова. Вы действительно хотите удалить свой аккаунт?",
"all stars" => "все звезды",
"photo" => "Фото",
"comment" => "комментарий",
"replied at" => "ответил на",
"do you really want to delete the reply to review from the database?" => "Вы действительно хотите удалить ответ на отзыв из базы данных?",
"select package" => "выберите пакет",
"payment options" => "Варианты оплаты",
"last payment" => "последний платеж",
"recurring" => "повторяющиеся",
"manual" => "руководство",
"subscription message" => "подписное сообщение",
"you have already a subscription enabled in paypal. if you want to use different paypal or different package, make sure to cancel your previous subscription from your paypal." => "у вас уже есть подписка в PayPal. Если вы хотите использовать другой PayPal или другой пакет, не забудьте отменить предыдущую подписку с вашего PayPal.",
"delete account?" => "удалить аккаунт?",
"success!" => "Успех!",
"linkedin" => "LinkedIn",
"(max 5mb)" => "(максимум 5 МБ)",
"credential check" => "проверка полномочий",
"sign up" => "Зарегистрироваться",
"name" => "имя",
"captcha" => "CAPTCHA,",
"invalid captcha" => "неверная капча",
"to activate your account please perform the following steps" => "чтобы активировать свою учетную запись, пожалуйста, выполните следующие действия",
"activate your account" => "Активируйте вашу учетную запись",
"account activation" => "активация аккаунта",
"message subject" => "Тема сообщения",
"message" => "сообщение",
"sorry, your bulk limit is exceeded for this module." => "извините, ваш предел объема превышен для этого модуля.",
"click here to see usage log" => "нажмите здесь, чтобы увидеть журнал использования",
"sorry, your monthly limit is exceeded for this module." => "извините, ваш месячный лимит превышен для этого модуля.",
"your account and all of it's corresponding locations and campaigns have been deleted successfully." => "Ваша учетная запись и все соответствующие местоположения и кампании были успешно удалены.",
"link and code will be expired after 24 hours" => "срок действия ссылки и кода истечет через 24 часа",
"to reset your password please perform the following steps" => "чтобы сбросить пароль, пожалуйста, выполните следующие действия",
"add-on purchase code has not been provided." => "код покупки дополнения не предоставлен.",
"purchase code is not valid or already used." => "Код покупки недействителен или уже используется.",
"something went wrong. curl is not working." => "что-то пошло не так. локон не работает.",
"add-on unique name has not been provided." => "уникальное имя надстройки не предоставлено.",
"add-on is already active. duplicate unique name found." => "надстройка уже активна. найдено уникальное имя.",
"access forbidden" => "доступ запрещен",
"add-on controller has not been provided." => "дополнительный контроллер не был предоставлен.",
"add-on controller not found." => "дополнительный контроллер не найден.",
"database error. something went wrong." => "ошибка базы данных. что-то пошло не так.",
"add-on has been activated successfully." => "Надстройка была успешно активирована.",
"add-on has been deactivated successfully." => "Надстройка была успешно деактивирована.",
"add-on has been deleted successfully." => "Надстройка была успешно удалена.",
"the email was sent by" => "письмо было отправлено",
"reset your password" => "сбросить пароль",
"we could not find any data." => "мы не смогли найти никаких данных.",
"location insights" => "информация о местоположении",
"end date" => "Дата окончания",
"coupon code" => "код купона",
"coupon code must be alphanumeric characters" => "код купона должен состоять из буквенно-цифровых символов",
"redeem url" => "обменять URL",
"schedule time" => "расписание",
"timezone" => "часовой пояс",
"summary" => "резюме",
"media url" => "URL-адрес СМИ",
"location name" => "название местоположения",
"it was a bad request!" => "это была плохая просьба!",
"post id" => "почтовый идентификатор",
"bad request" => "плохой запрос",
"post insights" => "опубликовать идеи",
"old password" => "Старый пароль",
"enter this code" => "введите этот код",
"new password" => "новый пароль",
"please login with new password" => "пожалуйста, войдите с новым паролем",
"the old password you have given is wrong" => "старый пароль вы указали неверно",
"cron job" => "хрон работа",
"theme manager" => "менеджер темы",
"dashboard" => "приборная доска",
"you do not have permission to access this content" => "у вас нет разрешения на доступ к этому контенту",
"admin panel" => "Панель администратора",
"login" => "авторизоваться",
"invalid email or password" => "неверный адрес электронной почты или пароль",
"account name or email not found." => "имя учетной записи или адрес электронной почты не найдены.",
"password recovery" => "восстановление пароля",
"go to this url" => "перейти на этот URL",
"something went wrong, please try again." => "Что-то пошло не так. Пожалуйста, попытайтесь еще раз.",
"review reply settings" => "просмотреть настройки ответа",
"post title" => "Заголовок поста",
"question id" => "идентификатор вопроса",
"review id" => "идентификатор обзора",
"star rating must be one of five, four, three, two and one" => "Звездный рейтинг должен быть один из пяти, четырех, трех, двух и одного",
"reply message" => "ответное сообщение",
"review name" => "название отзыва",
"profile photo" => "Аватар",
"you do not have permission to reply to the review" => "у вас нет разрешения на ответ на отзыв",
"submit" => "Разместить",
"reply to the review updated successfully" => "ответ на обзор успешно обновлен",
"invalid review reply id provided" => "указан неверный идентификатор ответа на отзыв",
"review reply has been deleted successfully" => "отзыв был успешно удален",
"questions list" => "список вопросов",
"invalid question id provided" => "указан неверный идентификатор вопроса",
"answer" => "ответ",
"invalid review id provided" => "указан неверный идентификатор отзыва",
"you do not have permission to answer the question" => "у вас нет разрешения на ответ на вопрос",
"answer to the question created successfully" => "ответ на вопрос создан успешно",
"the answer to the question has been deleted successfully" => "ответ на вопрос был успешно удален",
"review report" => "обзорный отчет",
"profile" => "профиль",
"language editor" => "языковой редактор",
"new language" => "новый язык",
"index (english)" => "индекс (английский)",
"translation" => "перевод",
"your data has been successfully saved." => "Ваши данные были успешно сохранены.",
"update language" => "обновить язык",
"your data has been updated successfully." => "Ваши данные были успешно обновлены.",
"click to delete" => "нажмите, чтобы удалить",
"review reply type is invalid" => "отзыв типа ответа неверен",
"reviews list" => "список отзывов",
"change settings" => "изменить настройки",
"no data found." => "данные не найдены.",
"review list" => "список отзывов",
"post list" => "список сообщений",
"questions & ans." => "вопросы и ответы",
"copy the below url for further use." => "скопируйте ниже URL для дальнейшего использования.",
"report" => "доклад",
"keyword is required" => "ключевое слово обязательно",
"reply is required" => "требуется ответ",
"star rating" => "Звездный рейтинг",
"reply type" => "тип ответа",
"generic message" => "общее сообщение",
"message for no match" => "сообщение не соответствует",
"update reply" => "обновить ответ",
"delete reply" => "удалить ответ",
"location manager" => "менеджер местоположения",
"posts list" => "список сообщений",
"copied!" => "скопировано!",
"copy" => "копия",
"review reply list" => "обзор списка ответов",
"add review reply settings" => "добавить отзыв отзыв настройки",
"keyword" => "ключевое слово",
"reply" => "Ответить",
"reply settings is already enabled for this location by" => "Настройки ответа уже включены для этого местоположения",
"review reply settings inserted successfully." => "Просмотр настроек ответа вставлен успешно.",
"something went wrong while updating data" => "что-то пошло не так при обновлении данных",
"review reply settings successfully updated." => "обзор настроек ответа успешно обновлен.",
"edit review reply settings" => "изменить настройки ответа на отзыв",
"review reply settings deleted successfully" => "Просмотр настроек ответа успешно удален",
"something went wrong while deleting review reply settings" => "что-то пошло не так при удалении настроек ответа на отзыв",
"start date" => "Дата начала",
"action url" => "URL действия",
"paypal recurring payment" => "PayPal регулярный платеж",
"analytics settings" => "настройки аналитики",
"address" => "адрес",
"user type" => "тип пользователя",
"package" => "пакет",
"expiry date" => "Дата истечения срока действия",
"log has been deleted successfully" => "журнал был успешно удален",
"something went wrong, please try again" => "Что-то пошло не так. Пожалуйста, попытайтесь еще раз",
"password has been changed successfully" => "пароль был успешно изменен",
"email sent successfully" => "письмо успешно отправлено",
"delete junk files" => "удалить ненужные файлы",
"general settings" => "общие настройки",
"front-end settings" => "настройки интерфейса",
"smtp settings" => "настройки smtp",
"advertisement settings" => "настройки рекламы",
"password" => "пароль",
"active" => "активный",
"inactive" => "неактивный",
"edit" => "редактировать",
"change password" => "изменить пароль",
"delete" => "удалять",
"activity" => "Мероприятия",
"add user" => "Добавить пользователя",
"edit user" => "редактировать пользователя",
"login log" => "вход в систему",
"email template settings" => "настройки шаблона электронной почты",
"pending" => "в ожидании",
"processing" => "обработка",
"completed" => "завершено",
"confirm password" => "Подтвердите Пароль",
"mobile" => "мобильный",
"announcement" => "объявление",
"force https" => "заставить https",
"add-on has been uploaded successfully. you can activate it from here." => "Надстройка была успешно загружена. Вы можете активировать его отсюда.",
"settings" => "настройки",
"company name" => "Название компании",
"company address" => "Адрес компании",
"company email" => "электронный адрес компании",
"company phone/ mobile" => "телефон компании / мобильный",
"time zone" => "часовой пояс",
"slogan" => "лозунг",
"language" => "язык",
"product name" => "наименование товара",
"product short name" => "краткое название продукта",
"master password" => "основной пароль",
"email sending option" => "опция отправки электронной почты",
"enable suppordesk" => "включить поддержку",
"email" => "Эл. адрес",
"enable signup form" => "включить форму регистрации",
"file upload limit" => "предел загрузки файлов",
"mailchimp list" => "список mailchimp",
"email address" => "Адрес электронной почты",
"smtp host" => "SMTP хост",
"smtp port" => "порт smtp",
"smtp user" => "пользователь smtp",
"smtp password" => "пароль smtp",
"connection type" => "тип соединения",
"status" => "положение дел",
"user manager" => "менеджер пользователей",
"never" => "никогда",
"full name" => "ФИО",
"activity log" => "журнал активности",
"published" => "опубликованный",
"action type" => "тип действия",
"location name is required" => "название места требуется",
"subscriber boradcast" => "подписчик boradcast",
"cta" => "кту",
"posting status" => "статус публикации",
"event" => "событие",
"offer" => "предлагает",
"visit post" => "посещение поста",
"not published yet." => "еще не опубликовано.",
"campaign report" => "отчет о кампании",
"edit campaign" => "редактировать кампанию",
"only pending and scheduled campaigns are editable" => "редактируются только ожидающие и запланированные кампании",
"delete campaign" => "удалить кампанию",
"post type" => "тип сообщения",
"created at" => "создан в",
"location name is invalid" => "название места неверно",
"video slide" => "видео слайд",
"module limit is over." => "предел модуля закончен.",
"campaign updated successfully." => "Кампания успешно обновлена.",
"something went wrong while submitting campaign." => "что-то пошло не так при отправке кампании.",
"campaign submitted successfully." => "кампания успешно отправлена.",
"something went wrong while deleting post!" => "что-то пошло не так при удалении поста!",
"campaigns" => "кампании",
"post campaign list" => "список после кампании",
"not scheduled" => "не планируется",
"please select" => "пожалуйста выберите",
"create campaign" => "создать кампанию",
"no data found for this campaign" => "данные для этой кампании не найдены",
"posted to locations" => "размещены в местах",
"campaign type" => "тип кампании",
"quick boradcast" => "быстрый борадкаст",
"carousel" => "карусель",
"draft" => "черновой вариант",
"auto reply set date" => "автоответчик установил дату",
"options" => "параметры",
"mark seen" => "Марк видел",
"add announcement" => "добавить объявление",
"title" => "заглавие",
"description" => "описание",
"edit announcement" => "редактировать объявление",
"this feature is disabled in this demo." => "эта функция отключена в этой демонстрации.",
"announcement has been deleted successfully" => "объявление было успешно удалено",
"announcement details" => "детали объявления",
"no data found" => "данные не найдены",
"access denied" => "доступ запрещен",
"announcement has been marked as seen sucessfully." => "объявление было помечено как увиденное успешно.",
"unseen announcements have been marked as seen." => "невидимые объявления были помечены как видимые.",
"type" => "тип",
"video" => "видео",
"number to send" => "номер для отправки",
"page name" => "название страницы",
"stopped" => "остановился",
"page" => "страница",
"you can edit the campaign" => "Вы можете редактировать кампанию",
"only pending campaigns are editable" => "редактируются только ожидающие кампании",
"activity calendar" => "календарь активности",
"campaign name" => "Название кампании",
"auto comment set date" => "автокомментарий установить дату",
"bulk" => "насыпной",
"text" => "текст",
"link" => "ссылка на сайт",
"image" => "образ",
"paypal email" => "электронная почта PayPal",
"unable to update review-reply data to database" => "невозможно обновить данные отзыва-ответа в базу данных",
"paypal sandbox mode" => "режим песочницы PayPal",
"language name" => "Название языка",
"do you really want to mark all unseen notifications as seen?" => "Вы действительно хотите пометить все невидимые уведомления как видимые?",
"generate api key" => "генерировать ключ API",
"get your api key" => "получить свой ключ API",
"re-generate api key" => "заново сгенерировать ключ API",
"your api key" => "ваш ключ API",
"every cron url must contain the api key for authentication purpose. generate your api key to see the cron job list." => "каждый URL cron должен содержать ключ api для аутентификации. сгенери свой ключ API, чтобы увидеть список заданий cron.",
"membership expiration alert & delete junk data" => "предупреждение об истечении срока действия членства и удаление ненужных данных",
"once/day" => "однажды",
"auto review reply" => "авто обзор ответа",
"once/5 minutes" => "один раз / 5 минут",
"post to location" => "разместить на месте",
"once/10 minutes" => "один раз / 10 минут",
"add language" => "добавить язык",
"seen" => "видели",
"add-ons languages" => "языки дополнений",
"3rd party languages" => "Сторонние языки",
"plugin languages" => "языки плагинов",
"please put a language name & then save." => "пожалуйста, введите название языка и затем сохраните.",
"only characters and underscores are allowed." => "разрешены только символы и подчеркивания.",
"sorry, this language already exists, you can not add this again." => "извините, этот язык уже существует, вы не можете добавить его снова.",
"add language translation" => "добавить языковой перевод",
"system languages" => "системные языки",
"download this as backup" => "скачать это как резервную копию",
"download" => "скачать",
"update this language" => "обновить этот язык",
"sorry, english language can not be deleted." => "извините, английский язык не может быть удален.",
"this is your default language, it can not be deleted." => "это ваш язык по умолчанию, его нельзя удалить.",
"mark all unseen as seen" => "отметить все невидимое как увиденное",
"unseen" => "невидимый",
"do you really want to delete this language? it will delete all files of this language." => "Вы действительно хотите удалить этот язык? это удалит все файлы этого языка.",
"deactive add-on?" => "неактивное дополнение?",
"are you sure?" => "вы уверены?",
"search..." => "поиск...",
"search" => "поиск",
"ticket" => "проездной билет",
"no reply found." => "ответ не найден.",
"reply ticket" => "ответный билет",
"system" => "система",
"activate" => "Активизировать",
"deactivate" => "деактивировать",
);

 ?>