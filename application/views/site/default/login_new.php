<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/fresh_changes/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/fresh_changes/images/favicon.ico" type="image/x-icon">
    <title><?php echo $this->config->item('product_name'); ?> - Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/fresh_changes/login/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>assets/fresh_changes/login/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/fresh_changes/login/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url(); ?>assets/fresh_changes/login/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="new-login-register">
        <div class="lg-info-panel">
            <div class="inner-panel">
                <a href="<?php echo base_url(); ?>" class="p-20 di"><img src="<?php echo base_url(); ?>assets/fresh_changes/images/icon.png" width="35" height="35"></a>
            </div>
        </div>
        <div class="new-login-box">
            <div>
                <a href="<?php echo base_url(); ?>"><img style="margin-left:25px;" width="320.55" height="100" src="<?php echo base_url(); ?>assets/fresh_changes/images/logo.png" alt='<?php echo $this->config->item("product_short_name"); ?>'></a>
            </div>
            <div class="white-box">
                <h3 class="box-title m-b-0">LOGIN</h3>
                <?php
                if ($this->session->flashdata('login_msg') != '') {
                    echo "<div class='alert alert-danger text-center'>";
                    echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                    echo $this->session->flashdata('login_msg');
                    echo "</div>";
                }
                if ($this->session->flashdata('reset_success') != '') {
                    echo "<div class='alert alert-success text-center'>";
                    echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                    echo $this->session->flashdata('reset_success');
                    echo "</div>";
                }
                if ($this->session->userdata('reg_success') != '') {
                    echo '<div class="alert alert-success text-center">';
                    echo '<button type="button" class="close" data-dismiss="alert">&times;</button>' . $this->session->userdata("reg_success") . '</div>';
                    $this->session->unset_userdata('reg_success');
                }
                if (form_error('username') != '' || form_error('password') != "") {
                    $form_error = "";
                    if (form_error('username') != '') $form_error .= form_error('username');
                    if (form_error('password') != '') $form_error .= form_error('password');
                    echo "<div class='alert alert-danger text-center'>" . '<button type="button" class="close" data-dismiss="alert">&times;</button>' . $form_error . "</div>";
                }

                $default_user = $default_pass = "";
                if ($this->is_demo == '1') {
                    $default_user = "admin@xerochat.com";
                    $default_pass = "123456";
                }
                ?>
                <form autocomplete="off" class="form-horizontal new-lg-form needs-validation" id="loginform" method="POST" action="<?php echo base_url('home/login'); ?>" novalidate="">

                    <div class="form-group  m-t-20">
                        <div class="col-xs-12">
                            <label>Email</label>
                            <input id="email" type="email" value="<?php echo $default_user ?>" class="form-control" name="username" tabindex="1" required autofocus placeholder="Email Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Password</label>
                            <input id="password" placeholder="Password" value="<?php echo $default_pass ?>" type="password" class="form-control" name="password" tabindex="2" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button style="background: linear-gradient(180deg, rgba(111,197,254,1) 27%, rgba(111,121,252,1) 100%);color:white;" class="btn btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>
                    <?php if ($this->config->item('enable_signup_form') != '0') : ?>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 text-center" style="margin-top:5px;">
                                <?php echo $google_login_button; ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 text-center" style="margin-top:5px;">
                                <?php echo $fb_login_button2 = str_replace("ThisIsTheLoginButtonForFacebook", $this->lang->line("Sign in with Facebook"), $fb_login_button); ?>
                            </div>

                            <div class="col-12">
                                <div class="text-muted text-center">
                                    <br><?php echo $this->lang->line("Do not have an account?"); ?> <a href="<?php echo base_url('home/sign_up'); ?>"><?php echo $this->lang->line("Create one"); ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <!-- <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social">
                                <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Don't have an account? <a href="register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                        </div>
                    </div> -->
                </form>
                <div id="recovery_form">
                    <form class="form-horizontal" id="recoverform" method="POST">
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" name="email" id="email_reset" type="email" tabindex="1" autofocus required placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button id="password_reset" style="background: linear-gradient(180deg, rgba(111,197,254,1) 27%, rgba(111,121,252,1) 100%);color:white;" class="btn btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>


    </section>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/js/custom.js"></script>

    <script src="<?php echo base_url(); ?>assets/modules/sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript">
        $('document').ready(function() {
            $("#password_reset").click(function(e) {
                e.preventDefault();


                var email = $("#email_reset").val();
                console.log(email);
                if (email == '') {
                    $("#email_reset").addClass('is-invalid');
                    return false;
                } else {
                    $("#email_reset").removeClass('is-invalid');
                }
                $(this).addClass('btn-progress');
                $.ajax({
                    context: this,
                    type: 'POST',
                    url: "<?php echo site_url(); ?>home/code_genaration",
                    data: {
                        email: email
                    },
                    success: function(response) {
                        $(this).removeClass('btn-progress');
                        if (response == '0') {
                            swal('<?php echo $this->lang->line("Error") ?>', '<?php echo $this->lang->line("Invalid email or it is not associated with any user") ?>', 'error');
                        } else {
                            var string = '<div class="alert alert-primary alert-has-icon"><div class="alert-icon"></div><div class="alert-body"><div class="alert-title"><?php echo $this->lang->line("Sent") ?></div><?php echo $this->lang->line("A email containing password reset steps has been sent to your email."); ?></div></div>';
                            $("#recovery_form").slideUp();
                            $("#recovery_form").html(string);
                            $("#recovery_form").slideDown();
                        }
                    }
                });

            });
        });
    </script>

    <script>
        window.customerlySettings = {
            app_id: "af598a25"
        };
        ! function() {
            function e() {
                var e = t.createElement("script");
                e.type = "text/javascript", e.async = !0,
                    e.src = "https://widget.customerly.io/widget/af598a25";
                var r = t.getElementsByTagName("script")[0];
                r.parentNode.insertBefore(e, r)
            }
            var r = window,
                t = document,
                n = function() {
                    n.c(arguments)
                };
            r.customerly_queue = [], n.c = function(e) {
                    r.customerly_queue.push(e)
                },
                r.customerly = n, r.attachEvent ? r.attachEvent("onload", e) : r.addEventListener("load", e, !1)
        }();
    </script>
</body>

</html>