<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/fresh_changes/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/fresh_changes/images/favicon.ico" type="image/x-icon">
    <title><?php echo $this->config->item('product_name'); ?> - Sign up</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/fresh_changes/login/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>assets/fresh_changes/login/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/fresh_changes/login/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url(); ?>assets/fresh_changes/login/css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body style="background-color: #e6e6e6;">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="new-login-register">
        <div class="lg-info-panel">
            <div class="inner-panel">
                <a href="<?php echo base_url(); ?>" class="p-20 di"><img src="<?php echo base_url(); ?>assets/fresh_changes/images/icon.png" width="35" height="35"></a>

            </div>
        </div>
        <div class="new-login-box" style="margin-top:5%;">
            <div>
                <a href="<?php echo base_url(); ?>"><img style="margin-left:25px;" width="320.55" height="100" src="<?php echo base_url(); ?>assets/fresh_changes/images/logo.png" alt='<?php echo $this->config->item("product_short_name"); ?>'></a>
            </div>
            <div class="white-box">
                <h3 class="box-title m-b-0">SIGN UP</h3>
                <?php
                if ($this->session->userdata('reg_success') == 1) {
                    echo "<div class='alert alert-success text-center'>" . $this->lang->line("An activation code has been sent to your email. please check your inbox to activate your account.") . "</div>";
                    $this->session->unset_userdata('reg_success');
                }
                if ($this->session->userdata('reg_success') == 'limit_exceed') {
                    echo "<div class='alert alert-danger text-center'>" . $this->lang->line("Signup has been disabled. Please contact system admin.") . "</div>";
                    $this->session->unset_userdata('reg_success');
                }
                if (form_error('name') != '' || form_error('email') != '' || form_error('confirm_password') != '' || form_error('password') != "") {
                    $form_error = "";
                    if (form_error('name') != '') $form_error .= str_replace(array("<p>", "</p>"), array("", ""), form_error('name')) . "<br>";
                    if (form_error('email') != '') $form_error .= str_replace(array("<p>", "</p>"), array("", ""), form_error('email')) . "<br>";
                    if (form_error('password') != '') $form_error .= str_replace(array("<p>", "</p>"), array("", ""), form_error('password')) . "<br>";
                    if (form_error('confirm_password') != '') $form_error .= str_replace(array("<p>", "</p>"), array("", ""), form_error('confirm_password')) . "<br>";
                    echo "<div class='alert alert-danger text-center'>" . $form_error . "</div>";
                }
                if (form_error('captcha'))
                    echo "<div class='alert alert-danger text-center'>" . form_error('captcha') . "</div>";
                else if ($this->session->userdata("sign_up_captcha_error") != '') {
                    echo "<div class='alert alert-danger text-center'>" . $this->session->userdata("sign_up_captcha_error") . "</div>";
                    $this->session->unset_userdata("sign_up_captcha_error");
                }
                ?>
                <form class="form-horizontal" autocomplete="off" method="POST" action="<?php echo site_url('home/sign_up_action'); ?>">

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <label><?php echo $this->lang->line("Name"); ?> *</label>
                            <input id="name" type="text" class="form-control" name="name" autofocus required value="<?php echo set_value('name'); ?>">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <label><?php echo $this->lang->line("Email"); ?> *</label>
                            <input id="email" type="email" class="form-control" name="email" required value="<?php echo set_value('email'); ?>">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <label><?php echo $this->lang->line("Password"); ?> *</label>
                            <input id="password" type="password" class="form-control" required name="password" value="<?php echo set_value('password'); ?>">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <label><?php echo $this->lang->line("Confirm Password"); ?> *</label>
                            <input id="password2" type="password" class="form-control" required name="confirm_password" value="<?php echo set_value('confirm_password'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?php echo $this->lang->line("Captcha"); ?> *</label>
                        <div class="input-group col-xs-12"> <span class="input-group-addon"><?php echo $num1 . "+" . $num2 . " = ?"; ?></span>
                            <input type="number" class="form-control" required name="captcha" placeholder="<?php echo $this->lang->line("Put your answer here"); ?>">
                        </div>

                    </div>


                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12 m-t-20">
                            <button style="background: linear-gradient(180deg, rgba(111,197,254,1) 27%, rgba(111,121,252,1) 100%);color:white;" class="btn btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Sign up</button>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social">
                                <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                        </div>
                    </div> -->
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Already have an Account? <a href="<?php echo base_url('home/login'); ?>" class="text-info m-l-5"><b>Login</b></a></p>
                        </div>
                    </div>
                </form>

            </div>
        </div>


    </section>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/fresh_changes/login/js/custom.js"></script>

    <script src="<?php echo base_url(); ?>assets/modules/sweetalert/sweetalert.min.js"></script>
    <script>
        window.customerlySettings = {
            app_id: "af598a25"
        };
        ! function() {
            function e() {
                var e = t.createElement("script");
                e.type = "text/javascript", e.async = !0,
                    e.src = "https://widget.customerly.io/widget/af598a25";
                var r = t.getElementsByTagName("script")[0];
                r.parentNode.insertBefore(e, r)
            }
            var r = window,
                t = document,
                n = function() {
                    n.c(arguments)
                };
            r.customerly_queue = [], n.c = function(e) {
                    r.customerly_queue.push(e)
                },
                r.customerly = n, r.attachEvent ? r.attachEvent("onload", e) : r.addEventListener("load", e, !1)
        }();
    </script>
</body>

</html>