<style type="text/css">
    .h5,
    h5 {
        font-size: 1.25rem;
        background-color: #464545;
        color: #fff;
        padding: 10px;
        border-radius: 0 0 5px 5px;
        /* top left, top right, bottom right, bottom left */
    }

    p {
        border-radius: 5px 5px 0 0;
        /* top left, top right, bottom right, bottom left */
        background-color: #6fc5fe;
        min-height: 300px;
        padding-top: 120px;
    }

    .lightboxcontainer {
        width: 100%;
        text-align: left;
    }

    .lightboxleft {
        width: 40%;
        float: left;
    }

    .lightboxright {
        width: 60%;
        float: left;
    }

    .divtext {
        margin: 36px;
    }

    @media (max-width: 800px) {
        .lightboxleft {
            width: 100%;
        }

        .lightboxright {
            width: 100%;
        }

        .divtext {
            margin: 12px;
        }
    }

    #html5-watermark {
        display: none;
    }
</style>
<section class="section section_custom">
    <div class="section-header">
        <h1><i class="fas fa-plus-circle"></i> <?php echo $page_title; ?></h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><?php echo $this->lang->line("Dashboard"); ?></div>
            <div class="breadcrumb-item"><?php echo $page_title; ?></div>
        </div>
    </div>

    <?php $this->load->view('admin/theme/message'); ?>

    <div class="row" style="margin-top: 10px;">

        <div class="col-xl-4">
            <center>
                <p><a href="https://www.youtube.com/embed/Ob2U8FEZiJs" class="html5lightbox" data-group="set1"><img class="center-block img-responsive" src="<?php echo base_url(); ?>assets/video-dark.svg" /></a></p>
                <h5>Tutorial Video 1</h5>
            </center>

        </div>

        <div class="col-xl-4">
            <center>
                <p><a href="https://www.youtube.com/embed/Ob2U8FEZiJs" class="html5lightbox" data-group="set1"><img src="<?php echo base_url(); ?>assets/video-dark.svg" /></a></p>
                <h5>Tutorial Video 2</h5>
            </center>

        </div>

        <div class="col-xl-4">
            <center>
                <p><a href="https://www.youtube.com/embed/Ob2U8FEZiJs" class="html5lightbox" data-group="set1"><img src="<?php echo base_url(); ?>assets/video-dark.svg" /></a></p>
                <h5>Tutorial Video 3</h5>
            </center>

        </div>
        <div class="col-xl-4">
            <center>
                <p><a href="https://www.youtube.com/embed/Ob2U8FEZiJs" class="html5lightbox" data-group="set1"><img src="<?php echo base_url(); ?>assets/video-dark.svg" /></a></p>
                <h5>Tutorial Video 4</h5>
            </center>
        </div>

        <div class="col-xl-4">
            <center>
                <p><a href="https://www.youtube.com/embed/Ob2U8FEZiJs" class="html5lightbox" data-group="set1"><img src="<?php echo base_url(); ?>assets/video-dark.svg" /></a></p>
                <h5>Tutorial Video 5</h5>
            </center>
        </div>


        <div class="col-xl-4">
            <center>
                <p><a href="https://www.youtube.com/embed/Ob2U8FEZiJs" class="html5lightbox" data-group="set1"><img src="<?php echo base_url(); ?>assets/video-dark.svg" /></a></p>
                <h5>Tutorial Video 6</h5>
            </center>

        </div>


    </div>
    </div>
    </div>
    </div>
</section>