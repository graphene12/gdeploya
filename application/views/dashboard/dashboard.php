<?php 
if(isset($system_dashboard))
  $has_system_dashboard = 'yes';
else
  $has_system_dashboard = 'no';
$month_name_array = array(
	'01' => 'January',
	'02' => 'February',
	'03' => 'March',
	'04' => 'April',
	'05' => 'May',
	'06' => 'June',
	'07' => 'July',
	'08' => 'August',
	'09' => 'September',
	'10' => 'October',
	'11' => 'November',
	'12' => 'December'
);
?>
<style>
  .list-unstyled-border li {
    margin-bottom: 45px !important;
  }
  #period_loader {height: 100%;width:100%;display: table;}
  #period_loader i{font-size:60px;display: table-cell; vertical-align: middle;padding:30px 0;}
  
</style>

<section class="section">
  <?php if($other_dashboard == 1) : ?>
    <?php if($system_dashboard == 'yes') : ?>
    <div class="section-header">
      <h1><i class="fas fa-tachometer-alt"></i> <?php echo $this->lang->line('System Dashboard'); ?> </h1>
    </div>
    <?php else : ?>
    <div class="section-header">
      <h1><i class="fas fa-tachometer-alt"></i> <?php echo $this->lang->line('Dashboard for')." ".$user_name." [".$user_email."]"; ?> </h1>
    </div>
    <?php endif; ?>
  <?php endif; ?>
  
  <?php if($other_dashboard == 1) : ?>
  <div class="section-body">
  <?php endif; ?>
    
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2">
          <div class="card-stats">
            <div class="card-stats-title"><?php echo $this->lang->line('Statistics'); ?></div>
            <div class="card-stats-items month_change_middle_content">
              <div class="card-stats-item">
                <div class="card-stats-item-count" id="total_gmb_accounts"><?php echo custom_number_format($total_gmb_accounts); ?></div>
                <div class="card-stats-item-label"><?php echo $this->lang->line('Accounts'); ?></div>
              </div>
              <div class="card-stats-item">
                <div class="card-stats-item-count" id="total_gmb_unique_locations"><?php echo custom_number_format($total_gmb_unique_locations); ?></div>
                <div class="card-stats-item-label"><?php echo $this->lang->line('unique locations'); ?></div>
              </div>
              <div class="card-stats-item">
                <div class="card-stats-item-count" id="total_gmb_posts"><?php echo custom_number_format($total_gmb_posts); ?></div>
                <div class="card-stats-item-label"><?php echo $this->lang->line('Post published'); ?></div>
              </div>
            </div>
          </div>
          <div class="card-icon shadow-primary bg-success">
            <i class="fas fa-reply-all"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header" style="background-color:#fff;">
              <h4><?php echo $this->lang->line('No. of review reply'); ?></h4>
            </div>
            <div class="card-body" id="total_review_reply">
              <?php echo custom_number_format($total_review_reply); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2">
          <div class="card-chart">
            <canvas id="seven_days_reply_chart" height="80"></canvas>
          </div>
          <div class="card-icon shadow-primary bg-info">
            <i class="fas fa-random"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header" style="background-color:#fff;">
              <h4><?php echo $this->lang->line('Last 7 days review replies'); ?></h4>
            </div>
            <div class="card-body">
              <?php echo custom_number_format($seven_days_reviews_gain); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2">
          <div class="card-chart">
            <canvas id="seven_days_post_chart" height="80"></canvas>
          </div>
          <div class="card-icon shadow-primary bg-danger">
            <i class="fas fa-file-signature"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header" style="background-color:#fff;">
              <h4><?php echo $this->lang->line('Last 7 days post published'); ?></h4>
            </div>
            <div class="card-body">
              <?php echo custom_number_format($seven_days_post_gain); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header">
            <h4><i class="fas fa-boxes"></i> <?php echo $this->lang->line('Last 30 days published posts(CTA/OFFER/EVENT)'); ?></h4>
          </div>
          <div class="card-body">
            <canvas id="post_type_chart" height="134"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card ">
          <div class="card-header">
            <h4><i class="fas fa-database"></i> <?php echo $this->lang->line("reviews gain based on type"); ?></h4>
            <!-- <div class="card-header-action dropdown">
              <a href="#" data-toggle="dropdown" class="btn btn-danger dropdown-toggle" id="selected_period"><?php echo $this->lang->line("This Month");?></a>
              <ul class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <li class="dropdown-title"><?php echo $this->lang->line("Select Period");?></li>
                <li><a href="#" class="dropdown-item period_change" period="today"><?php echo $this->lang->line("Today");?></a></li>
                <li><a href="#" class="dropdown-item period_change" period="week"><?php echo $this->lang->line("Last 7 Days");?></a></li>
                <li><a href="#" class="dropdown-item period_change active" period="month"><?php echo $this->lang->line("This Month");?></a></li>
                <li><a href="#" class="dropdown-item period_change" period="year"><?php echo $this->lang->line("This Year");?></a></li>
              </ul>
            </div> -->
          </div>
          <div class="card-body" id="top-5-scroll">
            <div class="text-center waiting hidden" id="period_loader"><i class="fas fa-spinner fa-spin blue text-center" style="font-size: 40px;"></i></div>
            <ul class="list-unstyled list-unstyled-border" id="period_change_content">
              
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?php echo base_url('assets/img/star/Star_5.png'); ?>" alt="5 stars">
                <div class="media-body">
                  <div class="media-title"><?php echo $this->lang->line('5 star reviews gain'); ?></div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-label" id="5_star_review"><?php echo isset($review_type_data['FIVE']) ? number_format($review_type_data['FIVE']) : 0; ?></div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?php echo base_url('assets/img/star/Star_4.png'); ?>" alt="4 stars">
                <div class="media-body">
                  <div class="media-title"><?php echo $this->lang->line('4 star reviews gain'); ?></div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-label" id="4_star_review"><?php echo isset($review_type_data['FOUR']) ? number_format($review_type_data['FOUR']) : 0; ?></div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?php echo base_url('assets/img/star/Star_3.png'); ?>" alt="3 stars">
                <div class="media-body">
                  <div class="media-title"><?php echo $this->lang->line('3 star reviews gain'); ?></div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-label" id="3_star_review"><?php echo isset($review_type_data['THREE']) ? number_format($review_type_data['THREE']) : 0; ?></div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?php echo base_url('assets/img/star/Star_2.png'); ?>" alt="2 stars">
                <div class="media-body">
                  <div class="media-title"><?php echo $this->lang->line('2 star reviews gain'); ?></div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-label" id="2_star_review"><?php echo isset($review_type_data['TWO']) ? number_format($review_type_data['TWO']) : 0; ?></div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="media">
                <img class="mr-3 rounded" width="55" src="<?php echo base_url('assets/img/star/Star_1.png'); ?>" alt="1 stars">
                <div class="media-body">
                  <div class="media-title"><?php echo $this->lang->line('1 star reviews gain'); ?></div>
                  <div class="mt-1">
                    <div class="budget-price">
                      <div class="budget-price-label" id="1_star_review"><?php echo isset($review_type_data['ONE']) ? number_format($review_type_data['ONE']) : 0; ?></div>
                    </div>
                  </div>
                </div>
              </li>

            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7">
        <div class="card">
          <div class="card-header">
            <h4><i class="fas fa-star"></i> <?php echo $this->lang->line('Latest reviews'); ?></h4>
          </div>
          <div class="card-body">
            <div class="owl-carousel owl-theme" id="products-carousel">
              <?php foreach($latest_review_info as $value) : ?>
              <div>
                <div class="product-item pb-3">
                  <div class="product-image">
                    <img alt="image" src="<?php echo $value['reviewer_photo']; ?>" class="rounded-circle">
                  </div>
                  <div class="product-details">
                    <div class="product-name"><?php echo $value['reviewer_name']; ?></div>
                    <div class="product-review">
                      <?php
                        if ('FIVE' == $value['review_star']) {
                            echo '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>';
                        } elseif ('FOUR' == $value['review_star']) {
                            echo '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>';
                        } elseif ('THREE' == $value['review_star']) {
                            echo '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>';
                        } elseif ('TWO' == $value['review_star']) {
                            echo '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>';
                        } elseif ('ONE' == $value['review_star']) {
                            echo '<i class="fas fa-star text-warning"></i>';
                        }
                      ?>
                    </div>
                    <div class="text-muted text-small"><?php echo date_time_calculator($value['reply_time'],true); ?></div>
                    <div class="product-cta">
                      <p><?php echo $value['location_display_name']; ?></p>
                    </div>
                  </div>
                </div>
              </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5">
        <div class="card">
          <div class="card-header"><h4><i class="fas fa-globe"></i> <?php echo $this->lang->line('Total reviews in different locations'); ?></h4></div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6">
                <ul class="list-unstyled list-unstyled-border list-unstyled-noborder mb-0">
                  <?php 
                    for($i=0;$i<3;$i++) : 
                      if(!isset($location_based_reviews[$i]['location_name'])) 
                        continue;
                  ?>
                    <li class="media"> 
                      <img class="img-fluid mt-1" src="<?php echo $location_based_reviews[$i]['location_profile_url']; ?>" alt="image" width="40">
                      <div class="media-body ml-3">
                        <div class="media-title">
                          <a href="<?php echo isset($location_based_reviews[$i]['location_map_url']) ? $location_based_reviews[$i]['location_map_url'] : '#'; ?>" target="_BLANK">
                            <?php echo isset($location_based_reviews[$i]['location_name']) ? $location_based_reviews[$i]['location_name'] : ''; ?>
                          </a>
                        </div>
                        <div class="text-small text-muted"><?php echo isset($location_based_reviews[$i]['total_reviews']) ? number_format($location_based_reviews[$i]['total_reviews']) : 0; echo " "; echo $this->lang->line('Reviews');?></div>
                      </div>
                    </li>
                  <?php endfor; ?>
                </ul>
              </div>
              <div class="col-sm-6">
                <ul class="list-unstyled list-unstyled-border list-unstyled-noborder mb-0">
                  <?php 
                    for($i=3;$i<6;$i++) : 
                      if(!isset($location_based_reviews[$i]['location_name'])) 
                        continue;
                  ?>
                    <li class="media"> 
                      <img class="img-fluid mt-1" src="<?php echo $location_based_reviews[$i]['location_profile_url']; ?>" alt="image" width="40">
                      <div class="media-body ml-3">
                        <div class="media-title">
                          <a href="<?php echo isset($location_based_reviews[$i]['location_map_url']) ? $location_based_reviews[$i]['location_map_url'] : '#'; ?>" target="_BLANK">
                            <?php echo isset($location_based_reviews[$i]['location_name']) ? $location_based_reviews[$i]['location_name'] : ''; ?>
                          </a>
                        </div>
                        <div class="text-small text-muted"><?php echo isset($location_based_reviews[$i]['total_reviews']) ? number_format($location_based_reviews[$i]['total_reviews']) : 0; echo " "; echo $this->lang->line('Reviews');?></div>
                      </div>
                    </li>
                  <?php endfor; ?>
                </ul>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
    <div class="row">
    	<div class="col-md-4">
    	    <div class="card card-hero">
    	      <div class="card-header">
    	        <div class="card-icon">
    	          <i class="far fa-share-square"></i>
    	        </div>
    	        <h4><?php if(count($upcoming_campaigns_info) >= 5) echo 5; else echo count($upcoming_campaigns_info); ?></h4>
    	        <div class="card-description"><?php echo $this->lang->line("Upcoming Post Campaigns") ?></div>
    	      </div>
    	      <div class="card-body p-0">
    	        <div class="tickets-list">
    	          <div class="yscroll" style="height: 300px;">             
    	            <?php 
    	              $post_names = array(
    	                'cta_post' => $this->lang->line('CTA Post'),
                      'event_post' => $this->lang->line('Event Post'),
                      'offer_post' => $this->lang->line('Offer Post'),
    	              ); 
    	            ?>
    	            <?php $sl=0; foreach ($upcoming_campaigns_info as $value) : if($sl == 5) break; ?>              
    	              <a href="#" class="ticket-item">
    	                <div class="ticket-title">
    	                  <h4 class="no_action"><?php echo $value['campaign_name']; ?></h4>
    	                </div>
    	                <div class="ticket-info">

    	                  <div>
    	                    <?php 
    	                      $post_type = '';
    	                      if(isset($value['post_type'])) $post_type = $post_names[$value['post_type']];
    	                      echo $post_type;
    	                    ?>                      
    	                    </div>
    	                  <div class="bullet"></div>
    	                  <div class="text-primary no_action"><?php echo date("d M y H:i",strtotime($value["schedule_time"])); ?></div>
    	                </div>
    	              </a>
    	            <?php $sl++; endforeach ?>
    	          </div>
    	          <a href="<?php echo base_url('campaigns/posts'); ?>" target="_BLANK" class="ticket-item ticket-more">
    	            <?php echo $this->lang->line('View All'); ?> <i class="fas fa-chevron-right"></i>
    	          </a>
    	        </div>
    	      </div>
    	    </div>
    	</div>
    	<div class="col-md-4">
    	    <div class="card card-hero">
    	      <div class="card-header">
    	        <div class="card-icon">
    	          <i class="far fa-share-square"></i>
    	        </div>
    	        <h4><?php if(count($completed_campaigns_info) >= 5) echo 5; else echo count($completed_campaigns_info); ?></h4>
    	        <div class="card-description"><?php echo $this->lang->line("Completed Post Campaigns") ?></div>
    	      </div>
    	      <div class="card-body p-0">
    	        <div class="tickets-list">
    	          <div class="yscroll" style="height: 300px;">
    	            <?php $sl=0; foreach ($completed_campaigns_info as $value) : if($sl == 5) break; ?>              
    	              <a href="#" class="ticket-item">
    	                <div class="ticket-title">
    	                  <h4 class="no_action"><?php echo $value['campaign_name']; ?></h4>
    	                </div>
    	                <div class="ticket-info">

    	                  <div>
    	                    <?php 
    	                      $post_type = '';
    	                      if(isset($value['post_type'])) $post_type = $post_names[$value['post_type']];
    	                      echo $post_type;
    	                    ?>                      
    	                    </div>
    	                  <div class="bullet"></div>
    	                  <div class="text-primary no_action"><?php echo date("d M y H:i",strtotime($value["schedule_time"])); ?></div>
    	                </div>
    	              </a>
    	            <?php $sl++; endforeach ?>
    	          </div>
    	          <a href="<?php echo base_url('campaigns/posts'); ?>" target="_BLANK" class="ticket-item ticket-more">
    	            <?php echo $this->lang->line('View All'); ?> <i class="fas fa-chevron-right"></i>
    	          </a>
    	        </div>
    	      </div>
    	    </div>
    	</div>
    </div>
    
  <?php if($other_dashboard == 1) : ?>
  </div>
  <?php endif; ?>
</section>
	
<script type="text/javascript">
	$(document).on('click', '.no_action', function(event) {
	  event.preventDefault();
	});
	var myChart1; 
	$(document).ready(function() {		
    var stepsize = "<?php echo $step_size; ?>";	
		var post_type_chart = document.getElementById('post_type_chart').getContext('2d');
		var myChart1 = new Chart(post_type_chart, {
		  type: 'line',
		  data: {
		    labels: <?php echo json_encode(array_values($post_type_date));?>,
		    datasets: [{
		      label: '<?php echo $this->lang->line('CTA post'); ?>',
		      data: <?php echo json_encode(array_values($cta_post_data));?>,
		      borderWidth: 2,
		      backgroundColor: 'rgba(254,86,83,.7)',
		      borderWidth: 0,
		      borderColor: 'transparent',
		      pointBorderWidth: 0,
		      pointRadius: 3.5,
		      pointBackgroundColor: 'transparent',
		      pointHoverBackgroundColor: 'rgba(254,86,83,.8)',
		    },
		    {
		      label: '<?php echo $this->lang->line('Offer post'); ?>',
		      data: <?php echo json_encode(array_values($offer_post_data));?>,
		      borderWidth: 2,
		      backgroundColor: 'rgba(63,82,227,.8)',
		      borderWidth: 0,
		      borderColor: 'transparent',
		      pointBorderWidth: 0 ,
		      pointRadius: 3.5,
		      pointBackgroundColor: 'transparent',
		      pointHoverBackgroundColor: 'rgba(63,82,227,.8)',
		    },
        {
          label: '<?php echo $this->lang->line('Event post'); ?>',
          data: <?php echo json_encode(array_values($event_post_data));?>,
          borderWidth: 2,
          backgroundColor: 'rgba(52,221,221,.8)',
          borderWidth: 0,
          borderColor: 'transparent',
          pointBorderWidth: 0 ,
          pointRadius: 3.5,
          pointBackgroundColor: 'transparent',
          pointHoverBackgroundColor: 'rgba(52,221,221,.8)',
        }]
		  },
		  options: {
		    legend: {
		      display: false
		    },
		    scales: {
		      yAxes: [{
		        gridLines: {
		          // display: false,
		          drawBorder: false,
		          color: '#f2f2f2',
		        },
		        ticks: {
		          beginAtZero: true,
		          stepSize: stepsize,
              // display: false,
		          // callback: function(value, index, values) {
		          //   return value;
		          // }
		        }
		      }],
		      xAxes: [{
		        gridLines: {
		          display: false,
		          tickMarkLength: 15,
		        }
		      }]
		    },
		  }
		});
	});
</script>

<script type="text/javascript">
	
  var seven_days_reply_chart = document.getElementById("seven_days_reply_chart").getContext('2d');

  var sevendays_review_chart_bgcolor = seven_days_reply_chart.createLinearGradient(0, 0, 0, 70);
  sevendays_review_chart_bgcolor.addColorStop(0, 'rgba(63,82,227,.2)');
  sevendays_review_chart_bgcolor.addColorStop(1, 'rgba(63,82,227,0)');

  var myChart = new Chart(seven_days_reply_chart, {
    type: 'line',
    data: {
      labels: <?php echo json_encode($seven_days_reviews_chart_label);?>,
      datasets: [{
        label: '<?php echo $this->lang->line("Reviews");?>',
        data: <?php echo json_encode($seven_days_reviews_chart_data) ;?>,
        backgroundColor: sevendays_review_chart_bgcolor,
        borderWidth: 3,
        borderColor: 'rgba(63,82,227,1)',
        pointBorderWidth: 0,
        pointBorderColor: 'transparent',
        pointRadius: 3,
        pointBackgroundColor: 'transparent',
        pointHoverBackgroundColor: 'rgba(63,82,227,1)',
      }]
    },
    options: {
      layout: {
        padding: {
          bottom: -1,
          left: -1
        }
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          gridLines: {
            display: false,
            drawBorder: false,
          },
          ticks: {
            beginAtZero: true,
            display: false
          }
        }],
        xAxes: [{
          gridLines: {
            drawBorder: false,
            display: false,
          },
          ticks: {
            display: false
          }
        }]
      },
    }
  });

  var seven_days_post_chart = document.getElementById("seven_days_post_chart").getContext('2d');

  var sevendays_post_chart_bgcolor = seven_days_post_chart.createLinearGradient(0, 0, 0, 70);
  sevendays_post_chart_bgcolor.addColorStop(0, 'rgba(63,82,227,.2)');
  sevendays_post_chart_bgcolor.addColorStop(1, 'rgba(63,82,227,0)');

  var myChart = new Chart(seven_days_post_chart, {
    type: 'line',
    data: {
      labels: <?php echo json_encode($seven_days_post_chart_label);?>,
      datasets: [{
        label: '<?php echo $this->lang->line("Post published");?>',
        data: <?php echo json_encode($seven_days_post_chart_data) ;?>,
        backgroundColor: sevendays_post_chart_bgcolor,
        borderWidth: 3,
        borderColor: 'rgba(63,82,227,1)',
        pointBorderWidth: 0,
        pointBorderColor: 'transparent',
        pointRadius: 3,
        pointBackgroundColor: 'transparent',
        pointHoverBackgroundColor: 'rgba(63,82,227,1)',
      }]
    },
    options: {
      layout: {
        padding: {
          bottom: -1,
          left: -1
        }
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          gridLines: {
            display: false,
            drawBorder: false,
          },
          ticks: {
            beginAtZero: true,
            display: false
          }
        }],
        xAxes: [{
          gridLines: {
            drawBorder: false,
            display: false,
          },
          ticks: {
            display: false
          }
        }]
      },
    }
  });



  $("#products-carousel").owlCarousel({
    items: 3,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    loop: true,
    responsive: {
      0: {
        items: 2
      },
      768: {
        items: 2
      },
      1200: {
        items: 3
      }
    }
  });

  $("#carousel_24h").owlCarousel({
    items: 3,
    margin: 10,
    autoplay: true,
    autoplayTimeout: 5000,
    loop: true,
    responsive: {
      0: {
        items: 2
      },
      768: {
        items: 2
      },
      1200: {
        items: 3
      }
    }
  });



</script>


<script>
	$(document).ready(function() {
		
		$(document).on('click', '.month_change', function(e) {
		  e.preventDefault(); 
		  $(".month_change").removeClass('active');
		  $(this).addClass('active');
		  var month_no = $(this).attr('month_no');
		  var month_name = $(this).html();
		  $("#orders-month").html(month_name);

		  $(".month_change_middle_content").hide();
		  $("#loader").removeClass('hidden');
      var system_dashboard = "<?php echo $has_system_dashboard; ?>";
      if(system_dashboard == 'yes')
        var url = "<?php echo base_url('dashboard/get_first_div_content/')?>"+system_dashboard;
      else
        var url = "<?php echo base_url('dashboard/get_first_div_content')?>";

		  $.ajax({
		     type:'POST' ,
		     url: url,
		     data: {month_no:month_no},
		     dataType : 'JSON',
		     success:function(response)
		     {
		      	$("#loader").addClass('hidden');
		      	$("#subscribed").html(response.subscribed);
		      	$("#unsubscribed").html(response.unsubscribed);
		      	$("#total_subscribers").html(response.total_subscribers);
		      	$("#message_sent").html(response.total_message_sent);
		      	$(".month_change_middle_content").show();
		     }
		  });
		});

		$(document).on('click', '.period_change', function(e) {
		  e.preventDefault(); 
		  $(".period_change").removeClass('active');
		  $(this).addClass('active');
		  var period = $(this).attr('period');
		  var selected_period = $(this).html();
		  $("#selected_period").html(selected_period);

		  $("#period_change_content").hide();
		  $("#period_loader").removeClass('hidden');
      var system_dashboard = "<?php echo $has_system_dashboard; ?>";
      if(system_dashboard == 'yes')
        var url = "<?php echo base_url('dashboard/get_subscriber_data_div/')?>"+system_dashboard;
      else
        var url = "<?php echo base_url('dashboard/get_subscriber_data_div')?>";

		  $.ajax({
		     type:'POST' ,
		     url: url,
		     data: {period:period},
		     dataType : 'JSON',
		     success:function(response)
		     {
		      	$("#period_loader").addClass('hidden');

		      	$("#total_email_gain").html(response.email.total_email_gain);
		      	$("#email_male_percentage").css('width',response.email.male_percentage);
		      	$("#email_male_number").html(response.email.male);
		      	$("#email_female_percentage").css('width',response.email.female_percentage);
		      	$("#email_female_number").html(response.email.female);

		      	$("#total_phone_gain").html(response.phone.total_phone_gain);
		      	$("#phone_male_percentage").css('width',response.phone.male_percentage);
		      	$("#phone_male_number").html(response.phone.male);
		      	$("#phone_female_percentage").css('width',response.phone.female_percentage);
		      	$("#phone_female_number").html(response.phone.female);

		      	$("#total_birthdate_gain").html(response.birthdate.total_birthdate_gain);
		      	$("#birthdate_male_percentage").css('width',response.birthdate.male_percentage);
		      	$("#birthdate_male_number").html(response.birthdate.male);
		      	$("#birthdate_female_percentage").css('width',response.birthdate.female_percentage);
		      	$("#birthdate_female_number").html(response.birthdate.female);

		      	$("#period_change_content").show();
		     }
		  });

		});


	});
</script>

