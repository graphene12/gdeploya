<section class="section section_custom">
  <div class="section-header">
    <h1><i class="fas fa-plus-circle"></i> <?php echo $page_title; ?></h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item"><?php echo $this->lang->line("Dashboard"); ?></div>
      <div class="breadcrumb-item"><?php echo $page_title; ?></div>
    </div>
  </div>

  <?php $this->load->view('admin/theme/message'); ?>

  <div class="row">
    <div class="col-md-4" style="margin-top:10px;">

      <form class="form-horizontal" action="<?php echo site_url().'dashboard/createUser';?>" method="POST" autocomplete="off">
        <div class="card">
          <div class="card-body">
            <div class="form-group">
              <label for="name"> <?php echo $this->lang->line("Full Name")?> </label>
              <input name="name" value="<?php echo set_value('name');?>"  class="form-control" type="text" required>
              <span class="red"><?php echo form_error('name'); ?></span>
            </div>
             
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="email"> <?php echo $this->lang->line("Email")?> *</label>
                  <input name="email" value="<?php echo set_value('email');?>"  class="form-control" type="email" required>
                  <span class="red"><?php echo form_error('email'); ?></span>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="mobile"><?php echo $this->lang->line("Mobile")?></label>              
                  <input name="mobile" value="<?php echo set_value('mobile');?>"  class="form-control" type="text" required>
                  <span class="red"><?php echo form_error('mobile'); ?></span>               
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="password"> <?php echo $this->lang->line("Password")?> *</label>
                  <input name="password" value="<?php echo set_value('password');?>"  class="form-control" type="password" required>
                  <span class="red"><?php echo form_error('password'); ?></span>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="confirm_password"> <?php echo $this->lang->line("Confirm Password")?> *</label>
                  <input name="confirm_password" value="<?php echo set_value('confirm_password');?>"  class="form-control" type="password" required>
                  <span class="red"><?php echo form_error('confirm_password'); ?></span>
                </div>
              </div>
            </div>

            <div class="row" id="hidden">
              <div class="col-12">
                <?php $expired_date_default = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s"). ' + 300 days'));
                ?>
                <div class="form-group">
                  <label for="expired_date"> <?php echo $this->lang->line("Expiry Date")?> *</label>
                  <input name="expired_date" value="<?php echo (set_value('expired_date')!="") ? set_value('expired_date') : $expired_date_default;?>"  required class="form-control datepicker" type="text">
                  <span class="red"><?php echo form_error('expired_date'); ?></span>
                </div>
              </div>
            </div>


          </div>

          <div class="card-footer bg-whitesmoke">
            <button name="submit" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-save"></i> <?php echo $this->lang->line("Save");?></button>
            <button  type="button" class="btn btn-secondary btn-lg float-right" onclick='goBack("admin/user_manager",0)'><i class="fa fa-remove"></i> <?php echo $this->lang->line("Cancel");?></button>
          </div>
        </div>
      </form>  
    </div>

    <div class="col-md-8" style="margin-top:10px;">
    <div class="table-responsive card">
        <table class="table">
            <thead >
                <tr style="background-color: black;color:white;">
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Expiry Date</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($reseller_users as $user): ?>
                    <tr>
                        <td><?php echo $user['name'] ?></td>
                        <td><?php echo $user['email'] ?></td>
                        <td><?php echo $user['mobile'] ?></td>
                        <td><?php echo substr_replace($user['expired_date'],'',11) ?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>

    </div>
  </div>
</section>

          


<script type="text/javascript">
  $(document).ready(function() {
    // if($("#price_default").val()=="0") $("#hidden").hide();
    // else $("#validity").show();
    $(".user_type").click(function(){
      if($(this).val()=="Admin") $("#hidden").hide();
      else $("#hidden").show();
    });
  });
</script>