<section class="section">
    <div class="section-header">
        <h1><i class="fas fa-hands-helping"></i> <?php echo $page_title; ?></h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><?php echo $this->lang->line("System"); ?></div>
            <div class="breadcrumb-item"><?php echo $page_title; ?></div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon text-primary">
                        <i class="fas fa-list"></i>
                    </div>
                    <div class="card-body">
                        <h4><?php echo $this->lang->line("Create CTA, Event, Offer Post"); ?></h4>
                        <p><?php echo $this->lang->line("Create CTA, Event or Offer post etc..."); ?></p>
                        <a href="<?php echo base_url("campaigns/posts"); ?>" class="card-cta"><?php echo $this->lang->line("Campaign list"); ?> <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon text-primary">
                        <i class="fas fa-rss"></i>
                    </div>
                    <div class="card-body">
                        <h4><?php echo $this->lang->line("RSS Auto Post"); ?></h4>
                        <p><?php echo $this->lang->line("Create RSS auto posts from here..."); ?></p>
                        <a href="<?php echo base_url("campaigns/rss_posts"); ?>" class="card-cta"><?php echo $this->lang->line("Campaign list"); ?> <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>