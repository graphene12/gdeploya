<style type="text/css">
    .dropdown-toggle:before {content:none !important;}
    .blue{color: #2C9BB3 !important;}
</style>
<section class="section">
    <div class="section-header">
        <h1><i class="fab fa-google"></i> <?php echo $page_title; ?></h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><?php echo $this->lang->line("System"); ?></div>
            <div class="breadcrumb-item"><a href="<?php echo base_url('social_apps/settings'); ?>"><?php echo $this->lang->line("Social Apps"); ?></a></div>
            <div class="breadcrumb-item"><?php echo $page_title; ?></div>
        </div>
    </div>
    <?php $this->load->view('admin/theme/message'); ?>

    <div class="row">
        <div class="col-12 col-lg-6">
            <div class="card" style="min-height: 250px;">
              <div class="card-body">
                  <b><?php echo $this->lang->line("Homepage URL")."</b>:<br>
                  <span class='blue'>".base_url(); ?></span><br/>

                  <b><?php echo $this->lang->line("Privacy Policy URL")."</b>:<br>
                  <span class='blue'>".base_url("home/privacy_policy"); ?></span><br/>

                  <b><?php echo $this->lang->line("Terms of Service URL")."</b>:<br>
                  <span class='blue'>".base_url("home/terms_use"); ?></span><br/><br>

                  <b><?php echo $this->lang->line("Google Auth Redirect URL")."</b>:<br>
                  <span class='blue'>".base_url("home/google_login_back"); ?></span><br/>
                  <span class="blue"><?php echo site_url("social_accounts/google_login_back"); ?></span><br/>
              </div>
            </div>
        </div>

        <div class="col-12 col-lg-6">
            <div class="card" style="min-height: 250px;">
              <div class="card-body">
                  <b><?php echo $this->lang->line("Enable APIs")."</b>: <br>";?><br>
                  <ul>
                    <li>Google My Business API</li>
                  </ul>
                  <p>Enable this API after getting approval & whitelisted for this Specific Google API . Submit for approval from <b><a href="https://docs.google.com/forms/d/e/1FAIpQLSfC_FKSWzbSae_5rOpgwFeIUzXUF1JCQnlsZM_gC1I2UHjA3w/viewform" target="_BLANK">here</a></b></p>
              </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-8 col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4> <i class="fas fa-list-ul"></i> <?php echo $this->lang->line('Apps List'); ?></h4>
                </div>
                <div class="card-body data-card">
                    <div class="table-responsive2">
                        <table class="table table-bordered" id="mytable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><?php echo $this->lang->line("Table ID"); ?></th>
                                <th><?php echo $this->lang->line("App Name"); ?></th>
                                <th><?php echo $this->lang->line("Status"); ?></th>
                                <th><?php echo $this->lang->line("Action"); ?></th>
                                <th><?php echo $this->lang->line("App Key"); ?></th>
                                <th><?php echo $this->lang->line("Client ID"); ?></th>
                                <th><?php echo $this->lang->line("Client Secret"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4"  id="app_info_card">
            <div class="card">
                <div class="card-header">
                    <div class="col-12 padding-0">
                        <h4>
                            <li class="fas fa-info-circle">
                                </i> <?php echo $this->lang->line("App Details"); ?>
                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" id="google_config_form_data" action="">
                        <input type="hidden" name="submit_type" value="add" id="submit_type">
                        <input type="hidden" name="table_id" id="table_id_info">
                        <div class="form-group">
                            <label for=""><i class="fas fa-file-signature"></i> <?php echo $this->lang->line("App Name");?> </label>
                            <input name="app_name" id="app_name" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for=""><i class="ion ion-key"></i> <?php echo $this->lang->line("APP Key");?> </label>
                            <input name="app_key" id="app_key" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for=""><i class="far fa-id-card"></i>  <?php echo $this->lang->line("Client ID");?></label>
                            <input name="google_client_id" id="google_client_id" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label for=""><i class="fas fa-key"></i>  <?php echo $this->lang->line("Client Secret");?></label>
                            <input name="google_client_secret" id="google_client_secret" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                            <label class="custom-switch mt-2">
                                <input type="checkbox" name="status" id="app_status" value="1" class="custom-switch-input" checked>
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description"><?php echo $this->lang->line('Active');?></span>
                            </label>
                        </div>
                    </form>
                    <button class="btn btn-primary btn-md" id="add_app_btn"><i class="fa fa-plus"></i> <?php echo $this->lang->line("Add App");?></button>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {

        $(document).on('mouseover', '[data-toggle=\"tooltip\"]', function(event) {
            event.preventDefault();
            $('[data-toggle=\"tooltip\"]').tooltip();
        });

        var perscroll,
            form = $('#google_config_form_data'),
            table = $("#mytable").DataTable({
            serverSide: true,
            processing:true,
            bFilter: true,
            order: [[ 1, "desc" ]],
            pageLength: 10,
            ajax:
                {
                    "url": '<?php echo base_url('social_apps/google_settings_data'); ?>',
                    "type": 'POST'
                },
            language:
                {
                    url: "<?php echo base_url('assets/modules/datatables/language/'.$this->language.'.json'); ?>"
                },
            dom: '<"top"f>rt<"bottom"lip><"clear">',
            columnDefs: [
                {
                    targets: [1],
                    visible: false
                },
                {
                    targets: '',
                    className: 'text-center'
                },
                {
                    targets: [0,4],
                    sortable: false
                },
                {
                    targets: [3],
                    render: function ( data, type, row, meta ) {
                        if (data == '0')
                            return "<span class='text-danger' style='min-width: 120px;'><i class='fas fa-times-circle'></i> <?php echo $this->lang->line("Inactive"); ?></span>";
                        else if (data == '1')
                            return "<span class='text-success' style='min-width: 120px;'><i class='fas fa-check-circle'></i> <?php echo $this->lang->line("Active"); ?></span>";
                    }
                },
                {
                    targets: [4],
                    render: function ( data, type, row, meta ) {
                        var action_width = (3 * 47) + 20;
                        var campaign_status_title;
                        var campaign_status_icon;

                        if (row[3] == '0') {
                            campaign_status_title = '<?php echo $this->lang->line("Make this app active"); ?>';
                            campaign_status_icon = 'fa-play';
                        } else if (row[3] == '1') {
                            campaign_status_title = '<?php echo $this->lang->line("Make this app inactive"); ?>';
                            campaign_status_icon = 'fa-pause'
                        }

                        var string = '<div class="dropdown d-inline dropleft"><button class="btn btn-outline-primary dropdown-toggle no_caret" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-briefcase"></i></button><div class="dropdown-menu mini_dropdown text-center" style="width:'+ action_width +'px !important"><a class="btn btn-circle btn-outline-info state_change_action" href="#" data-toggle="tooltip" title="'+ campaign_status_title +'" table_id="' + row[1] +'"><i class="fas '+ campaign_status_icon +'"></i></a>&nbsp;';
                        string += '<a class="btn btn-circle btn-outline-warning edit_table_data" href="#" data-toggle="tooltip" title="<?php echo $this->lang->line("Edit"); ?>" table_id="' + row[1] +'"><i class="fas fa-edit"></i></a>&nbsp;';
                        string += '<a class="btn btn-circle btn-outline-danger delete_table_data" href="#" data-toggle="tooltip" title="<?php echo $this->lang->line("Delete"); ?>" table_id="' + row[1] +'"><i class="fas fa-trash"></i></a></div></div>';

                        return string;
                    }
                },
                {
                    targets: [5],
                    render: function ( data, type, row, meta )
                    {

                        return row[4];
                    }
                },
                {
                    targets: [6],
                    render: function ( data, type, row, meta )
                    {

                        return row[5];
                    }
                },
                {
                    targets: [7],
                    render: function ( data, type, row, meta )
                    {

                        return row[6];
                    }
                },
            ],
            fnInitComplete:function(){ // when initialization is completed then apply scroll plugin
                if(areWeUsingScroll)
                {
                    if (perscroll) perscroll.destroy();
                    perscroll = new PerfectScrollbar('#mytable_wrapper .dataTables_scrollBody');
                }
            },
            scrollX: 'auto',
            fnDrawCallback: function( oSettings ) { //on paginition page 2,3.. often scroll shown, so reset it and assign it again
                if(areWeUsingScroll)
                {
                    if (perscroll) perscroll.destroy();
                    perscroll = new PerfectScrollbar('#mytable_wrapper .dataTables_scrollBody');
                }
            }
        });

        $(document).on('keyup', '#app_name, #app_key, #google_client_id, #google_client_secret', function(event) {
            event.preventDefault();

            var app_name = $("#app_name").val();
            var app_key = $("#app_key").val();
            var google_client_id = $("#google_client_id").val();
            var google_client_secret = $("#google_client_secret").val();

            if (app_name == '' &&
                app_key == '' &&
                google_client_id == '' &&
                google_client_secret == ''
            ) {
                $("#add_app_btn").html('<i class="fa fa-plus"></i> <?php echo $this->lang->line("Add App"); ?>');
                $("#submit_type").val('add');
                $("#app_status").prop('checked', false);
            }
        });

        $(document).on('click', '#add_app_btn', function(event) {
            event.preventDefault();
            var form_data = new FormData($(form)[0]);
            var submit_type = $("#submit_type").val();
            var btn_content = $(this).html();
            btn_content = btn_content.replace('<i class="fa fa-plus"></i>', '<i class="fas fa-spin fa-spinner"></i>');
            btn_content = btn_content.replace('<i class="fa fa-edit"></i>', '<i class="fas fa-spin fa-spinner"></i>');

            $(this).html(btn_content);

            var that = $(this);

            $.ajax({
                url: '<?php echo base_url('social_apps/google_settings_action'); ?>',
                type: 'POST',
                dataType: 'json',
                cache: false,
                processData: false,
                contentType: false,
                data: form_data,
                success: function (response) {

                    if (response.type == 'error') {
                        swal('<?php echo $this->lang->line("Error"); ?>', response.message, 'error');
                    } else if (response.type == 'success') {

                        $(form)[0].reset();

                        swal('<?php echo $this->lang->line("Success"); ?>', response.message, 'success');
                    }

                    table.draw();

                    if (submit_type == 'add')
                        btn_content = btn_content.replace('<i class="fas fa-spin fa-spinner"></i>', '<i class="fa fa-plus"></i>');
                    else
                        btn_content = btn_content.replace('<i class="fas fa-spin fa-spinner"></i>', '<i class="fa fa-edit"></i>');
                    $(that).html(btn_content);
                }
            });

        });

        $(document).on('click', '.edit_table_data', function(event) {
            event.preventDefault();

            var parent = $(this).parent().parent().parent().parent();
            var childrens = $(parent).children();

            var app_name = $(childrens)[1]['innerHTML'];
            var app_key = $(childrens)[4]['innerHTML'];
            var google_client_id = $(childrens)[5]['innerHTML'];
            var google_client_secret = $(childrens)[6]['innerHTML'];
            var app_status = $(childrens)[2]['innerHTML'];

            $("#app_name").val(app_name);
            $("#app_key").val(app_key);
            $("#google_client_id").val(google_client_id);
            $("#google_client_secret").val(google_client_secret);

            if (app_status.indexOf('<?php echo $this->lang->line("Active"); ?>') != -1)
                $("#app_status").prop('checked', true);
            else
                $("#app_status").prop('checked', false);

            $("#table_id_info").val($(this).attr('table_id'));
            $("#submit_type").val('edit');

            $("#add_app_btn").html('<i class="fa fa-edit"></i> <?php echo $this->lang->line("Edit App"); ?>');

            location.href = '<?php echo base_url('social_apps/google_settings#app_info_card'); ?>';
        });

        $(document).on('click', '.state_change_action', function(event) {
            event.preventDefault();

            var table_id = $(this).attr('table_id');
            var that = $(this).parent().parent();

            swal({
                title: '<?php echo $this->lang->line("Are you sure?"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to change this apps state?"); ?>',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $(that).find('button i').addClass('fa-spin fa-spinner');
                    $(that).find('button i').removeClass('fa-trash');
                    var base_url = '<?php echo site_url();?>';
                    var user_page_id = $("#migrate_list").attr('button_id');

                    $.ajax({
                        type:'POST' ,
                        url:"<?php echo site_url();?>social_apps/change_google_app_state",
                        dataType: 'json',
                        data: { table_id },
                        success:function(response) {
                            if(response.type == '1')
                                swal('<?php echo $this->lang->line("Success"); ?>', response.message, 'success');
                            else
                                swal('<?php echo $this->lang->line("Error"); ?>', response.message, 'error');

                            $(that).find('button i').removeClass('fa-spin fa-spinner');
                            $(that).find('button i').addClass('fa-trash');
                            table.draw();
                        }
                    });
                }
            });
        });

        $(document).on('click', '.delete_table_data', function(event) {
            event.preventDefault();

            var table_id = $(this).attr('table_id');
            var that = $(this).parent().parent();

            swal({
                title: '<?php echo $this->lang->line("Are you sure?"); ?>',
                text: '<?php echo $this->lang->line("Do you really want to delete this app config?"); ?>',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $(that).find('button i').addClass('fa-spin fa-spinner');
                    $(that).find('button i').removeClass('fa-trash');
                    var base_url = '<?php echo site_url();?>';

                    $.ajax({
                        type:'POST' ,
                        url:"<?php echo site_url();?>social_apps/delete_google_app",
                        dataType: 'json',
                        data: { table_id },
                        success:function(response) {
                            if(response.type == '1')
                                swal('<?php echo $this->lang->line("Success"); ?>', response.message, 'success');
                            else
                                swal('<?php echo $this->lang->line("Error"); ?>', response.message, 'error');

                            $(that).find('button i').removeClass('fa-spin fa-spinner');
                            $(that).find('button i').addClass('fa-trash');
                            table.draw();
                        }
                    });
                }
            });
        });
    });
</script>