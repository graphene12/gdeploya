<?php

require_once("Home.php"); // loading home controller

require_once APPPATH . '/libraries/Google_My_Business/Exception.php';
require_once APPPATH . '/libraries/Google_My_Business/Service/Exception.php';

class Location_manager extends Home
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in') != 1)
        redirect('home/login_page', 'location');   
        
        $this->important_feature();
        $this->member_validity();
              
    }

    public function index()
    {
      $this->location_list();
    }

    public function location_list()
    {
      $gmb_account_table_id = $this->session->userdata('google_mybusiness_user_table_id');
      $selected_location_table_id = $this->session->userdata('location_list_location_table_id');
      $location_infos = $this->basic->get_data('google_business_locations',['where'=>['user_account_id'=>$gmb_account_table_id]]);
      $location_info = array();
      $i=1;
      foreach($location_infos as $value)
      {
          if($value['id'] == $selected_location_table_id)
              $location_info[0] = $value;
          else
              $location_info[$i] = $value;

          $i++;
      }

      ksort($location_info);
      $data['location_info'] = $location_info;
      $data['body'] = "manage_location/location_list";
      $data['page_title'] = $this->lang->line('Location manager');
      $this->_viewcontroller($data);
    }

    public function get_location_details()
    {
        $this->ajax_check();
        $response = [];
        $location_table_id =  $this->input->post('location_table_id');
        // $response['review_reply_settings_url'] = base_url("location_manager/review_replies/$location_table_id");
        $this->session->set_userdata('location_list_location_table_id',$location_table_id);
        $user_id = $this->user_id;
        $select = ['google_business_locations.*'];
        $join = ['google_user_account'=>'google_user_account.id=google_business_locations.user_account_id,left'];
        $where = ['where'=>['google_user_account.user_id'=>$user_id,'google_business_locations.id'=>$location_table_id]];
        $location_info = $this->basic->get_data('google_business_locations',$where,$select,$join);

        if(empty($location_info))
        {
            $middle_column_content = '
                        <div class="card" id="nodata">
                          <div class="card-body">
                            <div class="empty-state">
                              <img class="img-fluid" style="height: 200px" src="'.base_url('assets/img/drawkit/drawkit-nature-man-colour.svg').'" alt="image">
                              <h2 class="mt-0">'.$this->lang->line("We could not find any data.").'</h2>
                            </div>
                          </div>
                        </div>';
        }
        else
        {

            $middle_column_content='
            <div class="card main_card">
              <div class="card-header padding-left-10">
                <h4 class="put_location_name_url"><i class="fas fa-map-pin"></i> <a target="_BLANK" href="'.$location_info[0]['map_url'].'">'.$location_info[0]['location_display_name'].'</a></h4>
              </div>
              <div class="card-body padding-10">

                <div class="row">
                
                  <div class="col-12">
                    <div class="card card-large-icons card-condensed middle_col_item active">
                      <div class="card-icon">
                        <i class="fas fa-cogs"></i>
                      </div>
                      <div class="card-body">
                        <h4>'.$this->lang->line("Review reply settings").'</h4>                    
                        <a href="'.base_url("location_manager/review_replies").'" id="review_reply_settings" data-page-id="'.$location_info[0]['id'].'" data-height="500" class="card-cta iframed">'.$this->lang->line("Change Settings").'</a>
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="card card-large-icons card-condensed middle_col_item">
                      <div class="card-icon">
                        <i class="fas fa-star"></i>
                      </div>
                      <div class="card-body">
                        <h4>'.$this->lang->line("Review list").'</h4>                    
                        <a href="'.base_url("location_manager/review_list").'" id="review_list" data-page-id="'.$location_info[0]['id'].'" data-height="500" class="card-cta iframed">'.$this->lang->line("Change Settings").'</a>
                      </div>
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="card card-large-icons card-condensed middle_col_item">
                      <div class="card-icon">
                        <i class="fas fa-share-square"></i>
                      </div>
                      <div class="card-body">
                        <h4>'.$this->lang->line("Post list").'</h4>                    
                        <a href="'.base_url("location_manager/post_list").'" id="get_post_list" data-page-id="'.$location_info[0]['id'].'" data-height="500" class="card-cta iframed">'.$this->lang->line("Change Settings").'</a>
                      </div>
                    </div>
                  </div>';
            if($this->session->userdata('user_type') == 'Admin' || in_array(5,$this->module_access))
            {
              $middle_column_content .='
                  <div class="col-12">
                    <div class="card card-large-icons card-condensed middle_col_item">
                      <div class="card-icon">
                        <i class="fas fa-question-circle"></i>
                      </div>
                      <div class="card-body">
                        <h4>'.$this->lang->line("Questions & Ans.").'</h4>          
                        <a href="'.base_url("location_manager/question_list").'" id="question_answer" data-page-id="'.$location_info[0]['id'].'" data-height="500" class="card-cta iframed">'.$this->lang->line("Change Settings").'</a>
                      </div>
                    </div>
                  </div>';
            }

            
            $middle_column_content .='
                </div>
              </div>
            </div>
            
            <script>
            $(\'[data-toggle="popover"]\').popover(); 
            $(\'[data-toggle="popover"]\').on("click", function(e) {e.preventDefault(); return true;});
            </script>
            '; 
        }

        $response['middle_column_content'] = $middle_column_content;
        $response['location_insight_url'] = base_url('location_manager/location_insights_basic/').$location_table_id;
        echo json_encode($response);


    }

    public function get_new_review_url()
    {
        $selected_location_table_id = $this->session->userdata('location_list_location_table_id');
        $location_info = $this->basic->get_data('google_business_locations',['where'=>['id'=>$selected_location_table_id]],'new_review_url');
        $review_url = isset($location_info[0]['new_review_url']) ? $location_info[0]['new_review_url'] : '';
        $content='<div class="row">
                    <div class="col-12">';
            $content .= '
                        <div class="card">
                          <div class="card-body">
                            <p>'.$this->lang->line("Copy the below URL for further use.").'</p>
                            <pre class="language-javascript">
                                <code class="dlanguage-javascript copy_code">
'.$review_url.'
                                </code>
                            </pre>
                          </div>
                        </div>';
            $content .='</div>
                </div>
                <script>
                    $(document).ready(function() {
                        Prism.highlightAll();
                        $(".toolbar-item").find("a").addClass("copy");

                        $(document).on("click", ".copy", function(event) {
                            event.preventDefault();

                            $(this).html("'.$this->lang->line('Copied!').'");
                            var that = $(this);
                            
                            var text = $(this).prev("code").text();
                            var temp = $("<input>");
                            $("body").append(temp);
                            temp.val(text).select();
                            document.execCommand("copy");
                            temp.remove();

                            setTimeout(function(){
                              $(that).html("'.$this->lang->line('Copy').'");
                            }, 2000); 

                        });
                    });
                </script>
                ';
        echo $content;
    }

    public function review_replies()
    {
      $selected_location_table_id = $this->session->userdata('location_list_location_table_id');
      $data['location_table_id'] = $selected_location_table_id;
      $data['page_title'] = $this->lang->line('Review reply list');
      $data['title'] = $this->lang->line('Review reply list');
      $data['body'] = 'manage_location/review_replies';
      $data['iframe'] = 1;
      $this->_viewcontroller($data);
    }

    public function review_reply_data()
    {
        $this->ajax_check();

        $location_id = trim($this->input->post("location_id", true));
        $display_columns = array('#', 'id', 'star', 'actions');

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $start = isset($_POST['start']) ? intval($_POST['start']) : 0;
        $limit = isset($_POST['length']) ? intval($_POST['length']) : 10;
        $sort_index = isset($_POST['order'][0]['column']) ? strval($_POST['order'][0]['column']) : 2;
        $sort = isset($display_columns[$sort_index]) ? $display_columns[$sort_index] : 'id';
        $order = isset($_POST['order'][0]['dir']) ? strval($_POST['order'][0]['dir']) : 'desc';
        $order_by = $sort . " " . $order;

        // if(! $location_id) {
        //     $location_id = $this->session->userdata('gmb_review_reply_location_id');
        // }
        // // Sets location ID for add review reply settings
        // $this->session->set_userdata('gmb_add_review_reply_settings_location_id', $location_id);

        $where = [
            'where' => [
                'user_id' => $this->user_id,
                'location_id' => $location_id,
            ]
        ];

        $select = ['google_review_reply_settings.*'];

        $table = 'google_review_reply_settings';
        $info = $this->basic->get_data($table,$where,$select,$join='',$limit,$start,$order_by,$group_by='');

        // Gets total rows
        $total_rows_array = $this->basic->count_row($table,$where,$count=$table.".id",$join='',$group_by='');
        $total_result = $total_rows_array[0]['total_rows'];

        $info_data = isset($info[0]) ? $info[0] : [];
        $td_data = [];
        $id = 0;
        foreach ($info_data as $key => $value) {
            $stars = ['five_star', 'four_star', 'three_star', 'two_star', 'one_star'];
            if (! in_array($key, $stars)) {
                continue;
            }

            $edit_review_reply = '<a class="btn btn-circle btn-outline-warning" href="' . base_url('location_manager/edit_settings/') . $key . '" data-toggle="tooltip" title="' . $this->lang->line("Edit") . '"><i class="fas fa-edit"></i></a>';
            $delete_review_reply = '<a class="btn btn-circle btn-outline-danger delete" data-toggle="tooltip" title="' . $this->lang->line("Delete") . '" id="' . $key . '" href="#"><i class="fas fa-trash-alt"></i></a>';
            $report_review_reply = '<a class="btn btn-circle btn-outline-info report" data-toggle="tooltip" title="' . $this->lang->line("Report") . '" id="' . $key . '" href="'.base_url('location_manager/review_report/').$key.'/'.$location_id.'" target="_BLANK"><i class="fas fa-eye"></i></a>';

            // Action section started from here
            $account_count = 2;
            $action_width = ($account_count*47)+20;
            $buttons = '<div class="dropdown d-inline dropright">
            <button class="btn btn-outline-primary dropdown-toggle no_caret" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-briefcase"></i></button>
            <div class="dropdown-menu mini_dropdown text-center" style="width:'.$action_width.'px !important">';

            $buttons .= $edit_review_reply;
            $buttons .= $report_review_reply;
            $buttons .= $delete_review_reply;
            $buttons .= "</div></div><script>$('[data-toggle=\"tooltip\"]').tooltip();</script>";

            switch ($key) {
                case 'five_star':
                    $val = (isset($info[0][$key]) && ! empty($info[0][$key])) ? $info[0][$key] : null;
                    if (null != $val) {
                        $id++;
                        $five_star = [
                            'id' => $id,
                            'star' => '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>',
                            'actions' => $buttons
                        ];
                        array_push($td_data, $five_star);
                    }
                    break;
                case 'four_star':
                    $val = (isset($info[0][$key]) && ! empty($info[0][$key])) ? $info[0][$key] : null;
                    if (null != $val) {
                        $id++;
                        $four_star = [
                            'id' => $id,
                            'star' => '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>',
                            'actions' => $buttons
                        ];
                        array_push($td_data, $four_star);
                    }
                    break;
                case 'three_star':
                    $val = (isset($info[0][$key]) && ! empty($info[0][$key])) ? $info[0][$key] : null;
                    if (null != $val) {
                        $id++;
                        $three_star = [
                            'id' => $id,
                            'star' => '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>',
                            'actions' => $buttons
                        ];
                        array_push($td_data, $three_star);
                    }
                    break;
                case 'two_star':
                    $val = (isset($info[0][$key]) && ! empty($info[0][$key])) ? $info[0][$key] : null;
                    if (null != $val) {
                        $id++;
                        $two_star = [
                            'id' => $id,
                            'star' => '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>',
                            'actions' => $buttons
                        ];
                        array_push($td_data, $two_star);
                    }
                    break;
                case 'one_star':
                    $val = (isset($info[0][$key]) && ! empty($info[0][$key])) ? $info[0][$key] : null;
                    if (null != $val) {
                        $id++;
                        $one_star = [
                            'id' => $id,
                            'star' => '<i class="fas fa-star text-warning"></i>',
                            'actions' => $buttons
                        ];
                        array_push($td_data, $one_star);
                    }
                    break;
            }
        }

        $data['draw'] = (int) $_POST['draw'] + 1;
        $data['recordsTotal'] = $total_result;
        $data['recordsFiltered'] = $total_result;
        $data['data'] = convertDataTableResult($td_data, $display_columns ,$start, $primary_key = "id");

        echo json_encode($data);
    }

    public function add_settings()
    {
        $data['page_title'] = $this->lang->line('Add review reply settings');
        $data['title'] = $this->lang->line('Add review reply settings');
        $data['body'] = 'manage_location/add_review_reply_settings';
        $data['iframe'] = 1;
        $this->_viewcontroller($data);
    }

    public function validate_keyword_settings() {
        $keyword_settings = isset($_POST['keyword_settings'][0]) ? strlen($_POST['keyword_settings'][0]) : 0;

        if (0 == $keyword_settings) {
            $this->form_validation->set_message('validate_keyword_settings', $this->lang->line("Keyword is required"));
            return false;
        }

        return true;
    }

    public function validate_reply_settings() {
        $reply_settings = isset($_POST['reply_settings'][0]) ? strlen($_POST['reply_settings'][0]) : 0;

        if (0 == $reply_settings) {
            $this->form_validation->set_message('validate_reply_settings', $this->lang->line("Reply is required"));
            return false;
        }

        return true;
    }

    public function save_review_reply()
    {
        $this->form_validation->set_rules('star_rating', $this->lang->line("Star rating"), 'required|in_list[five_star,four_star,three_star,two_star,one_star]');
        $this->form_validation->set_rules('reply_type', $this->lang->line("Reply type"), 'required|in_list[generic,keyword]');

        $reply_type = $this->input->post('reply_type', true);
        if ('generic' == $reply_type) {
            $this->form_validation->set_rules('generic_message', $this->lang->line("Generic message"), 'required');
        } elseif ('keyword') {
            $this->form_validation->set_rules('keyword_settings', $this->lang->line('Keyword'), 'callback_validate_keyword_settings');
            $this->form_validation->set_rules('reply_settings', $this->lang->line('Reply'), 'callback_validate_reply_settings');
            $this->form_validation->set_rules('not_found_reply_settings', $this->lang->line("Message for no match"), 'required');
        }

        if (false === $this->form_validation->run()) {
            $errors = $this->form_validation->error_array();
            $errors = str_replace(['<b>', '</b>'], '', $errors);
            echo json_encode([
                'status' => false,
                'errors' => $errors
            ]);
            exit;
        }

        // Gets location ID for updating star rating
        $selected_location_table_id = $this->session->userdata('location_list_location_table_id');
        
        $only_location_id_info = $this->basic->get_data('google_business_locations',['where'=>['id'=>$selected_location_table_id]],['only_location_id']);
        $only_location_id = isset($only_location_id_info[0]['only_location_id']) ? $only_location_id_info[0]['only_location_id'] : '';
        $existing_data = $this->basic->get_data('google_review_reply_settings',['where'=>['only_location_id'=>$only_location_id,'location_id !='=>$selected_location_table_id]]);
        if(!empty($existing_data))
        {
          $where = ['where'=>['google_business_locations.id'=>$existing_data[0]['location_id']]];
          $join = ['google_user_account'=>'google_business_locations.user_account_id=google_user_account.id,left'];
          $select = ['google_user_account.*'];
          $account_info = $this->basic->get_data('google_business_locations',$where,$select,$join);
          $name = isset($account_info[0]['account_display_name']) ? $account_info[0]['account_display_name'] : "";
          $email = isset($account_info[0]['email']) ? $account_info[0]['email'] : "";
          $profile_photo = isset($account_info[0]['profile_photo']) ? $account_info[0]['profile_photo'] : "";
          $response_html = '<div class="row text-center">
                              <div class="col-12">
                                <p><b>'.$this->lang->line('Reply settings is already enabled for this location by').'</b></p>
                              </div>
                              <div class="col-8 offset-2">
                                <ul class="list-unstyled list-unstyled-border">
                                    <li class="media">
                                        <img alt="image" class="mr-3 rounded-circle" width="50" src="'.$profile_photo.'">
                                        <div class="media-body">
                                          <div class="mt-0 mb-1 font-weight-bold">'.$name.'</div>
                                          <div class="text-success text-small font-600-bold"><i class="fas fa-envelope"></i> '.$email.'</div>
                                        </div>
                                    </li>
                                </ul>
                              </div>
                            </div>';
          echo json_encode([
              'status' => false,
              'message' => $response_html,
              'html' => 'yes'
          ]);
          exit;
        }

        // Gets star rating
        $star_rating = $this->input->post('star_rating');

        $data = [
            'status' => '0',
            'only_location_id' => $only_location_id,
            $star_rating => json_encode($_POST)
        ];
        $insert_data = [
          'user_id' => $this->user_id,
          'location_id' => $selected_location_table_id,
          'status' => '0',
          'only_location_id' => $only_location_id,
          $star_rating => json_encode($_POST)
        ];

        $where = [
            'user_id' => $this->user_id,
            'location_id' => $selected_location_table_id,
        ];

        $existing_where = [];
        $existing_where['where'] = [
            'user_id' => $this->user_id,
            'location_id' => $selected_location_table_id
        ];
        $existing_info = $this->basic->get_data('google_review_reply_settings',$existing_where);

        if(empty($existing_info))
        {
          if($this->basic->insert_data('google_review_reply_settings',$insert_data))
          {
            $message = $this->lang->line('Review reply settings inserted successfully.');
            echo json_encode([
                'status' => true,
                'message' => $message
            ]);
            exit;
          }
          else
          {
            $message = $this->lang->line('Something went wrong while updating data');
            echo json_encode([
                'status' => false,
                'message' => $message
            ]);
            exit;
          }
        }
        else
        {
          $this->basic->update_data('google_review_reply_settings', $where, $data);
          $message = $this->lang->line('Review reply settings successfully updated.');
          echo json_encode([
              'status' => true,
              'message' => $message
          ]);
          exit;
        }
    }

    public function edit_settings($star)
    {
        $stars = ['five_star', 'four_star', 'three_star', 'two_star', 'one_star'];
        if (! in_array($star, $stars)) {
            echo $this->error_404();
            exit;
        }

        $selected_location_table_id = $this->session->userdata('location_list_location_table_id');


        $where = [
            'where' => [
                'user_id' => $this->user_id,
                'location_id' => $selected_location_table_id
            ]
        ];

        $ratings = $this->basic->get_data('google_review_reply_settings', $where);

        $rating = isset($ratings[0][$star]) ? $ratings[0][$star] : [];

        $data['rating_details'] = json_decode($rating, true);
        $data['page_title'] = $this->lang->line('Edit review reply settings');
        $data['title'] = $this->lang->line('Edit review reply settings');
        $data['body'] = 'manage_location/edit_review_reply_settings';
        $data['iframe'] = 1;
        $this->_viewcontroller($data);
    }

    public function delete_star()
    {
        $this->ajax_check();

        $star = $this->input->post('star_rating');
        $stars = ['five_star', 'four_star', 'three_star', 'two_star', 'one_star'];
        if (! in_array($star, $stars)) {
            echo json_encode([
                'status' => false,
                'message' => $this->lang->line('Bad request'),
            ]);
            exit;
        }

        $selected_location_table_id = $this->session->userdata('location_list_location_table_id');
        $where = [
            'user_id' => $this->user_id,
            'location_id' => $selected_location_table_id,
        ];

        $data = [
            $star => null,
        ];

        if ($this->basic->update_data('google_review_reply_settings', $where, $data)) {
            
            $existing_stars = $this->basic->get_data('google_review_reply_settings',['where'=>['user_id'=>$this->user_id,'location_id'=>$selected_location_table_id]]);
            if($existing_stars[0]['five_star'] === null && $existing_stars[0]['four_star'] === null && $existing_stars[0]['three_star'] === null && $existing_stars[0]['two_star'] === null && $existing_stars[0]['one_star'] === null)
            {
              $this->basic->delete_data('google_review_reply_settings',['user_id'=>$this->user_id,'location_id'=>$selected_location_table_id]);
            }
            
            $message = $this->lang->line('Review reply settings deleted successfully');
            echo json_encode([
                'status' => true,
                'message' => $message
            ]);
            exit;
        } else {
            $message = $this->lang->line('Something went wrong while deleting review reply settings');
            echo json_encode([
                'status' => false,
                'message' => $message
            ]);
            exit;
        }
    }

    public function post_list()
    {
        $this->load->library('google_my_business');
        $gmb = $this->google_my_business;

        $location_table_id = $this->session->userdata('location_list_location_table_id');
        $location_info = $this->basic->get_data('google_business_locations',['where'=>['id'=>$location_table_id]]);
        $location_id = isset($location_info[0]['location_id']) ? $location_info[0]['location_id'] : null;

        // Holds posts list
        $posts_list = null;

        try {
            $posts_list = $gmb->postsList($location_id);
        } catch (Google_Service_Exception $e) {
            $e->getMessage();
        } catch (Google_Exception $e) {
            $e->getMessage();
        } catch (\Exception $e) {
            $e->getMessage();
        }

        $prepared_posts_list = [];
        if (is_array($posts_list['localPosts'])) {
            foreach ($posts_list['localPosts'] as $key => $post) {
                $photo = isset($post->getMedia()[0]) && is_object($post->getMedia()[0]) ? $post->getMedia()[0]->getGoogleUrl() : null;
                if ($post->getCallToAction()) {
                    $prepared_posts_list[] = [
                        'post_type' => 'callToAction',
                        'name' => $post->getName(),
                        'summary' => $post->getSummary(),
                        'searchUrl' => $post->getSearchUrl(),
                        'photo' => $photo,
                        'createTime' => $post->getCreateTime(),
                        'actionType' => $post->getCallToAction()->getActionType(),
                        'url' => $post->getCallToAction()->getUrl(),
                    ];
                } elseif ($post->getOffer()) {
                    $photo = isset($post->getMedia()[0]) && is_object($post->getMedia()[0]) ? $post->getMedia()[0]->getGoogleUrl() : null;
                    $prepared_posts_list[] = [
                        'post_type' => 'offer',
                        'name' => $post->getName(),
                        'summary' => $post->getSummary(),
                        'searchUrl' => $post->getSearchUrl(),
                        'photo' => $photo,
                        'createTime' => $post->getCreateTime(),
                        'couponCode' => $post->getOffer()->getCouponCode(),
                        'redeemUrl' => $post->getOffer()->getRedeemOnlineUrl(),
                    ];
                } elseif ($post->getEvent()) {
                    $start_date_time = '';
                    $end_date_time = '';

                    if ($post->getEvent()->getSchedule()) {
                        $schedule = $post->getEvent()->getSchedule();
                        $startDate = $schedule->getStartDate();
                        $startTime = $schedule->getStartTime();

                        if (is_object($startDate)) {
                            $start_date_time = $startDate->getYear() . '-' . $startDate->getMonth() . '-' . $startDate->getDay();
                        }
                        if (is_object($startTime)) {
                            $start_date_time .= $startTime->getHours() ? $startTime->getHours() . ':' . $startTime->getMinutes() : '';
                        }

                        $endDate = $schedule->getEndDate();
                        $endTime = $schedule->getEndTime();

                        if (is_object($endDate)) {
                            $end_date_time = $endDate->getYear() . '-' . $endDate->getMonth() . '-' . $endDate->getDay();
                        }
                        if (is_object($endTime)) {
                            $end_date_time .= $endTime->getHours() ? $endTime->getHours() . ':' . $endTime->getMinutes() : '';
                        }
                    }

                    $photo = isset($post->getMedia()[0]) && is_object($post->getMedia()[0]) ? $post->getMedia()[0]->getGoogleUrl() : null;
                    $prepared_posts_list[] = [
                        'post_type' => 'event',
                        'name' => $post->getName(),
                        'title' => $post->getEvent()->getTitle(),
                        'summary' => $post->getSummary(),
                        'searchUrl' => $post->getSearchUrl(),
                        'photo' => $photo,
                        'createTime' => $post->getCreateTime(),
                        'start_date_time' => $start_date_time,
                        'end_date_time' => $end_date_time,
                    ];
                }
            }
        }

        $data['posts_list'] = $prepared_posts_list;
        $data['page_title'] = $this->lang->line('Posts list');
        $data['title'] = $this->lang->line('Posts list');
        $data['body'] = 'manage_location/posts_list';
        $data['iframe'] = 1;
        $this->_viewcontroller($data);
    }

    public function review_list()
    {
        $this->load->library('google_my_business');
        $gmb = $this->google_my_business;

        $location_table_id = $this->session->userdata('location_list_location_table_id');
        $location_info = $this->basic->get_data('google_business_locations',['where'=>['id'=>$location_table_id]]);
        $location_id = isset($location_info[0]['location_id']) ? $location_info[0]['location_id'] : null;
        $location_name = isset($location_info[0]['location_display_name']) ? $location_info[0]['location_display_name'] : '';

        // Holds reviews list
        $reviews_list = null;

        try {
            $reviews_list = $gmb->reviewsList($location_id);
        } catch (Google_Service_Exception $e) {
            $e->getMessage();
        } catch (Google_Exception $e) {
            $e->getMessage();
        } catch (\Exception $e) {
            $e->getMessage();
        }

        $reviews = [];
        if (isset($reviews_list['reviews'])) {
            foreach ($reviews_list['reviews'] as $key => $review) {

                $anonymous = '';
                $displayName = '';
                $profilePhotoUrl = '';

                if (is_object($review->getReviewer())) {
                    $reviewer = $review->getReviewer();

                    $anonymous = $reviewer->getIsAnonymous();
                    $displayName = $reviewer->getDisplayName();
                    $profilePhotoUrl = $reviewer->getProfilePhotoUrl();
                }

                $reviewReplies = [];
                if (is_object($review->getReviewReply())) {
                    $reviewReplies['comment'] = $review->getReviewReply()->getComment();
                    $reviewReplies['updateTime'] = $review->getReviewReply()->getUpdateTime();
                }

                $reviews[] = [
                    'comment' => $review->getComment(),
                    'name' => $review->getName(),
                    'reviewId' => $review->getReviewId(),
                    'starRating' => $review->getStarRating(),
                    'anonymous' => $anonymous,
                    'displayName' => $displayName,
                    'profilePhotoUrl' => $profilePhotoUrl,
                    'locationName' => $location_name,
                    'createTime' => $review->getCreateTime(),
                    'reviewReply' => $reviewReplies,
                ];
            }
        }

        $data['reviews'] = $reviews;
        $data['page_title'] = $this->lang->line('Reviews list');
        $data['title'] = $this->lang->line('Reviews list');
        $data['body'] = 'manage_location/reviews_list';
        $data['iframe'] = 1;
        $this->_viewcontroller($data);
    }

    public function validate_review_id()
    {
        $review_id = $_POST['review_id'];

        if (! preg_match('~accounts/[0-9]+/locations/[0-9]+/reviews/[a-zA-Z0-9\-_]+~ui', $review_id, $matches)) {
            $this->form_validation->set_message('validate_review_id', $this->lang->line('Invalid review ID provided'));
            return false;
        }

        return true;
    }

    public function reply_to_review()
    {
        $this->ajax_check();

        $reply_type = $this->input->post('reply_type', true);

        if (! in_array($reply_type, ['location_manager_index', 'review_report'])) {
            $message = $this->lang->line('Review reply type is invalid');
            echo json_encode([
                'status' => false,
                'message' => $message,
            ]);
            exit;
        }

        $this->form_validation->set_rules('review_id', $this->lang->line('Review ID'), 'callback_validate_review_id');
        $this->form_validation->set_rules(
            'review_star',
            $this->lang->line('Star Rating'),
            'required|in_list[FIVE,FOUR,THREE,TWO,ONE]',
            [
                'in_list' => $this->lang->line('Star rating must be one of FIVE, FOUR, THREE, TWO and ONE'),
            ]
        );

        $this->form_validation->set_rules('reviewer_location_name', $this->lang->line('Location name'), 'required');
        $this->form_validation->set_rules('review_reply_message', $this->lang->line('Reply message'), 'required');

        if ('location_manager_index' == $reply_type) {
            $this->form_validation->set_rules('reviewer_display_name', $this->lang->line('Review name'), 'required');
            $this->form_validation->set_rules('reviewer_profile_photo', $this->lang->line('Profile photo'), 'required');
        }

        if (false === $this->form_validation->run()) {
            $errors = $this->form_validation->error_array();
            $errors = str_replace(['<b>', '</b>'], '', $errors);
            echo json_encode([
                'status' => false,
                'errors' => $errors
            ]);
            exit;
        }

        $review_id = $this->input->post('review_id');
        $review_star = $this->input->post('review_star');
        $review_comment = $this->input->post('review_comment', true);
        $review_reply_message = $this->input->post('review_reply_message', true);
        $review_reply_message = spintax_process($review_reply_message);
        $reviewer_location_name = $this->input->post('reviewer_location_name', true);

        if ('location_manager_index' == $reply_type) {
            $reviewer_display_name = $this->input->post('reviewer_display_name', true);
            $reviewer_profile_photo = $this->input->post('reviewer_profile_photo', true);
        }

        $location_id = explode('/reviews/', $review_id, 2);
        $location_id = $location_id[0];
        $user_id = $this->user_id;
        $reply_time = date('Y-m-d H:i:s');

        $where = [
            'where' => [
                'user_id' => $this->user_id,
                'location_id' => $location_id
            ],
        ];

        $location = $this->basic->get_data('google_business_locations', $where, ['id','location_id'], [], 1);
        if (1 != count($location)) {
            $message = $this->lang->line('You do not have permission to reply to the review');
            echo json_encode([
                'status' => false,
                'message' => $message,
            ]);
            exit;
        }

        $location_table_id = isset($location[0]['id']) ? $location[0]['id'] : null;

        $this->load->library('google_my_business');
        $gmb = $this->google_my_business;

        $error = '';

        try {
            $response = $gmb->replyReview($review_id, $review_reply_message);
        } catch (Google_Service_Exception $e) {
            $error = $e->getMessage();
        } catch (Google_Exception $e) {
            $error = $e->getMessage();
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        if (! empty($error)) {
            $where = [
                'user_id' => $this->user_id,
                'location_id' => $location_table_id,
                'review_id' => $review_id,
            ];

            $data = [
                'error' => $error,
                'reply_time' => date('Y-m-d H:i:s')
            ];

            $this->basic->update_data('google_review_reply_report', $where, $data);

            echo json_encode([
                'status' => false,
                'message' => $error,
            ]);
            exit;
        }

        if ('location_manager_index' == $reply_type) {
            $sql = "
              INSERT INTO `google_review_reply_report` (user_id, location_id, location_display_name, review_id, reviewer_name, reviewer_photo, review_star, review_comment, review_reply, reply_time) 
              VALUES ('{$user_id}', '{$location_table_id}', '{$reviewer_location_name}', '{$review_id}', '{$reviewer_display_name}', '{$reviewer_profile_photo}', '{$review_star}', '{$review_comment}', '{$review_reply_message}', '{$reply_time}') 
              ON DUPLICATE KEY 
              UPDATE `review_reply` = '{$review_reply_message}', `location_display_name` = '{$reviewer_location_name}', `reviewer_name` = '{$reviewer_display_name}', `reviewer_photo` = '{$reviewer_profile_photo}'";
                      } else {
                          $sql = "
              INSERT INTO `google_review_reply_report` (user_id, location_id, location_display_name, review_id, review_star, review_comment, review_reply, reply_time) 
              VALUES ('{$user_id}', '{$location_table_id}', '{$reviewer_location_name}', '{$review_id}', '{$review_star}', '{$review_comment}', '{$review_reply_message}', '{$reply_time}') 
              ON DUPLICATE KEY 
              UPDATE `review_reply` = '{$review_reply_message}', `location_display_name` = '{$reviewer_location_name}'";
        }

        $this->basic->execute_complex_query($sql);

        if (! $this->db->affected_rows() > 0) {
            $message = $this->lang->line('Unable to update review-reply data to database');
            echo json_encode([
                'status' => false,
                'message' => $message,
            ]);
            exit;
        }

        $message = $this->lang->line('Reply to the review updated successfully');
        echo json_encode([
            'status' => true,
            'message' => $message,
        ]);
        exit;
    }

    public function delete_reply_to_review()
    {
        $this->ajax_check();

        $this->form_validation->set_rules('review_id', $this->lang->line('Review ID'), 'callback_validate_review_id');

        if (false === $this->form_validation->run()) {
            $message = $this->lang->line('Invalid review reply ID provided');
            echo json_encode([
                'status' => false,
                'errors' => $message
            ]);
            exit;
        }

        $review_id = $this->input->post('review_id');

        $this->load->library('google_my_business');
        $gmb = $this->google_my_business;

        $error = '';

        try {
            $response = $gmb->deleteReply($review_id);
        } catch (Google_Service_Exception $e) {
            $error = $e->getMessage();
        } catch (Google_Exception $e) {
            $error = $e->getMessage();
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        if (! empty($error)) {
            $where = [
                'user_id' => $this->user_id,
                'location_id' => $this->session->userdata('location_list_location_table_id'),
                'review_id' => $review_id,
            ];

            $data = [
                'error' => $error,
                'reply_time' => date('Y-m-d H:i:s')
            ];

            $this->basic->update_data('google_review_reply_report', $where, $data);

            echo json_encode([
                'status' => false,
                'message' => $error,
            ]);
            exit;
        }

        $where = [
            'user_id' => $this->user_id,
            'review_id' => $review_id,
            'location_id' => $this->session->userdata('location_list_location_table_id'),
        ];

        $this->basic->update_data('google_review_reply_report', $where, [
            'review_reply' => null,
            'reply_time' => date('Y-m-d H:i:s'),
        ]);

        $message = $this->lang->line('Review reply has been deleted successfully');
        echo json_encode([
            'status' => true,
            'message' => $message,
        ]);
        exit;
    }

    public function question_list()
    {
        $this->load->library('google_my_business');
        $gmb = $this->google_my_business;

        $location_table_id = $this->session->userdata('location_list_location_table_id');
        $location_info = $this->basic->get_data('google_business_locations',['where'=>['id'=>$location_table_id]]);
        $location_id = isset($location_info[0]['location_id']) ? $location_info[0]['location_id'] : null;

        // Holds questions list
        $question_list = null;

        try {
            $question_list = $gmb->questionsList($location_id);
        } catch (Google_Service_Exception $e) {
            $e->getMessage();
        } catch (Google_Exception $e) {
            $e->getMessage();
        } catch (\Exception $e) {
            $e->getMessage();
        }

        $questions = [];
        if (isset($question_list['questions'])) {
            foreach ($question_list['questions'] as $key => $question) {
                $displayName = '';
                $profilePhotoUrl = '';
                if (is_object($question->getAuthor())) {
                    $author = $question->getAuthor();

                    $displayName = $author->getDisplayName();
                    $profilePhotoUrl = $author->getProfilePhotoUrl();
                }


                $answers = [];
                if (is_array($question->getTopAnswers())) {
                    foreach ($question->getTopAnswers() as $topAnswer) {
                        $questionerAnswerInfo = [
                            'text' => $topAnswer->getText(),
                            'createTime' => $topAnswer->getUpdateTime(),
                        ];

                        if (is_object($topAnswer)) {
                            $questionerAuthor = $topAnswer->getAuthor();
                            $questionerAuthorInfo = [
                                'displayName' => $questionerAuthor->getDisplayName(),
                                'profilePhotoUrl' => $questionerAuthor->getProfilePhotoUrl(),
                            ];
                        }

                        $answers[] = array_merge($questionerAnswerInfo, $questionerAuthorInfo);
                    }
                }

                $questions[] = [
                    'text' => $question->getText(),
                    'name' => $question->getName(),
                    'displayName' => $displayName,
                    'profilePhotoUrl' => $profilePhotoUrl,
                    'createTime' => $question->getCreateTime(),
                    'displayName' => $displayName,
                    'profilePhotoUrl' => $profilePhotoUrl,
                    'answers' => $answers,
                ];
            }
        }

        $data['questions'] = $questions;
        $data['page_title'] = $this->lang->line('Questions list');
        $data['title'] = $this->lang->line('Questions list');
        $data['body'] = 'manage_location/questions_list';
        $data['iframe'] = 1;
        $this->_viewcontroller($data);
    }

    public function validate_question_id()
    {
        $question_id = $_POST['question_id'];

        if (! preg_match('~accounts/[0-9]+/locations/[0-9]+/questions/[a-zA-Z0-9\-_]+~i', $question_id, $matches)) {
            $this->form_validation->set_message('validate_question_id', $this->lang->line('Invalid question ID provided'));
            return false;
        }

        return true;
    }

    public function answer_to_question()
    {
        $this->ajax_check();

        $this->form_validation->set_rules('question_id', $this->lang->line('Question ID'), 'callback_validate_question_id');
        $this->form_validation->set_rules('answer_to_question_message', $this->lang->line('Answer'), 'required|max_length[4096]');

        if (false === $this->form_validation->run()) {
            $errors = $this->form_validation->error_array();
            $errors = str_replace(['<b>', '</b>'], '', $errors);
            echo json_encode([
                'status' => false,
                'errors' => $errors
            ]);
            exit;
        }

        $question_id = $this->input->post('question_id');
        $question_text = $this->input->post('question_text', true);
        $answer_to_question_message = $this->input->post('answer_to_question_message', true);
        $location_id = explode('/questions/', $question_id, 2);
        $location_id = $location_id[0];

        $where = [
            'where' => [
                'user_id' => $this->user_id,
                'location_id' => $location_id
            ],
        ];

        $location = $this->basic->get_data('google_business_locations', $where, ['id','location_id'], [], 1);
        if (1 != count($location)) {
            $message = $this->lang->line('You do not have permission to answer the question');
            echo json_encode([
                'status' => false,
                'message' => $message,
            ]);
            exit;
        }

        $location_table_id = isset($location[0]['id']) ? $location[0]['id'] : null;

        $this->load->library('google_my_business');
        $gmb = $this->google_my_business;

        $error = '';

        try {
            $response = $gmb->answerQuestion($question_id, $answer_to_question_message);
        } catch (Google_Service_Exception $e) {
            $error = $e->getMessage();
        } catch (Google_Exception $e) {
            $error = $e->getMessage();
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        if (! empty($error)) {

            echo json_encode([
                'status' => false,
                'message' => $error,
            ]);
            exit;
        }

        $message = $this->lang->line('Answer to the question created successfully');
        echo json_encode([
            'status' => true,
            'message' => $message,
        ]);
        exit;
    }

    public function delete_question_answer()
    {
        $this->ajax_check();

        // accounts/107745512734031207626/locations/14692206365244175995/questions/AIe9_BGhYIk2KhOdF8fekWDvq8JUw-YhSYIY-XWMwRYsbtV-iL3nuz2hq1edih4FpamwR9UrnyRNx15zYxZtvLxrw2fIEkxv-UzRxl124s_fVjmbXyxaLDV3il5u7wGAyGEFrGhNMYH_
        $question_id = $this->input->post('question_id', true);
        
        if (! preg_match('@^accounts/[0-9]+/locations/[0-9]+/questions/[0-9a-zA-Z\-_]+$@i', $question_id)) {
            echo json_encode([
                'status' => false,
                'message' => $this->lang->line('Bad request')
            ]);
            exit;
        }

        // Inits google_my_business
        $params['gmb_user_table_id'] = $this->session->userdata('google_mybusiness_user_table_id');
        $params['redirectUri'] = '';
        $this->load->library('google_my_business', $params);
        $gmb = $this->google_my_business;

        if (is_object($gmb)) {
            try {
                $response = $gmb->deleteQuestionAnswer($question_id);
            } catch (Google_Service_Exception $e) {
                $error = $e->getMessage();
            } catch (Google_Exception $e) {
                $error = $e->getMessage();
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }

            // @TODO
            // Log or send email the error message
        }

        if (! empty($error)) {
            echo json_encode([
                'status' => false,
                'message' => $error,
            ]);
            exit;
        }

        echo json_encode([
            'status' => true,
            'message' => $this->lang->line('The answer to the question has been deleted successfully'),
        ]);
        exit;
    }

    public function review_report()
    {
      $gmb_user_table_id = $this->session->userdata('google_mybusiness_user_table_id');
      $locations = $this->basic->get_data('google_business_locations',['where'=>['user_account_id'=>$gmb_user_table_id,'user_id'=>$this->user_id]],['id','location_display_name']);

      $data['locations'] = $locations;
      $data['page_title'] = $this->lang->line('Review report');
      $data['title'] = $this->lang->line('Review report');
      $data['body'] = 'manage_location/review_report';
      $this->_viewcontroller($data);
    }

    public function review_report_data()
    {
        $this->ajax_check();

        $review_star     = trim($this->input->post("review_star", true));
        $location_table_id   = trim($this->input->post("location_name", true));
        $searching       = trim($this->input->post("searching", true));
        $post_date_range = $this->input->post("post_date_range", true);

        $display_columns = array('id', 'reviewer_photo', 'reviewer_name', 'review_star', 'review_comment', 'review_reply', 'actions', 'location_display_name', 'reply_time', 'error');
        $search_columns  = array('review_comment','review_reply','reply_time');

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $start = isset($_POST['start']) ? intval($_POST['start']) : 0;
        $limit = isset($_POST['length']) ? intval($_POST['length']) : 10;
        $sort_index = isset($_POST['order'][0]['column']) ? strval($_POST['order'][0]['column']) : 2;
        $sort = isset($display_columns[$sort_index]) ? $display_columns[$sort_index] : 'id';
        $order = isset($_POST['order'][0]['dir']) ? strval($_POST['order'][0]['dir']) : 'desc';
        $order_by = $sort . " " . $order;

        $where_simple = array();

        if(! empty($post_date_range)) {
            $exp        = explode('|', $post_date_range);
            $from_date  = isset($exp[0]) ? $exp[0] : '';
            $to_date    = isset($exp[1]) ? $exp[1] : '';

            if('Invalid date' != $from_date && 'Invalid date' != $to_date) {
                $from_date = date('Y-m-d', strtotime($from_date));
                $to_date   = date('Y-m-d', strtotime($to_date));
                $where_simple["Date_Format(reply_time, '%Y-%m-%d') >="] = $from_date;
                $where_simple["Date_Format(reply_time, '%Y-%m-%d') <="] = $to_date;
            }
        }

        if($review_star !="") {
            $where_simple['google_review_reply_report.review_star'] = $review_star;
        }
        if($location_table_id !="") {
            $where_simple['google_review_reply_report.location_id'] = $location_table_id;
        }
        if($searching !="") {
            $where_simple['google_review_reply_report.review_reply like'] = "%" . $searching . "%";
        }

        $where_simple['google_review_reply_report.user_id'] = $this->user_id;

        $where  = array('where' => $where_simple);

        if($searching !="") {
            $or_where_simple['google_review_reply_report.review_comment like'] = "%" . $searching . "%";
            $where['or_where'] = $or_where_simple;
        }

        $table = 'google_review_reply_report';
        $info = $this->basic->get_data($table,$where,$select='',$join='',$limit,$start,$order_by,$group_by='');

        // Gets total rows
        $total_rows_array = $this->basic->count_row($table,$where,$count=$table.".id",$join,$group_by='');
        $total_result = $total_rows_array[0]['total_rows'];

        // Prepares some vars
        for($i = 0; $i < count($info); $i++) {

            if ($info[$i]['reviewer_photo']) {
                $info[$i]['reviewer_photo'] = '<img src="' . $info[$i]['reviewer_photo'] . '" class="img-fluid" width="32" alt="' . $info[$i]['reviewer_name'] . '">';
            } else {
                $info[$i]['reviewer_photo'] = '<img src="' . base_url('upload/xerobiz/dummy_author.png') . '" class="img-fluid" width="32" alt="' . $info[$i]['reviewer_name'] . '">';
            }

            // Report campaign action
            $createReview = '<a 
                class="btn btn-circle btn-outline-primary update-review-reply" 
                data-toggle="tooltip" 
                title="' . $this->lang->line("Update reply") . '" 
                data-review-id="' . $info[$i]['review_id'] . '" 
                data-review-star="' . $info[$i]['review_star'] . '" 
                data-review-reply="' . $info[$i]['review_reply'] . '" 
                data-review-comment="' . $info[$i]['review_comment'] . '"
                data-location-name="' . $info[$i]['location_display_name'] . '"
                href="#"
            >
                <i class="fas fa-reply"></i>
            </a>';

            // Delete campaign action
            $deleteReview = '<a 
                class="btn btn-circle btn-outline-danger delete-review-reply" 
                data-toggle="tooltip" 
                title="' . $this->lang->line("Delete reply") . '" 
                data-review-id="' . $info[$i]['review_id'] . '" 
                href="#"
            >
                <i class="fas fa-trash-alt"></i>
            </a>';

            // Defines start rating
            if ('FIVE' == $info[$i]['review_star']) {
                $info[$i]['review_star'] = '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>';
            } elseif ('FOUR' == $info[$i]['review_star']) {
                $info[$i]['review_star'] = '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>';
            } elseif ('THREE' == $info[$i]['review_star']) {
                $info[$i]['review_star'] = '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>';
            } elseif ('TWO' == $info[$i]['review_star']) {
                $info[$i]['review_star'] = '<i class="fas fa-star text-warning"></i><i class="fas fa-star text-warning"></i>';
            } elseif ('ONE' == $info[$i]['review_star']) {
                $info[$i]['review_star'] = '<i class="fas fa-star text-warning"></i>';
            }

            if ($info[$i]['reply_time']) {
                $info[$i]['reply_time'] = "<div style='min-width:120px !important;'>" . date("M j, y H:i", strtotime($info[$i]['reply_time'])) . "</div>";
            }

            // Action section started from here
            $account_count = 2;
            $action_width = ($account_count*47)+20;
            $info[$i]['actions'] = '<div class="dropdown d-inline dropright">
            <button class="btn btn-outline-primary dropdown-toggle no_caret" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-briefcase"></i></button>
            <div class="dropdown-menu mini_dropdown text-center" style="width:'.$action_width.'px !important">';

            $info[$i]['actions'] .= $createReview;
            $info[$i]['actions'] .= $deleteReview;

            $info[$i]['actions'] .= "</div></div><script>$('[data-toggle=\"tooltip\"]').tooltip();</script>";
        }

        $data['draw'] = (int)$_POST['draw'] + 1;
        $data['recordsTotal'] = $total_result;
        $data['recordsFiltered'] = $total_result;
        $data['data'] = convertDataTableResult($info, $display_columns ,$start, $primary_key = "id");

        echo json_encode($data);
    }


    public function location_insights_basic($location_id = null)
    {
        if (null == $location_id) {
            return $this->error_404();
        }

        $where = [
            'where' => [
                'id' => $location_id,
                'deleted' => '0',
                'user_id' => $this->user_id
            ],
        ];

        $select = ['id', 'location_id', 'user_account_id', 'location_display_name'];

        $location = $this->basic->get_data('google_business_locations', $where, $select, [], 1);

        $no_data = false;
        if (1 != count($location)) {
            $no_data = true;
        }

        // Initial vars
        $error = '';
        $response = [];

        $location_name = '';
        $user_account_id = '';
        $location_table_id = '';
        $location_display_name = '';

        $start_date_time = '';
        $end_date_time = '';

        $no_data_message = '';

        if (false == $no_data) {
            $location_name = isset($location[0]['location_id'])
                ? $location[0]['location_id']
                : null;

            $location_table_id = isset($location[0]['id'])
                ? $location[0]['id']
                : null;

            $user_account_id = isset($location[0]['user_account_id'])
                ? $location[0]['user_account_id']
                : null;

            $location_display_name = isset($location[0]['location_display_name'])
                ? $location[0]['location_display_name']
                : null;

            // Inits google_my_business
            $params['gmb_user_table_id'] = $user_account_id;
            $params['redirectUri'] = '';
            $this->load->library('google_my_business', $params);
            $gmb = $this->google_my_business;

            if ($_POST) {
                $start_date_time_value = $this->input->post('from_date', true);
                $end_date_time_value = $this->input->post('to_date', true);
                $start_date_time = new DateTime($start_date_time_value);
                $end_date_time = new DateTime($end_date_time_value);

                if ($end_date_time <= $start_date_time) {
                    $start_date_time = new DateTime();
                    $start_date_time->modify('-2 months');
                    $end_date_time = new DateTime();
                }

                if ($end_date_time > $start_date_time) {
                    $too_much_interval = $start_date_time->diff($end_date_time);
                    $days = $too_much_interval->format('%R%a');
                    $date_time_difference = (int) $days;

                    if ($date_time_difference > 186) {
                        $start_date_time = new DateTime($start_date_time_value);
                        $end_date_time = new DateTime($start_date_time_value);
                        $interval = new DateInterval('P6M');
                        $end_date_time->add($interval);
                    }
                }
            } else {
                $start_date_time = new DateTime();
                $start_date_time->modify('-1 months');
                $end_date_time = new DateTime();
            }

            if (is_object($gmb)) {
                // accounts/107745512734031207626/locations/14692206365244175995
                $account_name = explode('/locations/', $location_name, 2);

                try {
                    $response = $gmb->locationInsightsBasicMetric(
                        $account_name[0],
                        [
                            $location_name,
                        ],
                        'ALL',
                        [
                            'AGGREGATED_DAILY',
                        ],
                        $start_date_time,
                        $end_date_time
                    );
                } catch (Google_Service_Exception $e) {
                    $error = $e->getMessage();
                } catch (Google_Exception $e) {
                    $error = $e->getMessage();
                } catch (\Exception $e) {
                    $error = $e->getMessage();
                }

                // @TODO
                // Log or send email the error message
            }
        }

        if (! count($response)) {
            $no_data = true;
        }

        $metrics = [
            'QUERIES_DIRECT',
            'QUERIES_INDIRECT',
            'QUERIES_CHAIN',
            'VIEWS_MAPS',
            'VIEWS_SEARCH',
            'ACTIONS_WEBSITE',
            'ACTIONS_PHONE',
            'ACTIONS_DRIVING_DIRECTIONS',
            'PHOTOS_VIEWS_MERCHANT',
            'PHOTOS_VIEWS_CUSTOMERS',
            'PHOTOS_COUNT_MERCHANT',
            'PHOTOS_COUNT_CUSTOMERS',
            'LOCAL_POST_VIEWS_SEARCH',
            'LOCAL_POST_ACTIONS_CALL_TO_ACTION',
        ];

        // Prepares array based on metrics
        $location_insights = [];
        if (is_object($response)) {
            foreach ($response as $metric) {
                if (is_array($metric->getMetricValues())) {

                    $i = 0;
                    // Loops 14 times
                    foreach ($metric->getMetricValues() as $metricValue) {
                        // Loops 30 times by default based on date range
                        foreach ($metricValue->getDimensionalValues() as $dimensionalValue) {
                            $startDateTime = '';
                            $value = $dimensionalValue->getValue();
                            if (is_object($dimensionalValue->getTimeDimension())) {
                                $timeRange = $dimensionalValue
                                    ->getTimeDimension()
                                    ->getTimeRange();
                                if (is_object($timeRange)) {
                                    $startDateTime = $timeRange->getStartTime();
                                }
                            }

                            $stringDateTime = date('M j, Y', strtotime($startDateTime));
                            $metricType = $metricValue->getMetric();

                            if (in_array($metricType, $metrics)) {
                                $location_insights[$metricType]['date'][] = $stringDateTime;
                                $location_insights[$metricType]['value'][] = $value ? $value : 0;
                            } else {
                                $location_insights[$metrics[$i]]['date'][] = $stringDateTime;
                                $location_insights[$metrics[$i]]['value'][] = mt_rand(100, 1000);
                            }
                        }

                        $i++;
                    }
                }
            }
        }

        $data['no_data'] = $no_data
            ? (! empty($error)
                ? $error
                : $this->lang->line("No data found."))
            : '';

        $data['post_insights'] = $location_insights;
        $data['location_name'] = $location_name;
        $data['location_table_id'] = $location_table_id;
        $data['location_display_name'] = $location_display_name;
        $data['from_date'] = is_object($start_date_time) ? $start_date_time->format('Y-m-d') : date('Y-m-d');
        $data['to_date'] = is_object($end_date_time) ? $end_date_time->format('Y-m-d') : date('Y-m-d');
        $data['title'] = $this->lang->line('Location Insights');
        $data['page_title'] = $this->lang->line('Location Insights');
        $data['body'] = 'manage_location/location_insights_basic';
        $this->_viewcontroller($data);
    }



}