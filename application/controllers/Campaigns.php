<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once("Home.php");
require_once(APPPATH . 'libraries/Google_My_Business/Exception.php');
require_once(APPPATH . 'libraries/Google_My_Business/Service/Exception.php');

class Campaigns extends Home
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in') != 1)
        redirect('home/login_page', 'location');
        if($this->session->userdata('user_type') != 'Admin' && !in_array(15,$this->module_access))
        redirect('home/login_page', 'location');
        $this->important_feature();
        $this->member_validity();
    }

    public function index()
    {
        $data['page_title'] = $this->lang->line('Campaigns');
        $data['title'] = $this->lang->line('Campaigns');
        $data['body'] = 'mybusiness/settings';
        $this->_viewcontroller($data);
    }

    public function posts()
    {
        $where = [
            'where' => [
                'user_account_id' => $this->session->userdata('google_mybusiness_user_table_id'),
            ],
        ];

        $locations = $this->basic->get_data('google_business_locations', $where, ['location_display_name']);

        $data['locations'] = $locations;
        $data['page_title'] = $this->lang->line('Post campaign list');
        $data['title'] = $this->lang->line('Post campaign list');
        $data['body'] = 'mybusiness/posts';
        $this->_viewcontroller($data);
    }


    public function posts_data()
    {
        $this->ajax_check();

        $post_type       = trim($this->input->post("post_type", true));
        $location_name   = trim($this->input->post("location_name", true));
        $searching       = trim($this->input->post("searching", true));
        $post_date_range = $this->input->post("post_date_range", true);

        $display_columns = array("#", 'id', 'campaign_name', 'post_type', 'title', 'actions', 'status', 'schedule_time', 'error');
        $search_columns  = array('campaign_name','post_type','schedule_time');

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $start = isset($_POST['start']) ? intval($_POST['start']) : 0;
        $limit = isset($_POST['length']) ? intval($_POST['length']) : 10;
        $sort_index = isset($_POST['order'][0]['column']) ? strval($_POST['order'][0]['column']) : 2;
        $sort = isset($display_columns[$sort_index]) ? $display_columns[$sort_index] : 'id';
        $order = isset($_POST['order'][0]['dir']) ? strval($_POST['order'][0]['dir']) : 'desc';
        $order_by = $sort . " " . $order;

        $where_simple = array();

        if(! empty($post_date_range)) {
            $exp        = explode('|', $post_date_range);
            $from_date  = isset($exp[0]) ? $exp[0] : "";
            $to_date    = isset($exp[1]) ? $exp[1] : "";

            if("Invalid date" != $from_date && "Invalid date" != $to_date) {
                $from_date = date('Y-m-d', strtotime($from_date));
                $to_date   = date('Y-m-d', strtotime($to_date));
                $where_simple["Date_Format(created_at,'%Y-%m-%d') >="] = $from_date;
                $where_simple["Date_Format(created_at,'%Y-%m-%d') <="] = $to_date;
            }
        }

        if($post_type !="") {
            $where_simple['google_posts_campaign.post_type'] = $post_type;
        }
        if($location_name !="") {
            $where_simple['google_business_locations.location_display_name'] = $location_name;
        }
        if($searching !="") {
            $where_simple['google_posts_campaign.campaign_name like'] = "%" . $searching . "%";
        }

        $where_simple['google_posts_campaign.user_id'] = $this->user_id;
        $where_simple['google_posts_campaign.deleted'] = '0';

        $where  = array('where' => $where_simple);
        $select = array(
            'google_posts_campaign.*',
            'google_business_locations.id AS location_id',
            'google_business_locations.location_display_name'
        );
        $join   = array('google_business_locations' => 'google_posts_campaign.location_table_id=google_business_locations.id,left');

        $table = 'google_posts_campaign';
        $info = $this->basic->get_data($table,$where,$select,$join,$limit,$start,$order_by,$group_by='');

        // Gets total rows
        $total_rows_array = $this->basic->count_row($table,$where,$count=$table.".id",$join,$group_by='');
        $total_result = $total_rows_array[0]['total_rows'];

        // Prepares some vars
        for($i = 0; $i < count($info); $i++) {
            // Defines statuses
            $post_type = $info[$i]['post_type'];
            $status = $info[$i]['status'];
            if ('0' == $status) {
                $info[$i]['status'] = '<div style="min-width:120px;" class="text-danger"><i class="fas fa-hourglass-start"></i> ' . $this->lang->line("Pending") . '</div>';
            } elseif ('1' == $status) {
                $info[$i]['status'] = '<div style="min-width:120px;" class="text-info"><i class="fas fa-spinner"></i> ' . $this->lang->line("Processing") . '</div>';
            } elseif ('2' == $status) {
                $info[$i]['status'] = '<div style="min-width:120px;" class="text-success"><i class="fas fa-check-circle"></i> ' . $this->lang->line("Completed") . '</div>';
            }

            // Defines post types
            if ('cta_post' == $post_type) {
                $info[$i]['post_type'] = '<div style="min-width:70px !important;"><i class="fa fa-file-alt"></i> ' . $this->lang->line("CTA") . '</div>';
            }
            if ('event_post' == $post_type) {
                $info[$i]['post_type'] = '<div style="min-width:70px !important;"><i class="fa fa-image"></i> ' . $this->lang->line("EVENT") . '</div>';
            }
            if ('offer_post' == $post_type) {
                $info[$i]['post_type'] = '<div style="min-width:70px !important;"><i class="fa fa-video"></i> ' . $this->lang->line("OFFER") . '</div>';
            }

            // Defines post titles
            if ('cta_post' == $post_type) {
                $info[$i]['title'] = $info[$i]['summary'];
            }

            if ('event_post' == $post_type) {
                $info[$i]['title'] = $info[$i]['event_post_title'];
            }

            if ('offer_post' == $post_type) {
                $info[$i]['title'] = $info[$i]['summary'];
            }

            if ($info[$i]['schedule_time']) {
                $info[$i]['schedule_time'] = "<div style='min-width:120px !important;'>" . date("M j, y H:i", strtotime($info[$i]['schedule_time'])) . "</div>";
            } else {
                $info[$i]['schedule_time'] = "<div style='min-width:120px !important;' class='text-muted'><i class='fas fa-exclamation-circle'></i> " . $this->lang->line('Not Scheduled') . "</div>";
            }

            // visit post action
            if ('cta_post' == $post_type) {
                $cta_action_type = $info[$i]['cta_action_type'];
                if ('call' == strtolower($cta_action_type)) {

                }
                // $visit_post = "<a target='_blank' href='" . $info[$i]['post_url'] . "' data-toggle='tooltip' title='" . $this->lang->line("Visit Post") . "' class='btn btn-circle btn-outline-info'><i class='fas fa-hand-point-right'></i></a>";
            } elseif ('event_post' == $post_type) {
                // $visit_post = "<a data-toggle='tooltip' title='" . $this->lang->line("not published yet.") . "' class='btn btn-circle btn-light pointer text-muted'><i class='fas fa-hand-point-right'></i></a>";
            } elseif ('offer_post' == $post_type) {

            }

            // Report campaign action
            if ('2' == $status) {
                $reportPost = '<a class="btn btn-circle btn-outline-info campaign-report" data-toggle="tooltip" title="' . $this->lang->line("Campaign report") . '" data-post-id="' . $info[$i]['id'] . '" data-campaign-name="' . $info[$i]['campaign_name'] . '" data-toggle="modal" data-target="#campaign-report-modal" href="#"><i class="fas fa-eye"></i></a>';
            } else {
                $reportPost = '<a class="btn btn-circle btn-outline-info disabled" data-toggle="tooltip" title="' . $this->lang->line("Campaign report") . '" data-campaign-name="' . $info[$i]['campaign_name'] . '" href="#"><i class="fas fa-eye"></i></a>';
            }

            // Edit campaign action
            if ('0' == $status) {
                $editPost = '<a class="btn btn-circle btn-outline-warning" href="' . base_url('campaigns/edit_post/') . $info[$i]['id'] . '" data-toggle="tooltip" title="' . $this->lang->line("Edit Campaign") . '"><i class="fas fa-edit"></i></a>';
            } else {
                $editPost = "<a class='btn btn-circle btn-light pointer text-muted' data-toggle='tooltip' title='" . $this->lang->line("Only pending and scheduled campaigns are editable") . "'><i class='fas fa-edit'></i></a>";
            }

            // Delete campaign action
            $deletePost = '<a class="btn btn-circle btn-outline-danger delete" data-toggle="tooltip" title="' . $this->lang->line("Delete Campaign") . '" id="' . $info[$i]['id'] . '" href="#"><i class="fas fa-trash-alt"></i></a>';

            // Action section started from here
            $account_count = 4;
            $action_width = ($account_count*47)+20;
            $info[$i]['actions'] = '<div class="dropdown d-inline dropright">
            <button class="btn btn-outline-primary dropdown-toggle no_caret" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-briefcase"></i></button>
            <div class="dropdown-menu mini_dropdown text-center" style="width:'.$action_width.'px !important">';

            $info[$i]['actions'] .= $reportPost;
            $info[$i]['actions'] .= $editPost;
            $info[$i]['actions'] .= $deletePost;

            $info[$i]['actions'] .= "</div></div><script>$('[data-toggle=\"tooltip\"]').tooltip();</script>";
        }

        $data['draw'] = (int)$_POST['draw'] + 1;
        $data['recordsTotal'] = $total_result;
        $data['recordsFiltered'] = $total_result;
        $data['data'] = convertDataTableResult($info, $display_columns ,$start, $primary_key = "id");

        echo json_encode($data);
    }

    public function create_post()
    {
        $where = [
            'where' => [
                'user_id' => $this->user_id,
                'user_account_id' => $this->session->userdata('google_mybusiness_user_table_id'),
            ],
        ];

        $locations = $this->basic->get_data('google_business_locations', $where, ['id', 'location_display_name']);

        $prepared_locations = [];
        if (count($locations)) {
            foreach ($locations as $location) {
                $location_key = $location['id'] . '-' . $location['location_display_name'];
                $prepared_locations[$location_key] = $location['location_display_name'];
            }
        }

        // Saves location table id in session
        $array_keys = implode('|', array_keys($prepared_locations));
        $this->session->set_userdata('gmb_location_table_id_array_keys', $array_keys);

        $data['locations'] = $prepared_locations;
        $actionTypes = [
            '' => $this->lang->line('Please Select'),
            'BOOK' => 'BOOK',
            'ORDER' => 'ORDER',
            'SHOP' => 'SHOP',
            'LEARN_MORE' => 'LEARN_MORE',
            'SIGN_UP' => 'SIGN_UP',
            'CALL' => 'CALL'
        ];
        $data['actionTypes'] = $actionTypes;
        $data['time_zone'] = $this->_time_zone_list();
        $data['page_title'] = $this->lang->line('Create campaign');
        $data['title'] = $this->lang->line('Create campaign');
        $data['body'] = 'mybusiness/create_post';
        $this->_viewcontroller($data);
    }

    public function campaign_report()
    {
        $this->ajax_check();

        $post_id = (int) $this->input->post('post_id');
        $where = [
            'where' => [
                'id' => $post_id,
                'user_id' => $this->user_id,
                'status' => '2',
                'deleted' => '0'
            ],
        ];

        $campaign = $this->basic->get_data('google_posts_campaign', $where, [], [], 1);
        $campaign_data = $campaign[0];

        $html = "";
        if(count($campaign) != 1) {
            $html .= '<div class="col-12 p-0">
                    <article class="article article-style-c shadow-none mb-0">
                        <div class="article-header">
                            <div class="article-image" data-background="'.base_url("assets/img/news/img01.jpg").'"" style="background-image: url(&quot;../assets/img/news/img01.jpg&quot;);">
                            </div>
                        </div>
                        <div class="article-details">
                            <div class="article-title">
                                <h6 class="text-center text-muted">'.$this->lang->line('No data found for this campaign').'</h6>
                            </div>
                        </div>
                    </article>
                </div>';

            echo $html; exit;
        }

        $media_url = $campaign_data['media_url'];
        $posted_locations = json_decode($campaign_data['response'],true);

        $html = '
            <div class="col-12 p-0">
                <article class="article article-style-c shadow-none mb-0">
                    <div class="article-header">
                        <img src="'.$media_url.'" width="100%">
                    </div>
                    <div class="article-details">
                        <div class="article-category">
                            <a class="text-decoration-none" href="#">'.$this->lang->line("Post Type").'</a> <div class="bullet"></div> <a class="text-decoration-none text-primary" href="#">'.ucwords(str_replace('_',' ',$campaign_data['post_type'])).'</a>
                            <a data-toggle="tooltip" title="'.$this->lang->line("Created at").'" class="float-right text-decoration-none text-muted" href="#"><i class="far fa-clock"></i> '.date("M j, Y",strtotime($campaign_data['created_at'])).'</a>
                        </div>
                        <div class="article-title mb-0">
                            <h2><a data-toggle="" href="#" class="text-decoration-none">'.$campaign_data['summary'].'</a></h2>
                        </div>
                        <div class="article-user mt-2">
                            <div class="article-user-details">
                                <div class="user-detail-name">
                                    <a class="text-decoration-none" href="#">'.$this->lang->line('Posted to Locations').'</a>
                                </div>
                                <div class="text-job text-transform-none">';

            foreach ($posted_locations as $key => $value) {

                if(isset($value['error_message']) && !empty($value['error_message'])) {

                    $html .= '<a data-toggle="tooltip" title="'.$value['error_message'].'" href="#" class="btn btn-sm btn-danger mt-2 mb-2 mr-2 ml-0"><i class="fas fa-exclamation-circle"></i> '.$value['location_name'].'</a>';

                } else if(isset($value['searchUrl']) && !empty($value['searchUrl'])) {

                    $html .= '<a target="_BLANK" data-toggle="tooltip" title="Visit Post"  href="'.$value['searchUrl'].'" class="btn btn-sm btn-primary mt-2 mb-2 mr-2 ml-0"><i class="fas fa-check"></i> '.$value['location_name'].'</a>';
                }

            }
                                    
            $html .='</div>
                            </div>
                        </div>
                    </div>
                </article>
            </div><script>$("[data-toggle=\'tooltip\']").tooltip();</script>
        ';

        echo $html; exit;
    }

    public function upload_media()
    {
        $this->ajax_check();

        // Determines upload path
        $output_dir = APPPATH . '../upload/xerobiz/';

        // Starts uploading file
        if (isset($_FILES['xerobiz_file'])) {

            $error = $_FILES['xerobiz_file']['error'];
            if( $error ) {
                echo json_encode([$error]);
                exit;
            }

            if (is_uploaded_file($_FILES['xerobiz_file']['tmp_name'])) {
                $tmp_name = $_FILES['xerobiz_file']['tmp_name'];
                $post_fileName =$_FILES["xerobiz_file"]["name"];
                $post_fileName_array=explode(".", $post_fileName);
                $extension=array_pop($post_fileName_array);

                $allow=".jpg,.jpeg,.png,.gif";
                $allow=str_replace('.', '', $allow);
                $allow=explode(',', $allow);
                if(!in_array(strtolower($extension), $allow)) 
                {
                    $custom_error['jquery-upload-file-error']="File type not allowed.";
                    echo json_encode($custom_error);
                    exit();
                }

                $post_fileName = $_FILES["xerobiz_file"]["name"];
                $ext = mb_substr($post_fileName, mb_strrpos('.', $post_fileName));
                $filename = 'image_' . $this->user_id . '_' . time() . substr(uniqid(mt_rand(), true), 0, 6) . "." . $ext;

                if (move_uploaded_file($tmp_name, $output_dir .'/'. $filename)) {
                    echo json_encode($filename);
                    exit;
                } else {
                    $custom_error['jquery-upload-file-error']="Something went wrong while uploading file";
                    echo json_encode($custom_error);
                    exit();
                }
            }
        }
        $custom_error['jquery-upload-file-error']="Please upload a valid file";
        echo json_encode($custom_error);
        exit;
    }

    public function delete_media()
    {
        // Checks ajax call
        $this->ajax_check();

        if( ! $_POST) {
            exit();
        }

        $output_dir = APPPATH . '../upload/xerobiz/';
        if(isset($_POST['op']) && $_POST['op'] == 'delete' && isset($_POST['name'])) {
            $fileName = strip_tags($_POST['name']);

            // Requires if somebody tries parent folder files
            $fileName = str_replace("..",".",$fileName);
            $filePath = $output_dir . $fileName;

            if (file_exists($filePath)) {
                unlink($filePath);
                $this->session->unset_userdata('gmb_media_type');
            }
        }
    }

    public function is_location_name_array()
    {
        $locations = isset($_POST['location_name']) ? $_POST['location_name'] : [];
        $array_keys = $this->session->userdata('gmb_location_table_id_array_keys');
        $location_names = explode('|', $array_keys);

        if (! is_array($location_names)
            || ! is_array($locations)
            || empty($locations)
        ) {
            $this->form_validation->set_message('is_location_name_array', $this->lang->line("Location name is required"));
            return false;
        }

        foreach ($locations as $location) {
            if (! in_array($location, $location_names)) {
                $this->form_validation->set_message('is_location_name_array', $this->lang->line("Location name is invalid"));
                return false;
            }
        }

        return true;
    }

    public function create_campaign()
    {
        if($this->is_demo == '1')
        {
            if($this->session->userdata('user_type') == "Admin")
            {
                $response['status'] = false;
                $response['errors'] = "You can not delete anything from admin account!!";
                echo json_encode($response);
                exit();
            }
        }

        $this->form_validation->set_rules('campaign_name', $this->lang->line('Campaign name'), 'required');
        $this->form_validation->set_rules('submitted_post_type', $this->lang->line('Campaign type'), 'required|in_list[cta_post,event_post,offer_post]');

        if ('cta_post' == $this->input->post('submitted_post_type')) {
            $this->form_validation->set_rules('cta_action_type', $this->lang->line('Action type'), 'required');
            if ('call' != strtolower($this->input->post('cta_action_type'))) {
                $this->form_validation->set_rules('cta_action_url', $this->lang->line('Action url'), 'required');
            }
        } elseif ('event_post' == $this->input->post('submitted_post_type')) {
            $this->form_validation->set_rules('event_post_title', $this->lang->line('Post title'), 'required');
            $this->form_validation->set_rules('start_date_time', $this->lang->line('Start date'), 'required');
            $this->form_validation->set_rules('end_date_time', $this->lang->line('End date'), 'required');
        } elseif ('offer_post' == $this->input->post('submitted_post_type')) {
            $this->form_validation->set_rules(
                'offer_coupon_code',
                $this->lang->line('Coupon code'),
                'regex_match[/^[a-zA-Z0-9]+$/]',
                [
                    'regex_match' => $this->lang->line('Coupon code must be alphanumeric characters'),
                ]
            );
            $this->form_validation->set_rules('offer_redeem_url', $this->lang->line('Redeem url'), 'required|valid_url');
        }

        if ('now' != $this->input->post('schedule_type')) {
            $this->form_validation->set_rules('schedule_time', $this->lang->line('Schedule time'), 'required');
            $this->form_validation->set_rules('time_zone', $this->lang->line('Timezone'), 'required');
        }

        $this->form_validation->set_rules('message', $this->lang->line('Summary'), 'required');
        $this->form_validation->set_rules('media_url', $this->lang->line('Media url'), 'required');
        $this->form_validation->set_rules('location_name', $this->lang->line('Location name'), 'callback_is_location_name_array');

        if (false === $this->form_validation->run()) {
            $errors = $this->form_validation->error_array();
            $errors = str_replace(['<b>', '</b>'], '', $errors);
            echo json_encode([
                'status' => false,
                'errors' => $errors
            ]);
            exit;
        }


        $data = [];
        if ('cta_post' == $this->input->post('submitted_post_type')) {
            $data['cta_action_type'] = $this->input->post('cta_action_type', true);
            if ('call' != strtolower($this->input->post('cta_action_type'))) {
                $data['cta_action_url'] = $this->input->post('cta_action_url', true);
            }
        } elseif ('event_post' == $this->input->post('submitted_post_type')) {
            $data['event_post_title'] = $this->input->post('event_post_title', true);
            $data['start_date_time'] = $this->input->post('start_date_time', true);
            $data['end_date_time'] = $this->input->post('end_date_time', true);
        } elseif ('offer_post' == $this->input->post('submitted_post_type')) {
            $data['offer_coupon_code'] = $this->input->post('offer_coupon_code', true);
            $data['offer_redeem_url'] = $this->input->post('offer_redeem_url', true);
        }

        $data['user_id'] = $this->user_id;
        $data['post_type'] = $this->input->post('submitted_post_type', true);
        $data['user_account_id'] = $this->session->userdata('google_mybusiness_user_table_id');

        $location_table_id = [];
        $location_names = [];
        $location_data = (array) $this->input->post('location_name', true);
        foreach ($location_data as $location) {
            list($location_id, $location_name) = explode('-', $location, 2);
            $location_table_id[] = $location_id;
            $location_names[] = [
                'id' => $location_id,
                'name' => $location_name,
                'status' => true,
            ];
        }

        //************************************************//
        $status=$this->_check_usage($module_id=15,$request=count($location_table_id));
        if($status=="2") 
        {
            echo json_encode([
                'status' => false,
                'message' => $this->lang->line("Module limit is over.")
            ]);               
            exit();
        }
        else if($status=="3") 
        {
            echo json_encode([
                'status' => false,
                'message' => $this->lang->line("Module limit is over.")
            ]);               
            exit();
        }
        //************************************************//

        $data['location_table_id'] = json_encode($location_table_id);
        $data['location_names'] = json_encode($location_names);
        $data['campaign_name'] = $this->input->post('campaign_name', true);
        $data['summary'] = $this->input->post('message', true);
        $data['media_type'] = 'PHOTO';
        $data['media_url'] = $this->input->post('media_url', true);
        $data['status'] = '0';
        $data['created_at'] = date('Y-m-d H:i:s');

        if ($this->input->post('schedule_time')
            && ! empty($this->input->post('schedule_time'))
        ) {
            $data['schedule_time'] = $this->input->post('schedule_time', true);
            $data['time_zone'] = $this->input->post('time_zone', true);
        } else {
            $data['schedule_type'] = 'now';
            $tz = '';
            if ($this->config->item('time_zone')) {
                $tz = $this->config->item('time_zone');
            } else {
                if (date_default_timezone_get()) {
                    $tz = date_default_timezone_get();
                }
            }

            $data['schedule_time'] = date('Y-m-d H:i:s');
            $data['time_zone'] = $tz;
        }

        $campaign_post_id = $this->session->userdata('gmb_campaign_post_id');
        if ($this->input->post('submitted_post_id')) {
            if ($campaign_post_id != $this->input->post('submitted_post_id')) {
                $message = $this->lang->line('It was a bad request!');
                echo json_encode([
                    'status' => false,
                    'message' => $message,
                ]);
                exit;
            }

            if ($this->basic->update_data('google_posts_campaign', ['id' => $campaign_post_id], $data)) {
                $this->session->unset_userdata('gmb_campaign_post_id');
                $message = $this->lang->line("Campaign updated successfully.");
                echo json_encode([
                    'status' => true,
                    'message' => $message,
                ]);
                exit;
            } else {
                $message = $this->lang->line("Something went wrong while submitting campaign.");
                echo json_encode([
                    'status' => false,
                    'message' => $message,
                ]);
            }
        }

        if ($this->basic->insert_data('google_posts_campaign', $data)) {
            $this->_insert_usage_log($module_id=15,$request=count($location_table_id)); 
            $message = $this->lang->line("Campaign submitted successfully.");
            echo json_encode([
                'status' => true,
                'message' => $message,
            ]);
            exit;
        } else {
            $message = $this->lang->line("Something went wrong while submitting campaign.");
            echo json_encode([
                'status' => false,
                'message' => $message,
            ]);
        }
    }

    public function edit_post($id)
    {
        if($this->is_demo == '1')
        {
            if($this->session->userdata('user_type') == "Admin")
            {
                echo "<h2 style='text-align:center;color:red;border:1px solid red; padding: 10px'>This feature is disabled in this demo.</h2>";
                exit();
            }
        }

        $postId = (int) $id;

        $where = [
            'where' => [
                'id' => $postId,
                'user_id' => $this->user_id,
                'user_account_id' => $this->session->userdata('google_mybusiness_user_table_id'),
                'deleted' => '0',
                'status' => '0'
            ],
        ];
        $campaign = $this->basic->get_data('google_posts_campaign', $where);

        $where = [
            'where' => [
                'user_id' => $this->user_id,
                'user_account_id' => $this->session->userdata('google_mybusiness_user_table_id'),
            ],
        ];

        $locations = $this->basic->get_data('google_business_locations', $where, ['id', 'location_display_name']);

        if (! count($campaign) || ! count($locations)) {
            return $this->error_404();
            exit;
        }

        $prepared_locations = [];
        foreach ($locations as $location) {
            $location_key = $location['id'] . '-' . $location['location_display_name'];
            $prepared_locations[$location_key] = $location['location_display_name'];
        }

        // Saves location table id in session
        $array_keys = implode('|', array_keys($prepared_locations));
        $this->session->set_userdata('gmb_location_table_id_array_keys', $array_keys);

        $actionTypes = [
            '' => $this->lang->line('Please Select'),
            'BOOK' => 'BOOK',
            'ORDER' => 'ORDER',
            'SHOP' => 'SHOP',
            'LEARN_MORE' => 'LEARN_MORE',
            'SIGN_UP' => 'SIGN_UP',
            'GET_OFFER' => 'GET_OFFER',
            'CALL' => 'CALL'
        ];

        // Prepares selected locations
        $location_table_id = json_decode($campaign[0]['location_table_id'], true);
        $location_names = json_decode($campaign[0]['location_names'], true);

        $selected_locations = [];
        foreach($location_table_id as $location_id) {
            $first_element = array_shift($location_names);
            $location_name = $first_element['name'];
            $location_key = $location_id . '-' . $location_name;
            $selected_locations[] = $location_key;
        }

        $this->session->set_userdata('gmb_campaign_post_id', $campaign[0]['id']);
        $data['campaign'] = isset($campaign[0]) ? $campaign[0] : [];
        $data['time_zone'] = $this->_time_zone_list();
        $data['locations'] = $prepared_locations;
        $data['selected_locations'] = $selected_locations;
        $data['actionTypes'] = $actionTypes;
        $data['page_title'] = $this->lang->line('Edit campaign');
        $data['title'] = $this->lang->line('Edit campaign');
        $data['body'] = 'mybusiness/edit_post';
        $this->_viewcontroller($data);
    }

    public function delete_post()
    {
        $this->ajax_check();

        if($this->is_demo == '1')
        {
            if($this->session->userdata('user_type') == "Admin")
            {
                $response['status'] = false;
                $response['errors'] = "You can not delete anything from admin account!!";
                echo json_encode($response);
                exit();
            }
        }

        $this->form_validation->set_rules('id', $this->lang->line('Post ID'), 'required');

        if (false === $this->form_validation->run()) {
            $message = $this->lang->line("Something went wrong while deleting post!");
            if ($this->form_validation->error('id')) {
                $message = $this->lang->line('It was a BAD request!');
            }

            echo json_encode([
                'status' => false,
                'message' => $message
            ]);
            exit;
        }

        $where = [
            'id' => $this->input->post('id'),
            'user_id' => $this->user_id
        ];

        $post_info = $this->basic->get_data('google_posts_campaign',['where'=>$where],['location_table_id','status']);
        $locations_array = isset($post_info[0]['location_table_id']) ? json_decode($post_info[0]['location_table_id'],true) : 0;
        $number_of_locations = count($locations_array);
        $posting_status = isset($post_info[0]['status']) ? $post_info[0]['status'] : 2;
        if($posting_status != 2)
            $this->_delete_usage_log($module_id=15,$request=$number_of_locations);

        if ($this->basic->delete_data('google_posts_campaign', $where)) {
            echo json_encode([
               'status' => true
            ]);
            exit;
        } else {
            echo json_encode([
                'status' => false
            ]);
            exit;
        }
    }

    public function post_insights()
    {
        // accounts/107745512734031207626/locations/14692206365244175995/localPosts/1247337822086088354
        // Check if is an ajax request
        $this->ajax_check();

        $post_name = $this->input->post('post_name', true);

        if (! preg_match('@^accounts/[0-9]+/locations/[0-9]+/localPosts/[0-9]+$@', $post_name)) {
            echo json_encode([
                'status' => false,
                'message' => $this->lang->line('Bad request')
            ]);
            exit;
        }

        // Inits google_my_business
        $params['gmb_user_table_id'] = $this->session->userdata('google_mybusiness_user_table_id');
        $params['redirectUri'] = '';
        $this->load->library('google_my_business', $params);
        $gmb = $this->google_my_business;

        // Initial vars
        $error = '';
        $response = '';
        $start_date_time = '';
        $end_date_time = '';

        // if ($_POST) {
        //     $start_date_time_value = $this->input->post('from_date', true);
        //     $end_date_time_value = $this->input->post('to_date', true);
        //     $start_date_time = new DateTime($start_date_time_value);
        //     $end_date_time = new DateTime($end_date_time_value);
        //
        //     if ($end_date_time <= $start_date_time) {
        //         $start_date_time = new DateTime();
        //         $start_date_time->modify('-2 months');
        //         $end_date_time = new DateTime();
        //     }
        //
        //     if ($end_date_time > $start_date_time) {
        //         $too_much_interval = $start_date_time->diff($end_date_time);
        //         $days = $too_much_interval->format('%R%a');
        //         $date_time_difference = (int) $days;
        //
        //         if ($date_time_difference > 186) {
        //             $start_date_time = new DateTime($start_date_time_value);
        //             $end_date_time = new DateTime($start_date_time_value);
        //             $interval = new DateInterval('P6M');
        //             $end_date_time->add($interval);
        //         }
        //     }
        // } else {
        //     $start_date_time = new DateTime();
        //     $start_date_time->modify('-5 days');
        //     $end_date_time = new DateTime();
        // }

        // Prepares date
        $start_date_time = new DateTime();
        $start_date_time->modify('-2 months');
        $end_date_time = new DateTime();

        if (is_object($gmb)) {
            $location_name = explode('/localPosts/', $post_name, 2);

            $post_names = [
                $post_name
            ];

            try {
                $response = $gmb->postsInsightsBasicMetric(
                    $location_name[0],
                    $post_names,
                    'ALL',
                    [
                        'AGGREGATED_DAILY',
                    ],
                    $start_date_time,
                    $end_date_time
                );
            } catch (Google_Service_Exception $e) {
                $error = $e->getMessage();
            } catch (Google_Exception $e) {
                $error = $e->getMessage();
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }

            // @TODO
            // Log or send email the error message
        }

        if (! empty($error)) {
            echo json_encode([
                'status' => false,
                'message' => $error,
            ]);
            exit;
        }

        $metrics = [
            'QUERIES_DIRECT',
            'QUERIES_INDIRECT',
            'QUERIES_CHAIN',
            'VIEWS_MAPS',
            'VIEWS_SEARCH',
            'ACTIONS_WEBSITE',
            'ACTIONS_PHONE',
            'ACTIONS_DRIVING_DIRECTIONS',
            'PHOTOS_VIEWS_MERCHANT',
            'PHOTOS_VIEWS_CUSTOMERS',
            'PHOTOS_COUNT_MERCHANT',
            'PHOTOS_COUNT_CUSTOMERS',
            'LOCAL_POST_VIEWS_SEARCH',
            'LOCAL_POST_ACTIONS_CALL_TO_ACTION',
        ];

        // Prepares array based on metrics
        $post_insights = [];
        if (is_object($response)) {
            foreach ($response as $metric) {
                if (is_array($metric->getMetricValues())) {
                    foreach ($metric->getMetricValues() as $metricValue) {

                        $metricType = $metricValue->getMetric();

                        if (! empty($metricType) && 'LOCAL_POST_VIEWS_SEARCH' == $metricType ) {
                            foreach ($metricValue->getDimensionalValues() as $dimensionalValue) {
                                $startDateTime = '';
                                $value = $dimensionalValue->getValue();
                                if (is_object($dimensionalValue->getTimeDimension())) {
                                    $timeRange = $dimensionalValue
                                        ->getTimeDimension()
                                        ->getTimeRange();
                                    if (is_object($timeRange)) {
                                        $startDateTime = $timeRange->getStartTime();
                                    }
                                }

                                $stringDateTime = date('M j, Y', strtotime($startDateTime));

                                $post_insights[$metricType]['date'][] = $stringDateTime;
                                $post_insights[$metricType]['value'][] = $value ? $value : 0;
                            }
                        }
                    }
                }
            }
        }

        echo json_encode([
            'status' => true,
            'data' => isset($post_insights['LOCAL_POST_VIEWS_SEARCH']) ? json_encode($post_insights['LOCAL_POST_VIEWS_SEARCH']) : []
        ]);
        exit;

        // $data['post_insights'] = $post_insights;
        // $data['post_name'] = $post_name[1];
        // $data['from_date'] = is_object($start_date_time) ? $start_date_time->format('Y-m-d') : date('Y-m-d');
        // $data['to_date'] = is_object($end_date_time) ? $end_date_time->format('Y-m-d') : date('Y-m-d');
        // $data['title'] = $this->lang->line('Post Insights');
        // $data['page_title'] = $this->lang->line('Post Insights');
        // $data['body'] = 'mybusiness/post_insights';
        // $this->_viewcontroller($data);
    }

    public function location_insights_driving_direction($location_id = null)
    {
        if (null == $location_id) {
            return $this->error_404();
        }

        $where = [
            'where' => [
                'id' => $location_id,
                'deleted' => '0'
            ],
        ];

        $select = ['id', 'location_id'];

        $location = $this->basic->get_data('google_business_locations', $where, $select, [], 1);

        if (1 != count($location)) {
            return $this->error_404();
        }

        $location_name = isset($location[0]['location_id'])
            ? $location[0]['location_id']
            : null;
        $location_table_id = isset($location[0]['id'])
            ? $location[0]['id']
            : null;

        // Inits google_my_business
        $params['gmb_user_table_id'] = $this->session->userdata('google_mybusiness_user_table_id');
        $params['redirectUri'] = '';
        $this->load->library('google_my_business', $params);
        $gmb = $this->google_my_business;

        // Initial vars
        $error = '';
        $response = '';
        $number_of_days = '';

        if ($_POST) {
            $number_of_days = $this->input->post('number_of_days');
        } else {
            $number_of_days = 'NINETY';
        }

        if (is_object($gmb)) {
            // accounts/107745512734031207626/locations/14692206365244175995
            $account_name = explode('/locations/', $location_name, 2);

            try {
                $response = $gmb->locationInsightsDrivingDirectionsMetric(
                    $account_name[0],
                    [
                        $location_name,
                    ],
                    $number_of_days
                );
            } catch (Google_Service_Exception $e) {
                $error = $e->getMessage();
            } catch (Google_Exception $e) {
                $error = $e->getMessage();
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }

            // @TODO
            // Log or send email the error message
        }

        if (! empty($error)) {
            return $this->error_404();
        }

        $metrics = [
            'QUERIES_DIRECT',
            'QUERIES_INDIRECT',
            'QUERIES_CHAIN',
            'VIEWS_MAPS',
            'VIEWS_SEARCH',
            'ACTIONS_WEBSITE',
            'ACTIONS_PHONE',
            'ACTIONS_DRIVING_DIRECTIONS',
            'PHOTOS_VIEWS_MERCHANT',
            'PHOTOS_VIEWS_CUSTOMERS',
            'PHOTOS_COUNT_MERCHANT',
            'PHOTOS_COUNT_CUSTOMERS',
            'LOCAL_POST_VIEWS_SEARCH',
            'LOCAL_POST_ACTIONS_CALL_TO_ACTION',
        ];

        echo '<pre>';
        print_r($response);
        exit;

        // Prepares array based on metrics
        $location_insights = [];
        if (is_object($response)) {
            foreach ($response as $metric) {
                if (is_array($metric->getMetricValues())) {

                    $i = 0;
                    // Loops 14 times
                    foreach ($metric->getMetricValues() as $metricValue) {
                        // Loops 30 times by default based on date range
                        foreach ($metricValue->getDimensionalValues() as $dimensionalValue) {
                            $startDateTime = '';
                            $value = $dimensionalValue->getValue();
                            if (is_object($dimensionalValue->getTimeDimension())) {
                                $timeRange = $dimensionalValue
                                    ->getTimeDimension()
                                    ->getTimeRange();
                                if (is_object($timeRange)) {
                                    $startDateTime = $timeRange->getStartTime();
                                }
                            }

                            $stringDateTime = date('M j, Y', strtotime($startDateTime));
                            $metricType = $metricValue->getMetric();

                            if (in_array($metricType, $metrics)) {
                                $location_insights[$metricType]['date'][] = $stringDateTime;
                                $location_insights[$metricType]['value'][] = $value ? $value : 0;
                            } else {
                                $location_insights[$metrics[$i]]['date'][] = $stringDateTime;
                                $location_insights[$metrics[$i]]['value'][] = mt_rand(100, 1000);
                            }
                        }

                        $i++;
                    }
                }
            }
        }

        $data['number_of_days'] = [
            'NINETY' => 'NINETY',
            'THIRTY' => 'THIRTY',
            'SEVEN' => 'SEVEN',
        ];

        $data['post_insights'] = $location_insights;
        $data['location_name'] = $location_name;
        $data['location_table_id'] = $location_table_id;
        $data['from_date'] = is_object($start_date_time) ? $start_date_time->format('Y-m-d') : date('Y-m-d');
        $data['to_date'] = is_object($end_date_time) ? $end_date_time->format('Y-m-d') : date('Y-m-d');
        $data['title'] = $this->lang->line('Location Insights');
        $data['page_title'] = $this->lang->line('Location Insights');
        $data['body'] = 'mybusiness/location_insights_driving_direction';
        $this->_viewcontroller($data);
    }

    

    public function rss_posts()
    {
        echo 'rss_posts';
    }
}


