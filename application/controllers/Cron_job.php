<?php
require_once("Home.php");

class Cron_job extends Home
{
    public function __construct()
    {
        parent::__construct();      
    }


    public function api_member_validity($user_id='')
    {
        if($user_id!='') {
            $where['where'] = array('id'=>$user_id);
            $user_expire_date = $this->basic->get_data('users',$where,$select=array('expired_date'));
            $expire_date = strtotime($user_expire_date[0]['expired_date']);
            $current_date = strtotime(date("Y-m-d"));
            $package_data=$this->basic->get_data("users",$where=array("where"=>array("users.id"=>$user_id)),$select="package.price as price, users.user_type",$join=array('package'=>"users.package_id=package.id,left"));

            if(is_array($package_data) && array_key_exists(0, $package_data) && $package_data[0]['user_type'] == 'Admin' )
                return true;

            $price = '';
            if(is_array($package_data) && array_key_exists(0, $package_data))
            $price=$package_data[0]["price"];
            if($price=="Trial") $price=1;

            
            if ($expire_date < $current_date && ($price>0 && $price!=""))
            return false;
            else return true;           

        }
    }


    public function index()
    {
       $this->get_api();
    }

    public function _api_key_generator()
    {
        if ($this->session->userdata('logged_in') != 1)
        redirect('home/login_page', 'location');

        if($this->session->userdata('user_type') != 'Admin')
        redirect('home/login_page', 'location');

        $this->member_validity();
        $val=$this->session->userdata("user_id")."-".substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 7 ).time()
        .substr(str_shuffle('abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789') , 0 , 7 );
        return $val;
    }

    public function get_api()
    {
        if ($this->session->userdata('logged_in') != 1)
        redirect('home/login_page', 'location');

        if($this->session->userdata('user_type') != 'Admin')
        redirect('home/login_page', 'location');

        $this->member_validity();

        $data['body'] = "admin/cron_job/command";
        $data['page_title'] = $this->lang->line("Cron Job");
        $api_data=$this->basic->get_data("native_api",array("where"=>array("user_id"=>$this->session->userdata("user_id"))));
        $data["api_key"]="";
        if(count($api_data)>0) $data["api_key"]=$api_data[0]["api_key"];
        $this->_viewcontroller($data);
    }

    public function get_api_action()
    { 
        if($this->is_demo == '1')
        {
            echo "<h2 style='text-align:center;color:red;border:1px solid red; padding: 10px'>This feature is disabled in this demo.</h2>"; 
            exit();
        }
        
        if ($this->session->userdata('logged_in') != 1)
        redirect('home/login', 'location');

        if($this->session->userdata('user_type') != 'Admin')
        redirect('home/login_page', 'location');

        $api_key=$this->_api_key_generator(); 
        if($this->basic->is_exist("native_api",array("api_key"=>$api_key)))
        $this->get_api_action();

        $user_id=$this->session->userdata("user_id");        
        if($this->basic->is_exist("native_api",array("user_id"=>$user_id)))
        $this->basic->update_data("native_api",array("user_id"=>$user_id),array("api_key"=>$api_key));
        else $this->basic->insert_data("native_api",array("api_key"=>$api_key,"user_id"=>$user_id));
            
        redirect('cron_job/get_api', 'location');
    }

    public function api_key_check($api_key="")
    {
        $user_id="";
        if($api_key!="")
        {
            $explde_api_key=explode('-',$api_key);
            $user_id="";
            if(array_key_exists(0, $explde_api_key))
            $user_id=$explde_api_key[0];
        }

        if($api_key=="")
        {        
            echo "API Key is required.";    
            exit();
        }

        if(!$this->basic->is_exist("native_api",array("api_key"=>$api_key,"user_id"=>$user_id)))
        {
           echo "API Key does not match with any user.";
           exit();
        }

        if(!$this->basic->is_exist("users",array("id"=>$user_id,"status"=>"1","deleted"=>"0","user_type"=>"Admin")))
        {
            echo "API Key does not match with any authentic user.";
            exit();
        }              
       

    }      

    public function post_to_gmb($api_key)
    {
        $this->api_key_check($api_key);

        $where = [
            'where' => [
                'google_posts_campaign.status' => '0',
                'google_posts_campaign.deleted' => '0',
            ],
        ];

        $campaigns = $this->basic->get_data('google_posts_campaign', $where, [], [], 15, '', 'id desc');

        if (! count($campaigns)) {
            return;
        }

        $ids = [];
        foreach ($campaigns as $key => $campaign) {
            // Prepares post IDs
            $ids[] = $campaign['id'];

            if ($campaign['schedule_time']) {
                if ($campaign['time_zone']) {
                    $time_zone = $campaign['time_zone'];
                } else {
                    if (date_default_timezone_get()) {
                        $time_zone = date_default_timezone_get();
                    }
                }

                // Timezone
                $set_time_zone = new DateTimeZone($time_zone);

                // Scheduled time
                $scheduled_time = new DateTime($campaign['schedule_time'], $set_time_zone);

                // Current date and time
                $now = new DateTime('now', $set_time_zone);
                $now->add(new DateInterval('PT10M'));

                // If scheduled time is not less than current
                // time + 10 minutes then just skip this post
                if (! ($scheduled_time <= $now)) {
                    unset($campaigns[$key]);

                    // Unsets scheduled id that is not being sent
                    array_splice($ids, array_search($campaign['id'], $ids), 1);
                }
            }
        }

        // Update state of posts
        $this->db->set('status', '1');
        $this->db->where_in('id', $ids);
        $this->db->update('google_posts_campaign');

        foreach ($campaigns as $post) {
            $user_id = $post['user_id'];
            if(!$this->api_member_validity($user_id)) continue;
            $params['gmb_user_table_id'] = $post['user_account_id'];
            $params['redirectUri'] = '';

            $this->load->library('google_my_business',$params);
            $gmb = $this->google_my_business;

            $location_table_ids = (array) json_decode($post['location_table_id'], true);
            $location_names = (array) json_decode($post['location_names'], true);

            foreach ($location_table_ids as $id) {
                $where = [
                    'where' => [
                        'id' => $id,
                        'user_account_id' => $post['user_account_id'],
                    ]
                ];

                $location = $this->basic->get_data(
                    'google_business_locations',
                    $where,
                    [
                        'location_id',
                        'location_display_name'
                    ],
                    [],
                    1
                );

                if (1 != count($location)) {
                    continue;
                }

                $location_id = isset($location[0]['location_id'])
                    ? trim($location[0]['location_id'])
                    : null;

                if (null == $location_id) {
                    continue;
                }

                $location_name = isset($location[0]['location_display_name'])
                    ? trim($location[0]['location_display_name'])
                    : null;

                if ('cta_post' == $post['post_type']) {
                    static $cta_results = [];
                    static $cta_errors = [];

                    $media = [
                        [
                            'mediaFormat' => $post['media_type'],
                            'sourceUrl' => $post['media_url'],
                        ],
                    ];

                    $response = '';
                    try {
                        $action_url = ('call' == strtolower($post['cta_action_type']))
                            ? null
                            : $post['cta_action_url'];
                        $post_summary = spintax_process($post['summary']);
                        $response = $gmb->callToActionPost(
                            $location_id,
                            $post['cta_action_type'],
                            $post_summary,
                            $media,
                            $action_url
                        );
                    } catch (Google_Service_Exception $e) {
                        $cta_error = $e->getMessage();
                    } catch (Google_Exception $e) {
                        $cta_error = $e->getMessage();
                    } catch (\Exception $e) {
                        $cta_error = $e->getMessage();
                    }

                    if ($response) {
                        $response->location_name = $location_name;
                        $cta_results[] = $response;
                    } elseif ($cta_error) {
                        $result['location_name'] = $location_name;
                        $result['error_message'] = $cta_error;
                        $cta_results[] = $result;
                        $cta_errors[] = $cta_error;

                        if ($location_name && is_array($location_names)) {
                            $key = array_search($location_name, array_column($location_names, 'name'));
                            if ($key) {
                                if (isset($location_names[$key]['status'])) {
                                    $location_names[$key]['status'] = false;
                                }
                            }
                        }
                    }

                    $this->basic->update_data(
                        'google_posts_campaign',
                        ['id' => $post['id']],
                        [
                            'status' => '2',
                            'location_names' => json_encode($location_names),
                            'response' => count($cta_results) ? json_encode($cta_results) : null,
                            'error' => count($cta_errors) ? json_encode($cta_errors) : null,
                        ]
                    );
                } elseif ('event_post' == $post['post_type']) {
                    static $event_results = [];
                    static $event_errors = [];

                    $start_date_time = new DateTime($post['start_date_time']);
                    $end_date_time = new DateTime($post['end_date_time']);
                    $media = [
                        [
                            'mediaFormat' => $post['media_type'],
                            'sourceUrl' => $post['media_url']
                        ]
                    ];
                    $post_summary = spintax_process($post['summary']);

                    $response = '';
                    try {
                        $response = $gmb->eventPost(
                            $location_id,
                            $post['event_post_title'],
                            $post_summary,
                            $start_date_time,
                            $end_date_time,
                            $media
                        );
                    } catch (Google_Service_Exception $e) {
                        $event_error = $e->getMessage();
                    } catch (Google_Exception $e) {
                        $event_error = $e->getMessage();
                    } catch (\Exception $e) {
                        $event_error = $e->getMessage();
                    }

                    if ($response) {
                        $response->location_name = $location_name;
                        $event_results[] = $response;

                    } elseif ($event_error) {
                        $result['location_name'] = $location_name;
                        $result['error_message'] = $event_error;
                        $event_results[] = $result;
                        $event_errors[] = $event_error;

                        if ($location_name && is_array($location_names)) {
                            $key = array_search($location_name, array_column($location_names, 'name'));
                            if ($key) {
                                if (isset($location_names[$key]['status'])) {
                                    $location_names[$key]['status'] = false;
                                }
                            }
                        }
                    }

                    $this->basic->update_data(
                        'google_posts_campaign',
                        ['id' => $post['id']],
                        [
                            'status' => '2',
                            'location_names' => json_encode($location_names),
                            'response' => count($event_results) ? json_encode($event_results) : null,
                            'error' => count($event_errors) ? json_encode($event_errors) : null,
                        ]
                    );
                } elseif ('offer_post' == $post['post_type']) {
                    static $offer_results = [];
                    static $offer_errors = [];

                    $media = [
                        [
                            'mediaFormat' => $post['media_type'],
                            'sourceUrl' => $post['media_url']
                        ]
                    ];
                    $post_summary = spintax_process($post['summary']);

                    $response = '';
                    try {
                        $response = $gmb->offerPost(
                            $location_id,
                            $post['offer_coupon_code'],
                            $post['offer_redeem_url'],
                            $post_summary,
                            $media
                        );

                    } catch (Google_Service_Exception $e) {
                        $offer_error = $e->getMessage();
                    } catch (Google_Exception $e) {
                        $offer_error = $e->getMessage();
                    } catch (\Exception $e) {
                        $offer_error = $e->getMessage();
                    }

                    if ($response) {
                        $response->location_name = $location_name;
                        $offer_results[] = $response;
                    } elseif ($offer_error) {
                        $result['location_name'] = $location_name;
                        $result['error_message'] = $offer_error;
                        $offer_results[] = $result;
                        $offer_errors[] = $offer_error;

                        if ($location_name && is_array($location_names)) {
                            $key = array_search($location_name, array_column($location_names, 'name'));
                            if ($key) {
                                if (isset($location_names[$key]['status'])) {
                                    $location_names[$key]['status'] = false;
                                }
                            }
                        }
                    }

                    $this->basic->update_data(
                        'google_posts_campaign',
                        ['id' => $post['id']],
                        [
                            'status' => '2',
                            'location_names' => json_encode($location_names),
                            'response' => count($offer_results) ? json_encode($offer_results) : null,
                            'error' => count($offer_errors) ? json_encode($offer_errors) : null,
                        ]
                    );
                }
            }

            // Unsets all arrays
            $cta_results = [];
            $cta_errors = [];
            $event_results = [];
            $event_errors = [];
            $offer_results = [];
            $offer_errors = [];
        }
    }

    public function reply_to_review($api_key)
    {
        $this->api_key_check($api_key);

        $where = [
            'where' => [
                'google_review_reply_settings.status' => '0',
                'google_business_locations.deleted' => '0'
            ],
        ];

        $select = [
            'google_review_reply_settings.*',
            'google_review_reply_settings.location_id as location_table_id',
            'google_business_locations.user_account_id',
            'google_business_locations.location_id as location_name',
            'google_business_locations.last_review_reply_id',
            'google_business_locations.location_display_name',
        ];

        $join = [
            'google_business_locations' => 'google_review_reply_settings.location_id=google_business_locations.id,left',
            'google_user_account' => 'google_business_locations.user_account_id=google_user_account.id,left',
        ];

        $locations = $this->basic->get_data('google_review_reply_settings', $where, $select, $join, 10, '', 'google_review_reply_settings.last_reply_time asc');

        if (! count($locations) > 0) {
            return;
        }

        $settings_ids = [];
        foreach ($locations as $location) {
            $settings_ids[] = $location['id'];
        }

        $this->db->where_in('id', $settings_ids);
        $this->db->update('google_review_reply_settings', ['status' => '1']);

        // Loops through locations
        foreach ($locations as $location) {

            $user_id = $location['user_id'];
            if(!$this->api_member_validity($user_id)) continue;
            // Prepares GMB
            $params['gmb_user_table_id'] = $location['user_account_id'];
            $params['redirectUri'] = '';

            $this->load->library('google_my_business',$params);
            $gmb = $this->google_my_business;

            // Tries to get reviews list
            $error = '';
            $response = '';

            try {
                $response = $gmb->reviewsList($location['location_name']);
            } catch (Google_Service_Exception $e) {
                $error = $e->getMessage();
            } catch (Google_Exception $e) {
                $error = $e->getMessage();
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }

            // Gets reviews IDs
            $unreplied_reviews = [];
            if (is_object($response)) {
                if (isset($response['reviews'])) {
                    foreach ($response['reviews'] as $review) {
                        if (is_object($review)) {
                            $unreplied_reviews[] = [
                                'name' => $review->getName(),
                                'star' => $review->getStarRating(),
                                'comment' => $review->getComment(),
                                'reviewer_photo' => is_object($review->getReviewer())
                                    ? $review->getReviewer()->getProfilePhotoUrl()
                                    : null,
                                'reviewer_display_name' => is_object($review->getReviewer())
                                    ? $review->getReviewer()->getDisplayName()
                                    : null,
                                'review_create_time' => $review->getCreateTime(),
                            ];
                        }
                    }
                }
            }

            // Finds index of the matched item
            $key = array_search($location['last_review_reply_id'], array_column($unreplied_reviews, 'name'));

            // Modifies $unreplied_reviews array - removes review ids that are already answered
            array_splice($unreplied_reviews, $key);

            if (count($unreplied_reviews)) {

                // Reverses the order based on key
                krsort($unreplied_reviews);

                // Loops through each review and replies to the review
                foreach ($unreplied_reviews as $review) {
                    // Updates last review ID
                    $this->basic->update_data('google_business_locations',
                        [
                            'id' => $location['location_table_id']
                        ],
                        [
                            'last_review_reply_id' => $review['name'],
                        ]
                    );

                    switch ($review['star']) {
                        case 'FIVE':
                            if ($location['five_star']) {
                                $data = (array) json_decode($location['five_star']);
                                $comment = $this->getComment($data, $review['comment']);
                                $this->replyReview($gmb, $location, $review, $comment);
                            }
                            break;

                        case 'FOUR':
                            if ($location['four_star']) {
                                $data = (array) json_decode($location['four_star']);
                                $comment = $this->getComment($data, $review['comment']);
                                $this->replyReview($gmb, $location, $review, $comment);
                            }
                            break;

                        case 'THREE':
                            if ($location['three_star']) {
                                $data = (array) json_decode($location['three_star']);
                                $comment = $this->getComment($data, $review['comment']);
                                $this->replyReview($gmb, $location, $review, $comment);
                            }
                            break;

                        case 'TWO':
                            if ($location['two_star']) {
                                $data = (array) json_decode($location['two_star']);
                                $comment = $this->getComment($data, $review['comment']);
                                $this->replyReview($gmb, $location, $review, $comment);
                            }
                            break;

                        case 'ONE':
                            if ($location['one_star']) {
                                $data = (array) json_decode($location['one_star']);
                                $comment = $this->getComment($data, $review['comment']);
                                $this->replyReview($gmb, $location, $review, $comment);
                            }
                            break;
                    }
                }
            }

            $this->basic->update_data('google_review_reply_settings',
                [
                    'id' => $location['id']
                ],
                [
                    'status' => '0',
                    'last_reply_time' => date('Y-m-d H:i:s')
                ]
            );

        }
    }


    /**
     * Returns appropriate comment
     *
     * @param array $data
     * @param string $comment
     * @return string
     */
    private function getComment($data, $comment)
    {
        $reply_type = $data['reply_type'];

        if ('keyword' == $reply_type) {
            if (isset($data['keyword_settings'])
                && is_array($data['keyword_settings'])
            ) {
                foreach($data['keyword_settings'] as $key => $keyword) {
                    if (false !== stripos($comment, $keyword)) {
                        $comment = isset($data['reply_settings'][$key])
                            ? $data['reply_settings'][$key]
                            : $data['not_found_reply_settings'];

                        break;
                    } else {
                        $comment = $data['not_found_reply_settings'];
                    }
                }
            } else {
                $comment = $data['not_found_reply_settings'];
            }
        } elseif ('generic' == $reply_type) {
            $comment = $data['generic_message'];
        }

        return spintax_process($comment);
    }


    /**
     * Replies to a review
     *
     * @param Google_my_business $gmb
     * @param array $location
     * @param array $review
     * @param string $comment
     * @return void
     */
    private function replyReview($gmb, array $location, array $review, string $comment)
    {
        $error = '';

        try {
            $gmb->replyReview($review['name'], $comment);
        } catch (Google_Service_Exception $e) {
            $error = $e->getMessage();
        } catch (Google_Exception $e) {
            $error = $e->getMessage();
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        $review_reply = empty($error) ? $comment : '';
        $error = ! empty($error) ? $error : null;
        $reply_time = date('Y-m-d H:i:s');

        $sql = "INSERT INTO `google_review_reply_report` 
                (user_id, location_id, location_display_name, review_id, reviewer_photo, reviewer_name, review_star, review_comment, review_reply, review_create_time, error, reply_time)
                VALUES ('{$location['user_id']}', '{$location['location_table_id']}', '{$location['location_display_name']}', '{$review['name']}', '{$review['reviewer_photo']}', '{$review['reviewer_display_name']}', '{$review['star']}', '{$review['comment']}', '{$review_reply}', '{$review['review_create_time']}', '{$error}', '{$reply_time}')
                ON DUPLICATE KEY UPDATE review_reply = '{$review_reply}', error = '{$error}', reply_time = '{$reply_time}'";

        $this->basic->execute_complex_query($sql);
        
    }


    // =====================OTHER FUNCTIONS===================
    public function membership_alert_delete_junk_data($api_key="") //membership_alert_delete_junk_data
    {
        $this->api_key_check($api_key);    

        $current_date = date("Y-m-d");
        $tenth_day_before_expire = date("Y-m-d", strtotime("$current_date + 10 days"));
        $one_day_before_expire = date("Y-m-d", strtotime("$current_date + 1 days"));
        $one_day_after_expire = date("Y-m-d", strtotime("$current_date - 1 days"));

        // echo $tenth_day_before_expire."<br/>".$one_day_before_expire."<br/>".$one_day_after_expire;

        //send notification to members before 10 days of expire date
        $where = array();
        $where['where'] = array(
            'user_type !=' => 'Admin',
            'expired_date' => $tenth_day_before_expire
            );
        $info = array();
        $value = array();
        $info = $this->basic->get_data('users',$where,$select='');
        $from = $this->config->item('institute_email');
        $mask = $this->config->item('product_name');

        // getting email template info
        $email_template_info = $this->basic->get_data("email_template_management",array('where'=>array('template_type'=>'membership_expiration_10_days_before')),array('subject','message'));

        if(isset($email_template_info[0]) && $email_template_info[0]['subject'] !='' && $email_template_info[0]['message'] !='') {

            $subject = $email_template_info[0]['subject'];
            foreach ($info as $value) 
            {
                if(!$this->api_member_validity($value['id'])) continue;
                $url = base_url();

                $message = str_replace(array('#USERNAME#','#APP_URL#','#APP_NAME#'),array($value['name'],$url,$mask),$email_template_info[0]['message']);

                $to = $value['email'];
                $this->_mail_sender($from, $to, $subject, $message, $mask, $html=1);
            }


        } else {

            $subject = "Payment Notification";
            foreach ($info as $value) 
            {
                if(!$this->api_member_validity($value['id'])) continue;
                $message = "Dear {$value['name']},<br/> your account will expire after 10 days, Please pay your fees.<br/><br/>Thank you,<br/><a href='".base_url()."'>{$mask}</a> team";
                $to = $value['email'];
                $this->_mail_sender($from, $to, $subject, $message, $mask, $html=1);
            }

        }

        //send notificatio to members before 1 day of expire date
        $where = array();
        $where['where'] = array(
            'user_type !=' => 'Admin',
            'expired_date' => $one_day_before_expire
            );
        $info = array();
        $value = array();
        $info = $this->basic->get_data('users',$where,$select='');
        $from = $this->config->item('institute_email');
        $mask = $this->config->item('product_name');

        // getting email template info
        $email_template_info_01 = $this->basic->get_data("email_template_management",array('where'=>array('template_type'=>'membership_expiration_1_day_before')),array('subject','message'));

        if(isset($email_template_info_01[0]) && $email_template_info_01[0]['subject'] != '' && $email_template_info_01[0]['message'] != '') {

            $subject = $email_template_info_01[0]['subject'];
            foreach ($info as $value) {
                if(!$this->api_member_validity($value['id'])) continue;
                $url = base_url();
                $message = str_replace(array('#USERNAME#','#APP_URL#','#APP_NAME#'),array($value['name'],$url,$mask),$email_template_info_01[0]['message']);

                $to = $value['email'];
                $this->_mail_sender($from, $to, $subject, $message, $mask, $html=1);
            }

        }
        else {

            $subject = "Payment Notification";
            foreach ($info as $value) {
                if(!$this->api_member_validity($value['id'])) continue;
                $message = "Dear {$value['name']},<br/> your account will expire tomorrow, Please pay your fees.<br/><br/>Thank you,<br/><a href='".base_url()."'>{$mask}</a> team";
                $to = $value['email'];
                $this->_mail_sender($from, $to, $subject, $message, $mask, $html=1);
            }

        }
        

        //send notificatio to members after 1 day of expire date
        $where = array();
        $where['where'] = array(
            'user_type !=' => 'Admin',
            'expired_date' => $one_day_after_expire
            );
        $info = array();
        $value = array();
        $info = $this->basic->get_data('users',$where,$select='');
        $from = $this->config->item('institute_email');
        $mask = $this->config->item('product_name');

        $email_template_info_02 = $this->basic->get_data("email_template_management",array('where'=>array('template_type'=>'membership_expiration_1_day_after')),array('subject','message'));

        if(isset($email_template_info_02[0]) && $email_template_info_02[0]['subject'] != '' && $email_template_info_02[0]['message'] != '') {

            $subject = $email_template_info_02[0]['subject'];

            foreach ($info as $value) {
                if(!$this->api_member_validity($value['id'])) continue;
                $url = base_url();
                $message = str_replace(array('#USERNAME#','#APP_URL#','#APP_NAME#'),array($value['name'],$url,$mask),$email_template_info_02[0]['message']);
                $to = $value['email'];
                $this->_mail_sender($from, $to, $subject, $message, $mask, $html=1);
            }

        } else {

            $subject = "Payment Notification";
            foreach ($info as $value) {
                if(!$this->api_member_validity($value['id'])) continue;
                $message = "Dear {$value['name']},<br/> your account has been expired, Please pay your fees for continuity.<br/><br/>Thank you,<br/><a href='".base_url()."'>{$mask}</a> team";
                $to = $value['email'];
                $this->_mail_sender($from, $to, $subject, $message, $mask, $html=1);
            }
        }   

        /****Clean Cache Directory , keep all files of last 24 hours******/
        $all_cache_file=$this->delete_cache('application/cache');
        // $all_cache_file=$this->delete_cache('upload/qrc');    

        //Delete error log file in root
        @unlink("error_log"); 

    }

    protected function delete_cache($myDir) //delete_junk_data
    {

        $cur_time=date('Y-m-d H:i:s');
        $yesterday=date("Y-m-d H:i:s",strtotime($cur_time." -1 day"));
        $yesterday=strtotime($yesterday);


        $dirTree = array();
        $di = new RecursiveDirectoryIterator($myDir,RecursiveDirectoryIterator::SKIP_DOTS);
        
        foreach (new RecursiveIteratorIterator($di) as $filename) {
        
        $dir = str_replace($myDir, '', dirname($filename));
        //$dir = str_replace('/', '>', substr($dir,1));
        
        $org_dir=str_replace("\\", "/", $dir);
        
        
        if($org_dir)
        $file_path = $org_dir. "/". basename($filename);
        else
        $file_path = basename($filename);

        $path_explode = explode(".",$file_path);
        $extension= array_pop($path_explode);

        if($file_path!='.htaccess' && $file_path!='index.html'){

             $full_file_path=$myDir."/".$file_path;

             $file_creation_time=filemtime($full_file_path);
             $file_creation_time=date('Y-m-d H:i:s',$file_creation_time); //convert unix time to system time zone 
             $file_creation_time=strtotime($file_creation_time);


             if($file_creation_time<$yesterday){
                $dirTree[] = trim($file_path,"/");
                unlink($full_file_path);

             }
                
        }

        
        }
        
        return $dirTree;
            
    }
    // =====================OTHER FUNCTIONS===================

}
