<?php

require_once("Home.php"); // including home controller

/**
 * class config
 * @category controller
 */
class Dashboard extends Home
{
    public $user_id;
    /**
     * load constructor method
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in') != 1)
            redirect('home/login_page', 'location');
        $this->user_id = $this->session->userdata('user_id');

        set_time_limit(0);
        $this->important_feature();
        $this->member_validity();
    }

    /**
     * load index method. redirect to config
     * @access public
     * @return void
     */
    public function index($default_value = '0')
    {
        if ($this->session->userdata('user_type') != 'Admin') $default_value = '0';
        if ($default_value == '0') {
            $user_id = $this->user_id;
            $data['other_dashboard'] = '0';
        } else {
            $user_id = $default_value;
            if ($default_value == 'system')
                $data['system_dashboard'] = 'yes';
            else {
                $user_info = $this->basic->get_data('users', array('where' => array('id' => $user_id)));
                $data['user_name'] = isset($user_info[0]['name']) ? $user_info[0]['name'] : '';
                $data['user_email'] = isset($user_info[0]['email']) ? $user_info[0]['email'] : '';
                $data['system_dashboard'] = 'no';
            }

            $data['other_dashboard'] = '1';
        }

        if ($this->is_demo === '1' && $data['other_dashboard'] === '1' && isset($data['system_dashboard']) && $data['system_dashboard'] === 'no') {
            echo "<h2 style='text-align:center;color:red;border:1px solid red; padding: 10px'>This feature is disabled in this demo.</h2>";
            exit();
        }


        $current_year = date("Y");
        $lastyear = $current_year - 1;
        $current_month = date("Y-m");
        $current_date = date("Y-m-d");
        $data['month_number'] = date('m');

        // first item section
        $total_gmb_accounts = 0;
        $total_gmb_unique_locations = 0;
        $total_gmb_posts = 0;
        $total_review_reply = 0;

        $where = [];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = array('count(id) as accounts');
        $gmb_account_info = $this->basic->get_data('google_user_account', $where, $select);
        $total_gmb_accounts = isset($gmb_account_info[0]['accounts']) ? $gmb_account_info[0]['accounts'] : 0;
        $data['total_gmb_accounts'] = $total_gmb_accounts;

        $where = [];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = array('id');
        $location_info = $this->basic->get_data('google_business_locations', $where, $select, '', '', '', '', 'location_display_name');
        $total_gmb_unique_locations = count($location_info);
        $data['total_gmb_unique_locations'] = $total_gmb_unique_locations;

        $where = array(
            'where' => array(
                'status' => '2'
            )
        );
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = array('count(id) as total_gmb_posts');
        $gmb_post_info = $this->basic->get_data("google_posts_campaign", $where, $select);
        $total_gmb_posts = isset($gmb_post_info[0]['total_gmb_posts']) ? $gmb_post_info[0]['total_gmb_posts'] : 0;
        $data['total_gmb_posts'] = $total_gmb_posts;


        $where = [];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = array('count(id) as total_review_reply');
        $review_reply_info = $this->basic->get_data("google_review_reply_report", $where, $select);
        $total_review_reply = isset($review_reply_info[0]['total_review_reply']) ? $review_reply_info[0]['total_review_reply'] : 0;
        $data['total_review_reply'] = $total_review_reply;
        // end of first item section


        // second item section [last 7 days review replies]
        $last_seven_day = date("Y-m-d", strtotime("$current_date - 7 days"));
        $where = array(
            'where' => array(
                // 'user_id' => $user_id,
                'date_format(reply_time,"%Y-%m-%d") >=' => $last_seven_day
            )
        );
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = array('count(id) as reviews', 'date_format(reply_time,"%Y-%m-%d") as reply_date');
        $seven_days_review_info = $this->basic->get_data('google_review_reply_report', $where, $select, '', '', '', 'date_format(reply_time,"%Y-%m-%d") asc', 'date_format(reply_time,"%Y-%m-%d")');
        $seven_days_reviews_chart_label = array();
        $seven_days_reviews_chart_data = array();
        $seven_days_reviews_gain = 0;
        if (!empty($seven_days_review_info)) {
            foreach ($seven_days_review_info as $value) {
                array_push($seven_days_reviews_chart_label, date("jS M y", strtotime($value['reply_date'])));
                array_push($seven_days_reviews_chart_data, $value['reviews']);
                $seven_days_reviews_gain = $seven_days_reviews_gain + $value['reviews'];
            }
        }
        $data['seven_days_reviews_chart_label'] = $seven_days_reviews_chart_label;
        $data['seven_days_reviews_chart_data'] = $seven_days_reviews_chart_data;
        $data['seven_days_reviews_gain'] = $seven_days_reviews_gain;
        // end of second item section


        // third item section [last 7 days posting report]
        $last_seven_day = date("Y-m-d", strtotime("$current_date - 7 days"));
        $where = array(
            'where' => array(
                'status' => '2',
                'date_format(schedule_time,"%Y-%m-%d") >=' => $last_seven_day
            )
        );
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = array('count(id) as posts', 'date_format(schedule_time,"%Y-%m-%d") as posting_time');
        $seven_days_posting_info = $this->basic->get_data('google_posts_campaign', $where, $select, '', '', '', 'date_format(schedule_time,"%Y-%m-%d") asc', 'date_format(schedule_time,"%Y-%m-%d")');
        $seven_days_post_chart_label = array();
        $seven_days_post_chart_data = array();
        $seven_days_post_gain = 0;
        if (!empty($seven_days_posting_info)) {
            foreach ($seven_days_posting_info as $value) {
                array_push($seven_days_post_chart_label, date("jS M y", strtotime($value['posting_time'])));
                array_push($seven_days_post_chart_data, $value['posts']);
                $seven_days_post_gain = $seven_days_post_gain + $value['posts'];
            }
        }
        $data['seven_days_post_chart_label'] = $seven_days_post_chart_label;
        $data['seven_days_post_chart_data'] = $seven_days_post_chart_data;
        $data['seven_days_post_gain'] = $seven_days_post_gain;
        // end of third item section

        // forth item section [post types]
        $cta_posts = array();
        $offer_posts = array();
        $event_posts = array();
        $post_type_date = array();
        $past_thirty_day = date("Y-m-d", strtotime($current_date . " -30 days"));
        $where = array(
            'where' => array(
                // 'user_id' => $user_id,
                'date_format(schedule_time,"%Y-%m-%d") >=' => $past_thirty_day,
                'status' => '2'
            )
        );
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = array('count(id) as post_count', 'post_type', 'date_format(schedule_time,"%Y-%m-%d") as schedule_time');
        $post_info = $this->basic->get_data('google_posts_campaign', $where, $select, '', '', '', '', 'date_format(schedule_time,"%Y-%m-%d"),post_type');

        foreach ($post_info as $value) {
            if ($value['post_type'] == 'cta_post')
                $cta_posts[$value['schedule_time']] = $value['post_count'];
            else if ($value['post_type'] == 'offer_post')
                $offer_posts[$value['schedule_time']] = $value['post_count'];
            else
                $event_posts[$value['schedule_time']] = $value['post_count'];

            if (!isset($cta_posts[$value['schedule_time']])) $cta_posts[$value['schedule_time']] = 0;
            if (!isset($offer_posts[$value['schedule_time']])) $offer_posts[$value['schedule_time']] = 0;
            if (!isset($event_posts[$value['schedule_time']])) $event_posts[$value['schedule_time']] = 0;

            $formated_date = date("jS M", strtotime($value['schedule_time']));
            $post_type_date[$value['schedule_time']] = $formated_date;
        }

        $largest_values = array();
        $max_value = 1;
        if (!empty($cta_posts)) array_push($largest_values, max($cta_posts));
        if (!empty($offer_posts)) array_push($largest_values, max($offer_posts));
        if (!empty($event_posts)) array_push($largest_values, max($event_posts));
        if (!empty($largest_values)) $max_value = max($largest_values);
        if ($max_value > 10) $data['step_size'] = floor($max_value / 10);
        else $data['step_size'] = 1;

        $data['cta_post_data'] = $cta_posts;
        $data['offer_post_data'] = $offer_posts;
        $data['event_post_data'] = $event_posts;
        $data['post_type_date'] = $post_type_date;
        // end of forth item section [post types]

        $where = [];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = array('count(id) as reviews', 'review_star');
        $review_type_info = $this->basic->get_data('google_review_reply_report', $where, $select, '', '', '', '', 'review_star');
        $review_type_data = [];
        foreach ($review_type_info as $value)
            $review_type_data[$value['review_star']] = $value['reviews'];
        $data['review_type_data'] = $review_type_data;

        // latest reviews
        $where = [];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $latest_review_info = $this->basic->get_data('google_review_reply_report', $where, '', '', '8', '', 'id desc');
        $data['latest_review_info'] = $latest_review_info;
        // end of latest reviews

        // location based reviews
        $where = [];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = ['count(id) as total_reviews', 'location_id'];
        $location_based_review_info = $this->basic->get_data('google_review_reply_report', $where, $select, '', '6', '', 'total_reviews desc', 'location_id');

        $where = [];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = ['id', 'location_display_name', 'map_url', 'profile_google_url'];
        $all_locations = $this->basic->get_data('google_business_locations', $where, $select);
        $all_location_info = [];
        foreach ($all_locations as $value) {
            $all_location_info[$value['id']]['location_name'] = $value['location_display_name'];
            $all_location_info[$value['id']]['location_map_url'] = $value['map_url'];
            $all_location_info[$value['id']]['location_profile_url'] = $value['profile_google_url'];
        }

        $location_based_reviews = [];
        $i = 0;
        foreach ($location_based_review_info as $value) {
            if (isset($all_location_info[$value['location_id']])) {
                $location_based_reviews[$i]['location_name'] = $all_location_info[$value['location_id']]['location_name'];
                $location_based_reviews[$i]['location_map_url'] = $all_location_info[$value['location_id']]['location_map_url'];
                $location_based_reviews[$i]['location_profile_url'] = $all_location_info[$value['location_id']]['location_profile_url'];
                $location_based_reviews[$i]['total_reviews'] = $value['total_reviews'];
                $i++;
            }
        }
        $data['location_based_reviews'] = $location_based_reviews;
        // end of location based reviews

        // upcoming post campaign
        $where = ['where' => ['status' => '0']];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = ['post_type', 'campaign_name', 'schedule_time'];
        $upcoming_campaigns_info = $this->basic->get_data('google_posts_campaign', $where, $select, '', '5', '', 'schedule_time desc');
        $data['upcoming_campaigns_info'] = $upcoming_campaigns_info;
        // end of upcoming post campaign

        // completed post campaign
        $where = ['where' => ['status' => '2']];
        if ($default_value != 'system') $where['where']['user_id'] = $user_id;
        $select = ['post_type', 'campaign_name', 'schedule_time'];
        $completed_campaigns_info = $this->basic->get_data('google_posts_campaign', $where, $select, '', '5', '', 'schedule_time asc');
        $data['completed_campaigns_info'] = $completed_campaigns_info;
        // end of completed post campaign


        $data['body'] = 'dashboard/dashboard';
        $data['page_title'] = $this->lang->line('Dashboard');
        $this->_viewcontroller($data);
    }

    public function reseller()
    {   
        $data['reseller_users'] = $this->basic->get_data('users', array('where' => array('reseller_id' => $this->session->userdata('user_id'))),array('id','name','email','mobile','expired_date'));
        $data['body'] = 'dashboard/reseller';
        $data['page_title'] = 'Reseller';
        $this->_viewcontroller($data);
    }

    public function tutorials(){
        $data['body'] = 'dashboard/tutorials';
        $data['page_title'] = 'Tutorial Videos';
        $this->_viewcontroller($data);
    }

    public function createUser()
    {

        if ($_SERVER['REQUEST_METHOD'] === 'GET')
            redirect('home/access_forbidden', 'location');

        if ($_POST) {
            $this->form_validation->set_rules('name', '<b>' . $this->lang->line("Full Name") . '</b>', 'trim');
            $this->form_validation->set_rules('email', '<b>' . $this->lang->line("Email") . '</b>', 'trim|required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('mobile', '<b>' . $this->lang->line("Mobile") . '</b>', 'trim');
            $this->form_validation->set_rules('password', '<b>' . $this->lang->line("Password") . '</b>', 'trim|required');
            $this->form_validation->set_rules('confirm_password', '<b>' . $this->lang->line("Confirm Password") . '</b>', 'trim|required|matches[password]');
            $this->form_validation->set_rules('expired_date', '<b>' . $this->lang->line("Expiry Date") . '</b>', 'trim|required');


            if ($this->form_validation->run() == FALSE) {
                $this->reseller();
            } else {
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $mobile = $this->input->post('mobile');
                $password = md5($this->input->post('password'));
                $confirm_password = $this->input->post('confirm_password');
                $user_type = 'Member';
                $expired_date = $this->input->post('expired_date');
                $reseller_id = $this->session->userdata('user_id');

                $data = array(
                    'name' => ucwords($name),
                    'email' => $email,
                    'mobile' => $mobile,
                    'password' => $password,
                    'user_type' => $user_type,
                    'status' => '1',
                    'add_date' => date("Y-m-d H:i:s"),
                    "package_id" => '1',
                    "expired_date" => $expired_date,
                    'reseller_id' => $reseller_id
                );



                if ($this->basic->insert_data('users', $data)) $this->session->set_flashdata('success_message', 1);
                else $this->session->set_flashdata('error_message', 1);

                redirect('dashboard/reseller', 'location');
            }
        }
    }


    public function get_subscriber_data_div($system_dashboard = 'no')
    {
        $this->ajax_check();
        $period = $this->input->post('period', true);
        $today = date("Y-m-d");
        $last_seven_day = date("Y-m-d", strtotime("$today - 7 days"));
        $this_month = date("Y-m");
        $this_year = date("Y");

        // fifth item section [email,phone,birthdate,locale gain]    

        // email section     
        $where_simple = array();
        if ($system_dashboard == 'no')
            $where_simple['user_id'] = $this->user_id;

        if ($period == 'today')
            $where_simple['date_format(entry_time,"%Y-%m-%d")'] = $today;
        else if ($period == 'week')
            $where_simple['date_format(entry_time,"%Y-%m-%d") >='] = $last_seven_day;
        else if ($period == 'month')
            $where_simple['date_format(entry_time,"%Y-%m")'] = $this_month;
        else if ($period == 'year')
            $where_simple['date_format(entry_time,"%Y")'] = $this_year;

        $where_simple['permission'] = '1';
        $where_simple['email !='] = '';
        $where = array(
            'where' => $where_simple
        );
        $select = array('count(id) as subscribers', 'gender');
        $subscriber_info = $this->basic->get_data('messenger_bot_subscriber', $where, $select, '', '', '', '', 'gender');
        $combined_info['email']['male'] = 0;
        $combined_info['email']['female'] = 0;
        foreach ($subscriber_info as $value) {
            if ($value['gender'] == 'male')
                $combined_info['email']['male'] = number_format($value['subscribers']);
            else
                $combined_info['email']['female'] = number_format($value['subscribers']);
        }
        $combined_info['email']['total_email_gain'] = number_format($combined_info['email']['male'] + $combined_info['email']['female']);
        $percentage_info = $this->get_percentage($combined_info['email']['male'], $combined_info['email']['female']);
        $combined_info['email']['male_percentage'] = $percentage_info[0] . '%';
        $combined_info['email']['female_percentage'] = $percentage_info[1] . '%';
        // end of email section

        // phone section
        $where_simple = array();
        if ($system_dashboard == 'no')
            $where_simple['user_id'] = $this->user_id;

        if ($period == 'today')
            $where_simple['date_format(phone_number_entry_time,"%Y-%m-%d")'] = $today;
        else if ($period == 'week')
            $where_simple['date_format(phone_number_entry_time,"%Y-%m-%d") >='] = $last_seven_day;
        else if ($period == 'month')
            $where_simple['date_format(phone_number_entry_time,"%Y-%m")'] = $this_month;
        else if ($period == 'year')
            $where_simple['date_format(phone_number_entry_time,"%Y")'] = $this_year;

        $where_simple['permission'] = '1';
        $where_simple['phone_number !='] = '';
        $where = array(
            'where' => $where_simple
        );
        $select = array('count(id) as subscribers', 'gender');
        $subscriber_info = $this->basic->get_data('messenger_bot_subscriber', $where, $select, '', '', '', '', 'gender');
        $combined_info['phone']['male'] = 0;
        $combined_info['phone']['female'] = 0;
        foreach ($subscriber_info as $value) {
            if ($value['gender'] == 'male')
                $combined_info['phone']['male'] = number_format($value['subscribers']);
            else
                $combined_info['phone']['female'] = number_format($value['subscribers']);
        }
        $combined_info['phone']['total_phone_gain'] = number_format($combined_info['phone']['male'] + $combined_info['phone']['female']);
        $percentage_info = $this->get_percentage($combined_info['phone']['male'], $combined_info['phone']['female']);
        $combined_info['phone']['male_percentage'] = $percentage_info[0] . '%';
        $combined_info['phone']['female_percentage'] = $percentage_info[1] . '%';
        // end of phone section


        // birthdate section
        $where_simple = array();
        if ($system_dashboard == 'no')
            $where_simple['user_id'] = $this->user_id;
        if ($period == 'today')
            $where_simple['date_format(birthdate_entry_time,"%Y-%m-%d")'] = $today;
        else if ($period == 'week')
            $where_simple['date_format(birthdate_entry_time,"%Y-%m-%d") >='] = $last_seven_day;
        else if ($period == 'month')
            $where_simple['date_format(birthdate_entry_time,"%Y-%m")'] = $this_month;
        else if ($period == 'year')
            $where_simple['date_format(birthdate_entry_time,"%Y")'] = $this_year;
        $where_simple['permission'] = '1';
        $where = array(
            'where' => $where_simple
        );
        $select = array('count(id) as subscribers', 'gender');
        $subscriber_info = $this->basic->get_data('messenger_bot_subscriber', $where, $select, '', '', '', '', 'gender');
        $combined_info['birthdate']['male'] = 0;
        $combined_info['birthdate']['female'] = 0;
        foreach ($subscriber_info as $value) {
            if ($value['gender'] == 'male')
                $combined_info['birthdate']['male'] = number_format($value['subscribers']);
            else
                $combined_info['birthdate']['female'] = number_format($value['subscribers']);
        }
        $combined_info['birthdate']['total_birthdate_gain'] = number_format($combined_info['birthdate']['male'] + $combined_info['birthdate']['female']);
        $percentage_info = $this->get_percentage($combined_info['birthdate']['male'], $combined_info['birthdate']['female']);
        $combined_info['birthdate']['male_percentage'] = $percentage_info[0] . '%';
        $combined_info['birthdate']['female_percentage'] = $percentage_info[1] . '%';
        // end of birthdate section

        // end of fifth item section [email,phone,birthdate,locale gain]

        echo json_encode($combined_info, true);
    }


    public function get_percentage($first_number, $second_number)
    {
        if ($first_number == 0 && $second_number == 0)
            return [(float) 0, (float) 0];

        $total = (int) $first_number + (int) $second_number;

        $first_percent = ($first_number / $total) * 100;
        $second_percent = ($second_number / $total) * 100;

        return [(float) $first_percent, (float) $second_percent];
    }
}
