<?php

require_once("Home.php"); // loading home controller

/**
* @category controller
* class Admin
*/

class Social_apps extends Home
{
    
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('logged_in')!= 1 ) {
            redirect('home/login', 'location');
        }

        if ($this->session->userdata('user_type') == "Member")
        redirect('home/login', 'location');        
              
        $this->important_feature();
        $this->periodic_check();

    }


    public function index()
    {
        $this->settings();
    }

    public function settings()
    {
        $data['page_title'] = $this->lang->line('Social Apps');
        $data['body'] = 'admin/social_apps/settings';
        $data['title'] = $this->lang->line('Social Apps');
        $this->_viewcontroller($data);
    }


    // Google My Business - Google Actions
    public function google_settings()
    {
        $data['page_title'] = $this->lang->line('Google App Settings');
        $data['title'] = $this->lang->line('Google App Settings');
        $data['body'] = 'admin/social_apps/google_settings';

        $this->_viewcontroller($data);
    }

    public function google_settings_data()
    {
        $this->ajax_check();

        $search_value = $_POST['search']['value'];
        $display_columns = array("#",'id','app_name','status','app_key','client_id', 'client_secret');
        $search_columns = array( 'app_name','app_key','client_id', 'client_secret');

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $start = isset($_POST['start']) ? intval($_POST['start']) : 0;
        $limit = isset($_POST['length']) ? intval($_POST['length']) : 10;
        $sort_index = isset($_POST['order'][0]['column']) ? strval($_POST['order'][0]['column']) : 1;
        $sort = isset($display_columns[$sort_index]) ? $display_columns[$sort_index] : 'id';
        $order = isset($_POST['order'][0]['dir']) ? strval($_POST['order'][0]['dir']) : 'desc';
        $order_by = $sort . " " . $order;

        $where = array();
        if ($search_value != '') {
            $or_where = array();
            foreach ($search_columns as $key => $value)
                $or_where[$value . ' LIKE '] = "%$search_value%";
            $where = array('or_where' => $or_where);
        }

        $this->db->where('user_id', $this->user_id);

        $table = "google_app_config";
        $info = $this->basic->get_data($table, $where, $select='', $join='', $limit, $start, $order_by, $group_by='');
        $i=0;
        foreach($info as $value)
        {
            if($this->is_demo == '1')
            {
                $info[$i]['app_key'] = 'XXXXXXXXXXX';
                $info[$i]['client_id'] = 'XXXXXXXXXXX';
                $info[$i]['client_secret'] = 'XXXXXXXXXXX';
            }
            $i++;
        }

        $this->db->where('user_id', $this->user_id);
        $total_rows_array=$this->basic->count_row($table, $where, $count=$table . ".id", $join = '', $group_by = '');
        $total_result=$total_rows_array[0]['total_rows'];

        $data['draw'] = (int)$_POST['draw'] + 1;
        $data['recordsTotal'] = $total_result;
        $data['recordsFiltered'] = $total_result;
        $data['data'] = convertDataTableResult($info, $display_columns ,$start);

        echo json_encode($data);
    }

    public function google_settings_action()
    {
        if($this->is_demo == '1')
        {
            echo json_encode(array('type' => 'error', 'message' => $this->lang->line("This feature is disabled in this demo.")));
            exit();
        }
        $this->ajax_check();

        $submit_type = $this->input->post('submit_type', true);
        $app_name = $this->input->post('app_name', true);
        $app_key = $this->input->post('app_key', true);
        $client_id = $this->input->post('google_client_id', true);
        $client_secret = $this->input->post('google_client_secret', true);
        $status = $this->input->post('status', true);

        $response = array();

        if ($app_name == '') {
            $response['type'] = 'error';

            if ($app_name == '')
                $response['message'] = $this->lang->line('App Name can not be empty');

            echo json_encode($response);
            exit;
        }

        $data['app_name'] = $app_name;
        $data['user_id'] = $this->user_id;

        // if ($this->session->userdata("user_type") == "Admin") {
        //     $data['is_admin_app'] = '1';
        // }

        if ($submit_type == 'add') {
            if ($app_key != '') $data['app_key'] = $app_key;
            if ($client_id != '') $data['client_id'] = $client_id;
            if ($client_secret != '') $data['client_secret'] = $client_secret;
        } else {
            $data['app_key'] = $app_key;
            $data['client_id'] = $client_id;
            $data['client_secret'] = $client_secret;
        }

        if ($status == '') $data['status'] = '0';
        else $data['status'] = '1';

        if ($submit_type == 'add') {
            $this->basic->insert_data('google_app_config', $data);
            if ($this->db->affected_rows() > 0) {
                // $inserted_id = $this->db->insert_id();
                // $this->basic->update_data('users', array('social_app_google_config_id' => '0'), array("social_app_google_config_id" => $inserted_id));
                $response['type'] = 'success';
                $response['message'] = $this->lang->line('App added successfully.');
            } else {
                $response['type'] = 'error';
                $response['message'] = $this->lang->line('Something went wrong.');
            }

            echo json_encode($response);
        } elseif ($submit_type == 'edit') {
            $table_id = $this->input->post('table_id', true);
            $this->basic->update_data('google_app_config', array('id' => $table_id), $data);
            echo json_encode(array('type' => 'success', 'message' => $this->lang->line('App updated successfully.')));
        }
    }

    public function change_google_app_state()
    {
        if($this->is_demo == '1')
        {
            echo json_encode(array('type' => '0', 'message' => $this->lang->line("This feature is disabled in this demo.")));
            exit();
        }
        $this->ajax_check();

        $table_id = $this->input->post('table_id', true);
        $current_state = $this->basic->get_data('google_app_config', array('where' => array('id' => $table_id)), array('status'));
        $current_state = $current_state[0]['status'];

        if ($current_state == '0') $next_state = '1';
        if ($current_state == '1') $next_state = '0';

        $this->basic->update_data('google_app_config', array('id' => $table_id), array('status' => $next_state));

        echo json_encode(array('type' => '1', 'message' => $this->lang->line("App status changed successfully")));
    }

    public function delete_google_app()
    {
        if($this->is_demo == '1')
        {
            echo json_encode(array('type' => '0', 'message' => $this->lang->line("This feature is disabled in this demo.")));
            exit();
        }
        $this->ajax_check();
        $table_id = $this->input->post('table_id', true);

        $this->db->trans_start();

        $this->basic->delete_data('google_app_config', array('id' => $table_id));
        $google_user_account_info = $this->basic->get_data('google_user_account',['where'=>['app_config_id'=>$table_id]]);
        foreach($google_user_account_info as $user_info)
        {
            $user_id = $user_info['user_id'];
            $gmb_user_table_id = $user_info['id'];
            
            $this->basic->delete_data('google_user_account',['user_id'=>$user_id,'id'=>$gmb_user_table_id]);
            $locations = $this->basic->get_data('google_business_locations',['where'=>['user_account_id'=>$gmb_user_table_id]],'id');
            foreach($locations as $value)
            {
                $this->basic->delete_data('google_business_locations',['id'=>$value['id']]);
                $this->basic->delete_data('google_posts_campaign',['user_id'=>$user_id,'location_table_id'=>$value['id']]);
                $this->basic->delete_data('google_review_reply_report',['user_id'=>$user_id,'location_id'=>$value['id']]);
                $this->basic->delete_data('google_review_reply_settings',['user_id'=>$user_id,'location_id'=>$value['id']]);
            }
            $this->_delete_usage_log($module_id=1,$request=1,$user_id);
        }


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) 
        {    
            $response['type'] = '0';
            $response['message'] = $this->lang->line('Something went wrong, please try again.');         
        }
        else
        {
            $response['type'] = '1';
            $response['message'] = $this->lang->line("APP and all of it's corresponding accounts, locations and campaigns have been deleted successfully.");      
        }

        echo json_encode($response);
    }

    // Google My Business - Facebook Actions
    protected function facebookTokenValidityCheck($input_token)
    {

        if($input_token=="") 
        return "<span class='badge badge-status text-danger'><i class='fas fa-times-circle red'></i> ".$this->lang->line('Invalid')."</span>";
        $this->load->library("fb_rx_login"); 
        
        if($this->config->item('developer_access') == '1')
        {
            $valid_or_invalid = $this->fb_rx_login->access_token_validity_check_for_user($input_token);
            
            if($valid_or_invalid)
                return "<span class='badge badge-status text-success'><i class='fa fa-check-circle green'></i> ".$this->lang->line('Valid')."</span>";
            else
                return "<span class='badge badge-status text-danger'><i class='fa fa-clock-o red'></i> ".$this->lang->line('Expired')."</span>";
        }
        else
        {
            $url="https://graph.facebook.com/debug_token?input_token={$input_token}&access_token={$input_token}";
            $result= $this->fb_rx_login->run_curl_for_fb($url);
            $result = json_decode($result,true);
             
            if(isset($result["data"]["is_valid"]) && $result["data"]["is_valid"])
                return "<span class='badge badge-status text-success'><i class='fa fa-check-circle green'></i> ".$this->lang->line('Valid')."</span>";
            else
                return "<span class='badge badge-status text-danger'><i class='fa fa-clock-o red'></i> ".$this->lang->line('Expired')."</span>"; 
        }

    }

    public function add_facebook_settings()
    {
        if ($this->session->userdata('user_type') != 'Admin')
            redirect('home/login_page', 'location');

        $data['facebook_settings'] = array();
        $data['page_title'] = $this->lang->line('Facebook App Settings');
        $data['title'] = $this->lang->line('Facebook App Settings');
        $data['body'] = 'admin/social_apps/facebook_settings';

        $appsData_info = $this->basic->get_data("facebook_rx_config");
        $app_data = isset($appsData_info[0]) ? $appsData_info[0] : [];
        if($this->is_demo == '1')
        {
            $app_data['app_id'] = 'XXXXXXXXXXX';
            $app_data['app_secret'] = 'XXXXXXXXXXX';
        }
        $data['app_data'] = $app_data;
        $this->_viewcontroller($data);
    }

    public function facebook_settings_update_action()
    {
        if($this->is_demo == '1')
        {
            echo "<h2 style='text-align:center;color:red;border:1px solid red; padding: 10px'>This feature is disabled in this demo.</h2>"; 
            exit();
        }

        if (!isset($_POST)) exit;

        $this->form_validation->set_rules('api_id', $this->lang->line("App ID"), 'trim|required');
        $this->form_validation->set_rules('api_secret', $this->lang->line("App Secret"), 'trim|required');
        // $table_id = $this->input->post('table_id',true);

        if ($this->form_validation->run() == FALSE) 
        {
            return $this->add_facebook_settings();
        }
        else {

            $insert_data['app_name'] = trim($this->input->post('app_name'));
            $insert_data['app_id'] = trim($this->input->post('api_id'));
            $insert_data['app_secret'] = trim($this->input->post('api_secret'));
            $insert_data['user_id'] = $this->user_id;
            
            $status = $this->input->post('status');
            if($status=='') $status='0';
            $insert_data['status'] = $status;

            $facebook_settings = $this->basic->get_data('facebook_rx_config');

            if (count($facebook_settings) > 0 ) {

                $id = $facebook_settings[0]['id'];
                $this->basic->update_data('facebook_rx_config', array("id"=>$id), $insert_data);
            }
            else {
                $this->basic->insert_data('facebook_rx_config', $insert_data);
            }

            $this->session->set_flashdata('success_message', '1');
            redirect(base_url('social_apps/add_facebook_settings'),'location');
            
        }
    }




}

