<?php

require_once("Home.php"); // loading home controller

class Social_accounts extends Home
{ 
    
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in') != 1)
        redirect('home/login_page', 'location');   
        
        if($this->session->userdata('user_type') != 'Admin' && !in_array(1,$this->module_access))
        redirect('home/login_page', 'location'); 

        $this->important_feature();
        $this->member_validity();
             
    }


    public function index()
    {
      $this->account_import();
    }
  
    public function account_import()
    {
        $data['body'] = 'import_account/account_import';
        $data['page_title'] = $this->lang->line('Google Account Import');

        $data["google_login_button"] = '';

        $login_msg = '';
        $google_app_config = $this->basic->get_data("google_app_config",array("where"=>array("status"=>"1"),$select='',$join='',$limit=1,$start=NULL,$order_by=rand()));
        if(!empty($google_app_config))
        {
            try
            {
                $params['redirectUri'] = base_url("social_accounts/google_login_back");
                $this->load->library("google_my_business",$params);
                $data["google_login_button"] = $this->google_my_business->login_button();

            } catch (Google_Service_Exception $e) {
                $login_msg = $e->getMessage();
            } catch (Google_Exception $e) {
                $login_msg = $e->getMessage();
            } catch (\Exception $e) {
                $login_msg = $e->getMessage();
            }
        }

        $where['where'] = array('user_id'=>$this->user_id);
        $existing_accounts = $this->basic->get_data('google_user_account',$where);

        $show_import_account_box = 1;
        $data['show_import_account_box'] = 1;

        if(!empty($existing_accounts))
        {
            $i=0;
            foreach($existing_accounts as $value)
            {
                $existing_account_info[$i]['useraccount_table_id'] = $value['id'];
                $existing_account_info[$i]['account_display_name'] = $value['account_display_name'];
                $existing_account_info[$i]['email'] = $value['email'];
                $existing_account_info[$i]['account_id'] = $value['account_id'];
                $existing_account_info[$i]['profile_photo'] = $value['profile_photo'];

                $where = array();
                $where['where'] = array('user_account_id'=>$value['id']);
                $location_list = $this->basic->get_data('google_business_locations',$where);
                $existing_account_info[$i]['location_list'] = $location_list;

                $i++;
            }

            $data['existing_accounts'] = $existing_account_info;
        }
        else
            $data['existing_accounts'] = '0';


        $this->_viewcontroller($data);
    }


    public function location_delete_action()
    {
        $this->ajax_check();
        $response = array();
        if($this->is_demo == '1')
        {
            if($this->session->userdata('user_type') == "Admin")
            {
                $response['status'] = 0;
                $response['message'] = "You can not delete anything from admin account!!";
                echo json_encode($response);
                exit();
            }
        }

        $location_table_id = $this->input->post("location_table_id",true);

        $this->db->trans_start();

        $this->basic->delete_data('google_business_locations',['id'=>$location_table_id]);
        $this->basic->delete_data('google_posts_campaign',['user_id'=>$this->user_id,'location_table_id'=>$location_table_id]);
        $this->basic->delete_data('google_review_reply_report',['user_id'=>$this->user_id,'location_id'=>$location_table_id]);
        $this->basic->delete_data('google_review_reply_settings',['user_id'=>$this->user_id,'location_id'=>$location_table_id]);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) 
        {    
            $response['status'] = 0;
            $response['message'] = $this->lang->line('Something went wrong, please try again.');         
        }
        else
        {
            $response['status'] = 1;
            $response['message'] = $this->lang->line("Your location and all of it's corresponding campaigns have been deleted successfully.");      
        }

        
        echo json_encode($response);

    }


    public function account_delete_action()
    {
        $response = array();
        if($this->is_demo == '1')
        {
            if($this->session->userdata('user_type') == "Admin")
            {
                $response['status'] = 0;
                $response['message'] = "You can't delete anything from admin account!!";
                echo json_encode($response);
                exit();
            }
        }
        
        $gmb_user_table_id = $this->input->post("gmb_user_table_id");

        $this->db->trans_start();

        $this->basic->delete_data('google_user_account',['user_id'=>$this->user_id,'id'=>$gmb_user_table_id]);
        $locations = $this->basic->get_data('google_business_locations',['where'=>['user_account_id'=>$gmb_user_table_id]],'id');
        foreach($locations as $value)
        {
            $this->basic->delete_data('google_business_locations',['id'=>$value['id']]);
            $this->basic->delete_data('google_posts_campaign',['user_id'=>$this->user_id,'location_table_id'=>$value['id']]);
            $this->basic->delete_data('google_review_reply_report',['user_id'=>$this->user_id,'location_id'=>$value['id']]);
            $this->basic->delete_data('google_review_reply_settings',['user_id'=>$this->user_id,'location_id'=>$value['id']]);
        }
        $this->_delete_usage_log($module_id=1,$request=1);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) 
        {    
            $response['status'] = 0;
            $response['message'] = $this->lang->line('Something went wrong, please try again.');         
        }
        else
        {
            $response['status'] = 1;
            $response['message'] = $this->lang->line("Your account and all of it's corresponding locations and campaigns have been deleted successfully. Now you'll be redirected to the login page again.");      
        }

        $this->session->sess_destroy();
        echo json_encode($response);
        
    }


    public function fb_rx_account_switch()
    {
        $this->ajax_check();
        $id=$this->input->post("id");
        $this->session->set_userdata("google_mybusiness_user_table_id",$id); 
    }

    public function google_login_back()
    {
        $params['redirectUri'] = base_url("social_accounts/google_login_back");
        $this->load->library("google_my_business",$params);
        $config_table_id = $this->session->userdata('gmb_config_table_id');
        $myBusiness = $this->google_my_business;
        $client = $this->google_my_business->client;
        $oauth = $this->google_my_business->oauth;

        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $access_token = $client->getAccessToken();
            $client->setAccessToken($access_token);
        }

        $accounts = [];
        $login_msg = '';

        try {
            $accounts = $myBusiness->accountInformation();
        } catch (Google_Service_Exception $e) {
            $login_msg = $e->getMessage();
        } catch (Google_Exception $e) {
            $login_msg = $e->getMessage();
        } catch (\Exception $e) {
            $login_msg = $e->getMessage();
        }

        if (! empty($login_msg)) {
            $this->session->set_flashdata('login_msg', $login_msg);
            return redirect("home/login_page");
        }

        $name = '';
        $account_id = '';

        if (isset($accounts[0])) {
            $account_id = isset($accounts[0]['name']) ? $accounts[0]['name'] : '';
            $name = isset($accounts[0]['accountName']) ? $accounts[0]['accountName'] : '';
        }

        $login_msg = '';
        $userProfile = '';

        try {
            $userProfile = $oauth->userinfo->get();
        } catch (Google_Service_Exception $e) {
            $login_msg = $e->getMessage();
        } catch (Google_Exception $e) {
            $login_msg = $e->getMessage();
        } catch (\Exception $e) {
            $login_msg = $e->getMessage();
        }

        if (! empty($login_msg)) {
            $this->session->set_flashdata('login_msg', $login_msg);
            return redirect("home/login_page");
        }

        $email = '';
        $username = '';
        $profilePicture = '';

        if (is_object($userProfile)) {
            $username = trim($userProfile->getGivenName() . ' ' . $userProfile->getFamilyName());
            $email = $userProfile->getEmail();
            $profilePicture = $userProfile->getPicture();
        }

        $access_token = $client->getAccessToken();
        $user_id = $this->user_id;

        if($account_id && $email)
        {
            if(file_exists(APPPATH.'core/licence_type.txt'))
               $this->license_check_action();

            $data = [
                'user_id' => $user_id,
                'app_config_id' => $config_table_id,
                'account_id'=> $account_id,
                'account_name' => $username,
                'email' => $email,
                'account_display_name' => $name,
                'access_token' => $access_token,
                'profile_photo' => $profilePicture
            ];

            $existing_info = $this->basic->get_data('google_user_account',['where'=>['user_id'=>$user_id,'account_id'=>$account_id]]);
            if(empty($existing_info))
            {
                //************************************************//
                $status=$this->_check_usage($module_id=1,$request=1);
                if($status=="2") 
                {
                    $this->session->set_userdata('limit_cross', $this->lang->line("Module limit is over, you can not add more Google account now."));
                    redirect('social_accounts/index', 'location');              
                    exit();
                }
                else if($status=="3") 
                {
                    $this->session->set_userdata('limit_cross', $this->lang->line("Module limit is over, you can not add more Google account now."));
                    redirect('social_accounts/index', 'location');              
                    exit();
                }
                //************************************************//
                
                $this->basic->insert_data('google_user_account',$data);
                $google_mybusiness_user_table_id = $this->db->insert_id();
                $this->session->set_userdata('google_mybusiness_user_table_id',$google_mybusiness_user_table_id);
                $this->_insert_usage_log($module_id=1,$request=1); 
            }
            else
            {
                $this->basic->update_data('google_user_account',['user_id'=>$user_id,'account_id'=>$account_id],$data);
                $google_mybusiness_user_table_id = $existing_info[0]['id'];
                $this->session->set_userdata('google_mybusiness_user_table_id',$google_mybusiness_user_table_id);
            }

            $error = '';
            $locations = [];

            try {
                $locations = $myBusiness->listAccountsLocations($account_id);
            } catch (Google_Service_Exception $e) {
                $error = $e->getMessage();
            } catch (Google_Exception $e) {
                $error = $e->getMessage();
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }

            if (! empty($error)) {
                // Do your stuff
            }

            if (count($locations)) {
                foreach ($locations as $key => $location) {

                    try {
                        $reviews_list = $myBusiness->reviewsList($location['name']);
                    } catch (Google_Service_Exception $e) {
                        $e->getMessage();
                    } catch (Google_Exception $e) {
                        $e->getMessage();
                    } catch (\Exception $e) {
                        $e->getMessage();
                    }

                    $last_review = null;

                    if (count($reviews_list)) {
                        $last_review = isset($reviews_list['reviews'][0]['name']) ? $reviews_list['reviews'][0]['name'] : null;
                    }

                    $temp = explode('/', $location['name']);
                    $only_location_id = array_pop($temp);
                    $data = [];
                    $data['user_id'] = $user_id;
                    $data['user_account_id'] = $google_mybusiness_user_table_id;
                    $data['location_id'] = $location['name'];
                    $data['only_location_id'] = $only_location_id;
                    $data['location_display_name'] = $location['locationName'];
                    $data['latitude_longitude'] = json_encode($location['latlng']);
                    $data['map_url'] = isset($location['metadata']['mapsUrl']) ? $location['metadata']['mapsUrl'] : '';
                    $data['new_review_url'] = isset($location['metadata']['newReviewUrl']) ? $location['metadata']['newReviewUrl'] : '';
                    $data['address'] = json_encode($location['address']);
                    $data['last_review_reply_id'] = $last_review;

                    $profile_google_url = '';
                    $cover_google_url = '';

                    // Pulls off media
                    $error = '';
                    $mediaItems = [];

                    try {
                        $mediaItems = $myBusiness->list_media($location['name']);
                    } catch (Google_Service_Exception $e) {
                        $error = $e->getMessage();
                    } catch (Google_Exception $e) {
                        $error = $e->getMessage();
                    } catch (\Exception $e) {
                        $error = $e->getMessage();
                    }

                    if (! empty($error)) {
                        // Do your stuff
                    }

                    if (count($mediaItems)) {
                        foreach ($mediaItems as $key => $media) {

                            if (isset($media['locationAssociation']['category']) 
                                && 'profile' == strtolower($media['locationAssociation']['category'])
                            ) {
                                $profile_google_url = isset($media['googleUrl']) ? $media['googleUrl'] : '';
                            }

                            if (isset($media['locationAssociation']['category']) 
                                && 'cover' == strtolower($media['locationAssociation']['category'])
                            ) {
                                $cover_google_url = isset($media['googleUrl']) ? $media['googleUrl'] : '';
                            }
                        }
                    }

                    $data['profile_google_url'] = $profile_google_url;
                    $data['cover_google_url'] = $cover_google_url;

                    if(!$this->basic->is_exist("google_business_locations",array("user_account_id"=>$google_mybusiness_user_table_id,"location_id"=>$location['name'])))
                        $this->basic->insert_data('google_business_locations',$data);
                    else
                        $this->basic->update_data('google_business_locations',["user_account_id"=>$google_mybusiness_user_table_id,"location_id"=>$location['name']],$data);

                }
            }
            
            if ($this->session->userdata('logged_in') == 1 && $this->session->userdata('user_type') == 'Admin')
            {
                redirect('social_accounts/index', 'location');
            }
            if ($this->session->userdata('logged_in') == 1 && $this->session->userdata('user_type') == 'Member')
            {
                redirect('social_accounts/index', 'location');
            }
        }
    }
}